@extends('datadelivery::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-upload" aria-hidden="true"></i> Data Delivery</h3>
</div>
<div class="alert alert-warning">
 <p> Upload the file with the correct extension. The file name is not important.</p>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<div class="col-sm-8 col-centered">
<div class="panel panel-default">
<div class="panel-body">
<form action="{{ URL::to('data_delivery/upload') }}" method="POST" enctype="multipart/form-data">
{!! csrf_field() !!}
            <div class="form-group">
            <label>File to upload</label>
            <select class="form-control" name="csvtype">
              <option value="1">Google - Pulse Chat CSV</option>
              <option value="2">Google - Pulse Email CSV</option>
              <option value="3">Google - Pulse CSAT CSV</option>
              <option value="22">Google - QMS</option>
              <option value="21">Google - Tier 2 QA</option>
              <option value="16">Google (YouTube) - Commerce Dashboard</option>
              <option value="17">Google (YouTube) - CPO</option>
              <option value="18">Google (YouTube) - Productivity</option>
              <option value="4">HTC - Agent PType SLA</option>
              <option value="5">HTC - Chat</option>
              <option value="6">HTC - QMS IVR</option>
              <option value="15">HTC - QMS IVR 2014</option>
              <option value="7">HTC - Device Map</option>
              <option value="8">HTC - DSAT Daily</option>
              <option value="9">HTC - Email Tickets Lang Daily</option>
              <option value="10">HTC - Email Tickets Lang Weekly</option>
              <option value="11">HTC - Email Replies Daily</option>
              <option value="12">HTC - Email SLA Monthly</option>
              <option value="13">HTC - Email SLA Weekly</option>
              <option value="14">HTC - Email SLA Daily</option>
              <option value="19">Windstream CSAT</option>
              <option value="20">Groupon FTR</option>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label" for="InputFile">File input</label>
            <input type="file" name="fileToUpload" id="input-1a" type="file" class="file" data-show-preview="false">
          </div>
        </form>
</div>
</div>
</div>
</div>
@endsection