@extends('elements::layouts.app')
@section('content')
  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> Integrations</h3> 
  </div>
@if(Session::has('message'))
<br />
                <div class="alert alert-success">
                  {!!Session::get('message')!!}
                </div>
@endif
<br /><br />
<div class="container-fluid">

            <form method="post" action="IntegrationController@bulk_update" class="form-inline">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="lead_status">For selected rows, change </label>
                {!! Form::select('bulk_name', $test_columns, [], ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="lead_status"> to</label>

                {!! Form::text('bulk_value', null, ['class' => 'form-control'])!!}

                <select class="form-control" name="updatetype" style="display: none">
                  <option value=""></option>
                  @foreach($type as $key => $value)
                  <option value="{!! $value->type !!}">{!! $value->type !!}</option>
                  @endforeach
                </select>

                <select class="form-control" name="updatesubtype" style="display: none">
                  <option value=""></option>
                  @foreach($sub_type as $key => $value)
                  <option value="{!! $value->sub_type !!}">{!! $value->sub_type !!}</option>
                  @endforeach
                </select>

                <select class="form-control" name="updateelementtype" style="display: none">
                  <option value=""></option>
                  @foreach($element_type as $key => $value)
                  <option value="{!! $value->element_type !!}">{!! $value->element_type !!}</option>
                  @endforeach
                </select>

                <select class="form-control" name="updatesystemname" style="display: none">
                  <option value=""></option>
                  @foreach($system_name as $key => $value)
                  <option value="{!! $value->system_name !!}">{!! $value->system_name !!}</option>
                  @endforeach
                </select>

                <select class="form-control" name="updateticketreq" style="display: none">
                  <option value=""></option>
                  <option value="1">True</option>
                  <option value="0">False</option>
                </select>
            </div>
            <button class="btn btn-primary">Save</button>
<hr>
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-bordered table-condensed" id="sortable">
<thead>
<tr>
<th width="10px"><input type="checkbox" id="checkAll" /></th>
<th>Src_type_id</th>
<th>Integration*</th>
<th class="select-filter">Type*</th>
<th class="select-filter">Sub_Type*</th>
<th>Integration_Comment*</th>
<th class="select-filter">Element_Type*</th>
<th class="select-filter">System_Name*</th>
<th class="select-filter">Ticket_req*</th>
</tr>
</thead>
<tfoot>
<tr>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
<tbody>
@foreach($integrations as $key => $value)
 <tr>
        <td width="10px"> <input type="checkbox" name="ids_to_edit[]" value="{{$value->src_type_id}}" /> </td>
            <td>{{ $value->src_type_id }}</a></td>
            <td><a href="#" class="testEdit" data-type="text" data-column="integration" data-url="{{route('integration/update', ['src_type_id'=>$value->src_type_id])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="integration">{{ $value->integration }}</a></td>
            <td><a href="#" class="testEdit2" data-column="type" data-url="{{route('integration/update', ['src_type_id'=>$value->src_type_id])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="type">{{ $value->type }}</a></td>
            <td><a href="#" class="testEdit3" data-column="sub_type" data-url="{{route('integration/update', ['src_type_id'=>$value->src_type_id])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="sub_type">{{ $value->sub_type }}</a></td>
            <td><a href="#" class="testEdit4" data-type="text" data-column="integration_comment" data-url="{{route('integration/update', ['src_type_id'=>$value->src_type_id])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="integration_comment">{{ $value->integration_comment }}</a></td>
            <td><a href="#" class="testEdit5" data-column="element_type" data-url="{{route('integration/update', ['src_type_id'=>$value->src_type_id])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="element_type">{{ $value->element_type }}</a></</td>
            <td><a href="#" class="testEdit6" data-column="system_name" data-url="{{route('integration/update', ['src_type_id'=>$value->src_type_id])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="system_name">{{ $value->system_name }}</a></td>
            <td>
                    @if ($value->ticket_req == "1") 
                    <a href="#" class="testEdit7" data-column="ticket_req" data-url="{{route('integration/update', ['src_type_id'=>$value->src_type_id])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="ticket_req">True</a>
                    @elseif ($value->ticket_req == "0") 
                    <a href="#" class="testEdit7" data-column="ticket_req" data-url="{{route('integration/update', ['src_type_id'=>$value->src_type_id])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="ticket_req">False</a>
                    @endif 
            </td>
</tr>
 @endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $('select[name=bulk_name]').on('change', function(){
    if($(this).val() == "type"){
        $('select[name=updatetype]').show();
        $('select[name=updatesubtype]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updateelementtype]').hide().val('');
        $('select[name=updatesystemname]').hide().val('');
        $('select[name=updateticketreq]').hide().val('');
    }
    else if($(this).val() == "sub_type"){
        $('select[name=updatetype]').hide().val('');
        $('select[name=updatesubtype]').show();
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updateelementtype]').hide().val('');
        $('select[name=updatesystemname]').hide().val('');
        $('select[name=updateticketreq]').hide().val('');
    }
    else if($(this).val() == "element_type"){
        $('select[name=updatetype]').hide().val('');
        $('select[name=updatesubtype]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updateelementtype]').show();
        $('select[name=updatesystemname]').hide().val('');
        $('select[name=updateticketreq]').hide().val('');
    }
    else if($(this).val() == "system_name"){
        $('select[name=updatetype]').hide().val('');
        $('select[name=updatesubtype]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updateelementtype]').hide().val('');
        $('select[name=updatesystemname]').show();
        $('select[name=updateticketreq]').hide().val('');
    }
    else if($(this).val() == "ticket_req"){
        $('select[name=updatetype]').hide().val('');
        $('select[name=updatesubtype]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updateelementtype]').hide().val('');
        $('select[name=updatesystemname]').hide().val('');
        $('select[name=updateticketreq]').show();
    }
    else {
        $('select[name=updatetype]').hide().val('');
        $('select[name=updatesubtype]').hide().val('');
        $('input[name=bulk_value]').show();
        $('select[name=updateelementtype]').hide().val('');
        $('select[name=updatesystemname]').hide().val('');
        $('select[name=updateticketreq]').hide().val('');
    }
});



$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit2').editable({
        type: 'select',
        source: [
        <?php foreach($type as $key => $value){ ?>
            {value: '<?php echo $value->type; ?>', text: '<?php echo $value->type; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

        $('.testEdit3').editable({
        type: 'select',
        source: [
        <?php foreach($sub_type as $key => $value){ ?>
            {value: '<?php echo $value->sub_type; ?>', text: '<?php echo $value->sub_type; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

        $('.testEdit4').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

        $('.testEdit5').editable({
        type: 'select',
        source: [
        <?php foreach($element_type as $key => $value){ ?>
            {value: '<?php echo $value->element_type; ?>', text: '<?php echo $value->element_type; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

        $('.testEdit6').editable({
        type: 'select',
        source: [
        <?php foreach($system_name as $key => $value){ ?>
            {value: '<?php echo $value->system_name; ?>', text: '<?php echo $value->system_name; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

        $('.testEdit7').editable({
        type: 'select',
        source: [
            {value: '1', text: 'True'},
            {value: '0', text: 'False'},
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });
});

  $(document).ready(function() {
    $('#sortable').DataTable({
      "stateSave": true,
        "scrollX": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
      "columnDefs": [
        { "orderable": false, "targets": 0 },
        { "targets": [3,4,6,7,8], type: "html" }
                    ],
      "order": [[ 1, "desc" ]],
        initComplete: function () {
        this.api().columns('.select-filter').every( function () {
            var column = this;
            var select = $('<select name="filter"><option value=""></option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            column.data().unique().sort().each( function ( d, j ) {
               // For first column
               // ignore HTML tags 
                if((column.index() == 3) || (column.index() == 4) || (column.index() == 6) || (column.index() == 7) || (column.index() == 8))
                { d = $(d).text(); }
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );
    }
} );
    var usedNames = {};
    $('select[name="filter"] > option').each(function(){
      if(usedNames[this.text]) {
          $(this).remove();
      } else {
          usedNames[this.text] = this.value;
      }
  });
} );



 $("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });
</script>
@endsection