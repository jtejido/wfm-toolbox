@extends('elements::layouts.app')
@section('content')
  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> Contracts</h3> 
  </div>

<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-bordered table-condensed" id="sortable">
<thead>
<tr>
<th>Contract_id</th>
<th>Contract*</th>
<th>Customer_id*</th>
</tr>
</thead>
<tfoot>
<tr>
<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
<tbody>
@foreach($contracts as $key => $value)
 <tr>
            <td>{{ $value->contract_id }}</td>
            <td><a href="#" class="testEdit" data-type="text" data-column="contract" data-url="{{route('contract/update', ['contract_id'=>$value->contract_id])}}" data-pk="{{$value->contract_id}}" data-title="change" data-name="contract">{{ $value->contract }}</a></td>
            <td><a href="#" class="testEdit2" data-column="customer_id" data-url="{{route('contract/update', ['contract_id'=>$value->contract_id])}}" data-pk="{{$value->contract_id}}" data-title="change" data-name="customer_id">{{ $value->customer }}</a></td>
</tr>
 @endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit2').editable({
        type: 'select',
        source: [
        <?php foreach($customers as $key => $value){ ?>
            {value: '<?php echo $value->customer_id; ?>', text: '<?php echo $value->customer; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

    
});


  $(document).ready(function() {
    $('#sortable').DataTable({
      "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
        initComplete: function () {
            this.api().columns('.select-filter').every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
} );
} );

</script>
@endsection