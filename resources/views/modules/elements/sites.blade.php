@extends('elements::layouts.app')
@section('content')
  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> Sites</h3> 
  </div>
<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-bordered table-condensed" id="sortable">
<thead>
<tr>
<th>Site_id</th>
<th>Site*</th>
<th>City*</th>
<th>Country*</th>
<th>Region_id*</th>
<th>Site_timezone*</th>
<th>Daily_hours*</th>
</tr>
</thead>
<tfoot>
<tr>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
<tbody>
@foreach($sites as $key => $value)
 <tr>
            <td>{{ $value->site_id }}</td>
            <td><a href="#" class="testEdit" data-type="text" data-column="site" data-url="{{route('sites/update', ['site_id'=>$value->site_id])}}" data-pk="{{$value->site_id}}" data-title="change" data-name="site">{{ $value->site }}</a></td>
            <td><a href="#" class="testEdit2" data-type="text" data-column="city" data-url="{{route('sites/update', ['site_id'=>$value->site_id])}}" data-pk="{{$value->site_id}}" data-title="change" data-name="city">{{ $value->city }}</a></td>
            <td><a href="#" class="testEdit3" data-type="text" data-column="country" data-url="{{route('sites/update', ['site_id'=>$value->site_id])}}" data-pk="{{$value->site_id}}" data-title="change" data-name="country">{{ $value->country }}</a></td>
            <td><a href="#" class="testEdit4" data-column="region_id" data-url="{{route('sites/update', ['site_id'=>$value->site_id])}}" data-pk="{{$value->site_id}}" data-title="change" data-name="region_id">{{ $value->region }}</a></td>
            <td><a href="#" class="testEdit5" data-type="text" data-column="site_timezome" data-url="{{route('sites/update', ['site_id'=>$value->site_id])}}" data-pk="{{$value->site_id}}" data-title="change" data-name="site_timezome">{{ $value->site_timezome }}</a></td>
            <td><a href="#" class="testEdit6" data-type="text" data-column="daily_hours" data-url="{{route('sites/update', ['site_id'=>$value->site_id])}}" data-pk="{{$value->site_id}}" data-title="change" data-name="daily_hours">{{ $value->daily_hours }}</a></td>
</tr>
 @endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit2').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit3').editable({
        type: 'select',
        source: [
            {value: '1', text: 'True'},
            {value: '0', text: 'False'}
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

    $('.testEdit4').editable({
        type: 'select',
        source: [
        <?php foreach($regions as $key => $value){ ?>
            {value: '<?php echo $value->region_id; ?>', text: '<?php echo $value->region; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

    $('.testEdit5').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit6').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });
});

  $(document).ready(function() {
    $('#sortable').DataTable({
      "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
      "order": [[ 1, "desc" ]],
        initComplete: function () {
            this.api().columns('.select-filter').every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
} );
} );

</script>
@endsection