<!DOCTYPE html>
<html lang="en">
<head>
@include('includes.head')
</head>
<body class="hold-transition skin-yellow-light sidebar-mini">
                  <div class="inner-wrap">
                    <section class="content">
                        <div class="row">
                        <div class="col-lg-12">
                            
                                @yield('content')
                        </div>
                        </div>
                     </section>
                  </div>
</body>
<script src="{{ asset('/js/all.js') }}"></script>
<script src="{{ asset('/js/app.min.js') }}"></script>
@yield('js')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
</body>
</html>