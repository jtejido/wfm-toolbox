<!DOCTYPE html>
<html lang="en">
<head>
@include('includes.head')
</head>
<body class="hold-transition skin-yellow-light sidebar-mini fixed">
<div class="wrapper">
@include('partials.header')
@include('includes.sidebar')
        <!-- MAIN VIEW -->
                  <div class="content-wrapper">
                    <section class="content">
                        <div class="row">
                        <div class="col-lg-12">
                            
                                @yield('content')
                        </div>
                        </div>
                     </section>
                  </div>
                

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.1
    </div>
    <strong>Conduent Internal Use Only.</strong>
</div>
</body>
<script type="text/javascript">
  $(document).ready(function() {
    $(".dropdown-toggle").dropdown();
});
</script>
<script src="{{ asset('/js/all.js') }}"></script>
<script src="{{ asset('/js/app.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap-modal.js') }}"></script>
<script src="{{ asset('/js/bootstrap-modalmanager.js') }}"></script>
@yield('js')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
</body>
</html>