@extends('admin::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-user" aria-hidden="true"></i> User Administration</h3>
    
</div>

<div class="alert alert-info">
 <p> This page is used for changing Users' permissions to specific modules, Assign Web Queries, and a dashboard for recent Queries used in tables that are within D&PM Toolbox's scope. Please note that any changes made on a specific user requires logging out/in from the app in order to take effect.</p>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif

<div class="container-fluid">

<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                      <li class="active"><a href="#tab1default" data-toggle="tab">Toolbox Users</a></li>
                                      <li><a href="#tab2default" data-toggle="tab">Web Query Users</a></li>
                                      <li><a href="#tab3default" data-toggle="tab">Toolbox Queries</a></li>
                                      <li><a href="#tab4default" data-toggle="tab">User Accounts</a></li>
                            </ul>
    </div>
<div class="panel-body">
<div class="tab-content">
        <div class="tab-pane fade in active" id="tab1default">
        <div class="container-fluid">
        <small style="float: right;"><i>* are editable</i></small><br/>
        <small style="float: right;"><i>** only applies to Auto-Reports and Elements</i></small>
<br />
          <div class="widget-box">
            <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-signal"></i></span>
              <h5>Toolbox Module Users</h5><button type="button" class="buttons btn btn-default btn-sm" data-toggle="modal" data-target="#create-user-module">Create Record</button>
            </div>
            <div class="widget-content" >
            <div class="row-fluid">
              <table class="table-bordered table-condensed" id="toolbox_users">
                    <thead>
                        <tr>
                            <th>WIN_ID</th>
                            <th>Name</th>
                            <th>Modules*</th>
                            <th>Is Admin?*</th>
                            <th>Is Read-only?**</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($admins as $key => $value)
                        <tr>
                            <td>{{ $value->winid }}</td>
                            <td>{{ $value->name }}</td>
                            <td>

                            <?php
                             $mod = substr($value->modules, 1, -1);
                            ?>
                                <a href="#" class="modulesedit" data-type="checklist" data-value="{{$mod}}" data-title="Select Modules" data-column="modules" data-url="admin/update/{{ $value->winid }}" data-pk="{{$value->winid}}" data-name="modules"></a>

                            </td>
                            <td>
                                @if ($value->is_admin == "1") 
                                    <a href="#" class="testEdit" data-column="is_admin" data-url="admin/update/{{ $value->winid }}" data-pk="{{$value->winid}}" data-title="change" data-name="is_admin" data-value="{{$value->is_admin}}">True</a>
                                    @elseif ($value->is_admin == "0") 
                                    <a href="#" class="testEdit" data-column="is_admin" data-url="admin/update/{{ $value->winid }}" data-pk="{{$value->winid}}" data-title="change" data-name="is_admin" data-value="{{$value->is_admin}}">False</a>
                                    @endif 
                            </td>

                            <td>
                                @if ($value->is_readonly == "1") 
                                    <a href="#" class="testEdit2" data-column="is_readonly" data-url="admin/update/{{ $value->winid }}" data-pk="{{$value->winid}}" data-title="change" data-name="is_readonly" data-value="{{$value->is_readonly}}">True</a>
                                    @elseif ($value->is_readonly == "0") 
                                    <a href="#" class="testEdit2" data-column="is_readonly" data-url="admin/update/{{ $value->winid }}" data-pk="{{$value->winid}}" data-title="change" data-name="is_readonly" data-value="{{$value->is_readonly}}">False</a>
                                    @endif 
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            </div>
          </div>
          </div>
        </div>

        <div class="tab-pane fade" id="tab2default">
                <div class="container-fluid">
                    <div class="widget-box">
                        <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-signal"></i></span>
                          <h5>Web Query Users</h5><button type="button" class="buttons btn btn-default btn-sm" data-toggle="modal" data-target="#create-user-query">Create Record</button>
                        </div>
                        <div class="widget-content" >
                        <div class="row-fluid">
                          <table class="table-bordered table-condensed" id="query_users">
                                <thead>
                                    <tr>
                                        <th>Win ID</th>
                                        <th>Name</th>
                                        <th>Web Query Title</th>
                                        <th>Web Query Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($query_users as $key => $value)
                                    <tr>
                                        <td>{{ $value->user_name }}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->title }}</td>
                                        <td>{{ $value->descr }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        </div>
                      </div>
                    </div>
            </div>

            <div class="tab-pane fade" id="tab3default">
                <div class="container-fluid">
                    <div class="widget-box">
                        <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-signal"></i></span>
                          <h5>Toolbox Queries</h5><button type="button" class="buttons btn btn-default btn-circle btn-sm fa fa-refresh" onclick="window.location.reload();"></button>
                        </div>
                
                        <div class="widget-content" >
                          <div class="row-fluid">
                          <table class="table-bordered table-condensed" id="query_track_users">
                                <thead>
                                    <tr>
                                        <th>Win ID</th>
                                        <th>Query</th>
                                        <th>Bindings</th>
                                        <th>Page</th>
                                        <th>Time Stamp</th>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                        </div>
                      </div>
                    </div>
            </div>



            <div class="tab-pane fade" id="tab4default">
                <div class="container-fluid">
                    <div class="widget-box">
                        <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-signal"></i></span>
                          <h5>User Accounts</h5><button type="button" class="buttons btn btn-default btn-sm" data-toggle="modal" data-target="#create-user-account">Create Record</button>
                        </div>
                        <div class="widget-content" >
                        <div class="row-fluid">
                          <table class="table-bordered table-condensed" id="account_map_users">
                                <thead>
                                    <tr>
                                        <th>Win ID</th>
                                        <th>Client</th>
                                        <th>Name</th>

                                    </tr>
                                </thead>

                            </table>
                        </div>
                        </div>
                      </div>
                    </div>
            </div>

</div>
</div>
</div>




</div>

<div id="create-user-module" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add User</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                    <div class="col-sm-12">
                    <form method="POST" action="{{ URL::to('admin/storemodule') }}">
                        {!! csrf_field() !!}
                        <label for="winid">Win ID</label>
                        <input class="form-control" type="text" name="winid"/>
                        <br /><br />
                        <label for="modules">Modules</label>
                        <br />
                        <ul class="list-group">
                                <li class="list-group-item">
                                Elements
                                <div class="material-switch pull-right">
                                    <input id="_elements" name="modules[]" value="elements" type="checkbox"><label for="_elements" class="label-primary"></label>
                                </div>
                                </li>
                                <li class="list-group-item">
                                Auto Reports
                                <div class="material-switch pull-right">
                                    <input id="_auto_reports" type="checkbox" name="modules[]" value="auto_reports"><label for="_auto_reports" class="label-primary"></label>
                                </div>
                                </li>
                                <li class="list-group-item">
                                DB Management
                                <div class="material-switch pull-right">
                                    <input id="_dbmgmt" type="checkbox" name="modules[]" value="dbmgmt"><label for="_dbmgmt" class="label-primary"></label>
                                </div>
                                </li>
                                <li class="list-group-item">
                                MSS Search
                                <div class="material-switch pull-right">
                                    <input id="_mss_search" type="checkbox" name="modules[]" value="mss_search"><label for="_mss_search" class="label-primary"></label>
                                </div>
                                </li>
                                <li class="list-group-item">
                                Data Delivery
                                <div class="material-switch pull-right">
                                    <input id="_data_delivery" type="checkbox" name="modules[]" value="data_delivery"><label for="_data_delivery" class="label-primary"></label>
                                </div>
                                </li>
                                <li class="list-group-item">
                                Web Query
                                <div class="material-switch pull-right">
                                    <input id="_web_query" type="checkbox" name="modules[]" value="web_query"><label for="_web_query" class="label-primary"></label>
                                </div>
                                </li>
                        </ul>
                        <br /><br />
                        <label for="is_admin">Is Admin?</label>
                        <select class="form-control" name="is_admin">
                        <option value=''></option>
                        <option value='1'>True</option>
                        <option value='0'>False</option>
                        </select>
                        <br /><br />
                        <label for="is_readonly">Is Read-only?</label>
                        <select class="form-control" name="is_readonly">
                        <option value=''></option>
                        <option value='1'>True</option>
                        <option value='0'>False</option>
                        </select>
                        <br /><br />
                    {!! Form::submit('Save', array('class' => 'btn btn-primary btn-lg')) !!}
                    </form> 
                    </div>
                    </div>
            </div>
</div>
</div>
</div>

<div id="create-user-query" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add User</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                    <div class="col-sm-12">
                    <form method="POST" action="{{ URL::to('admin/storequery') }}">
                        {!! csrf_field() !!}
                        <label for="winid">Win ID</label>
                        <input class="form-control" type="text" name="user_name"/>
                        <br /><br />
                        <label for="modules">Web Report</label>
                        <br />
                        <select class="form-control scrollable-menu" name="report_guid">
                        <option value=""></option>
                        @foreach ($webreports as $key => $value)
                        <option value="{{ $value['report_guid'] }}">{!! $value['title'] !!}</option>
                        @endforeach
                        </select>
                        <br /><br />
                    {!! Form::submit('Save', array('class' => 'btn btn-primary btn-lg')) !!}
                    </form> 
                    </div>
                    </div>
            </div>
</div>
</div>
</div>


<div id="create-user-account" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Map User Client</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                    <div class="col-sm-12">
                    <form method="POST" action="{{ URL::to('admin/storeuseraccount') }}">
                        {!! csrf_field() !!}
                        <label for="winid">Win ID</label>
                        <input class="form-control" type="text" name="user_name"/>
                        <br /><br />
                        <label for="modules">Client</label>
                        <br />
                        <select class="form-control scrollable-menu" name="client">
                        <option value=""></option>
                        @foreach ($accounts as $key => $value)
                        <option value="{{ $value['client'] }}">{!! $value['client'] !!}</option>
                        @endforeach
                        </select>
                        <br /><br />
                    {!! Form::submit('Save', array('class' => 'btn btn-primary btn-lg')) !!}
                    </form> 
                    </div>
                    </div>
            </div>
</div>
</div>
</div>

@endsection
@section('js')
<script>
$('#modulesnew').multiSelect();
$(document).ready(function(){
    $('#toolbox_users').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        }
} );
});

$(document).ready(function(){
    $('#query_users').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        }
} );
});


$('.modulesedit').editable({
        source: [
        <?php foreach($modules as $module){ ?>
            {value: '<?php echo $module ?>', text: '<?php echo $module ?>'},
        <?php } ?>
        ],
          display: function(value, sourceData) {
    //display checklist as comma-separated values
    var html = [],
      checked = $.fn.editableutils.itemsByValue(value, sourceData);

    if (checked.length) {
      $.each(checked, function(i, v) {
        html.push($.fn.editableutils.escape(v.text));
      });
      $(this).html(html.join(', '));
    } else {
      $(this).empty();
    }
  },
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

  $('.testEdit').editable({
        type: 'select',
        source: [
            {value: '1', text: 'True'},
            {value: '0', text: 'False'}
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

  $('.testEdit2').editable({
        type: 'select',
        source: [
            {value: '1', text: 'True'},
            {value: '0', text: 'False'}
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });


$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    var conf = $.extend( {
        pages: 25,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'GET' // Ajax HTTP method
    }, opts );

    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;

    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
        
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
        
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );

        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));

                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
            
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);

            request.start = requestStart;
            request.length = requestLength*conf.pages;

            // Provide the same `data` options as DataTables.
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }

            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);

                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    json.data.splice( requestLength, json.data.length );
                    
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            json.draw = request.draw; // Update the echo for each response
            json.data.splice( 0, requestStart-cacheLower );
            json.data.splice( requestLength, json.data.length );

            drawCallback(json);
        }
    }
};

// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );


//
// DataTables initialisation
//
$(document).ready(function() {
    $('#query_track_users').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": $.fn.dataTable.pipeline( {
            url: 'admin/querysearch',
            pages: 25 // number of pages to cache
        } ),
        "lengthMenu": [[5, 10, 25, 50, 75], [5, 10, 25, 50, 75]],
        "pageLength": 5,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
          },
        "order": [[ 4, "desc" ]]
    } );
} );

$(document).ready(function() {
    $('#account_map_users').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": $.fn.dataTable.pipeline( {
            url: 'admin/accountsearch',
            pages: 25 // number of pages to cache
        } ),
        "lengthMenu": [[5, 10, 25, 50, 75], [5, 10, 25, 50, 75]],
        "pageLength": 5,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
          }
    } );
} );


</script>
@endsection
