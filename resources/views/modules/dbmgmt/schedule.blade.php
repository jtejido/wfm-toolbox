@extends('dbmgmt::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Schedules</h3>
    <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#create-schedule">Create Record</button>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-striped table-condensed" id="sortable">
    <thead>
        <tr>
            <th>Object ID</th>
            <th>Hour</th>
            <th>Minutes</th>
            <th>DOW</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($schedules as $key => $value)
        <tr>
            <td>{{ $value->obj_id }}</td>
            <td>{{ $value->time_hour }}</td>
            <td>{{ $value->time_min }}</td>
            <td>
            <?php
             $dow_arr = array("0", "1", "2", "3", "4", "5", "6");
             $day_arr = array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
             $dow = str_replace($dow_arr, $day_arr, $value->dow);
             $dow = substr($dow, 1, -1);
            ?>
                {{ $dow  }}
            </td>
            <td><span style="text-align: center;">
                <form method="POST" action="{{ URL::to('schedule/'.$value->obj_id.'/delete') }}" accept-charset="UTF-8">
                {!! csrf_field() !!}
                <button type="submit" class="btn btn-primary btn-md"> Delete</button>
                <a class="btn btn-primary btn-md" href="{{ URL::to('schedule/'.$value->obj_id.'/edit') }}"> Edit</a>
                {!! Form::hidden('_method', 'DELETE') !!}
                {!! Form::close() !!}
            </span>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
</div>
</div>
</div>
<!-- Create Modal -->
<div id="create-schedule" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" action="{{ URL::to('schedule') }}" accept-charset="UTF-8">
                        {!! csrf_field() !!}
                            <div class="col-sm-12">
                                <strong>{!! Form::label('obj_id', 'Validation ID:') !!}</strong>
                               {!! Form::text('obj_id', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('time_hour', 'Time(Hour):') !!}</strong>
                                {!! Form::text('time_hour', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('time_min', 'Time(Minutes):') !!}</strong>
                                {!! Form::text('time_min', null, array('class' => 'form-control')) !!}
                            </div>
                            
                            <div class="col-sm-12">
                                <strong> {!! Form::label('new_dow', 'Day of Week:') !!}</strong>
                            <div class="col-sm-12">
                                <div class="row">
                                    <input type="checkbox" name="new_dow[]" value="1"><label>Monday</label>
                                    <input type="checkbox" name="new_dow[]" value="2"><label>Tuesday</label>
                                    <input type="checkbox" name="new_dow[]" value="3"><label>Wednesday</label>
                                    <input type="checkbox" name="new_dow[]" value="4"><label>Thursday</label>
                                    <input type="checkbox" name="new_dow[]" value="5"><label>Friday</label>
                                    <input type="checkbox" name="new_dow[]" value="6"><label>Saturday</label>
                                    <input type="checkbox" name="new_dow[]" value="0"><label>Sunday</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                            <div class="col-sm-12 text-center">
                            {!! Form::submit('Create Record', array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                    {!! Form::close() !!}
                    </div>
                 </div>
</div>
</div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        }
} );
});
</script>
@endsection