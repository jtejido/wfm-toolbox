@extends('dbmgmt::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Objects</h3>
    <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#create-object">Create Record</button>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-striped table-condensed" id="sortable">
    <thead>
        <tr>
            <th>Validation ID</th>
            <th>Table or View Name*</th>
            <th>Schema of Table / View*</th>
            <th>Validation Type*</th>
            <th>Where Statement (optional)*</th>
            <th>Column to use for Validation*</th>
            <th>Operator*</th>
            <th>Expected Results*</th>
            <th>Description of Validation*</th>
            <th>Is Validation Active?*</th>
            <th>Friendly Validation Subject*</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($objects as $key => $value)
        <tr>
            <td>{{ $value->obj_id }}</td>
            <td><a href="#" class="testEdit" data-type="text" data-column="obj_name" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_name">{{ $value->obj_name }}</a></td>
            <td><a href="#" class="testEdit2" data-type="text" data-column="obj_schema" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_schema">{{ $value->obj_schema }}</a></td>
            <td><a href="#" class="testEdit3" data-column="obj_type" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_type" data-value="{{$value->obj_type}}">{{ $value->obj_type }}</a></td>
            <td><a href="#" class="testEdit4" data-type="text" data-column="obj_where" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_where">{{ $value->obj_where }}</a></td>
            <td><a href="#" class="testEdit5" data-type="text" data-column="obj_column" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_column">{{ $value->obj_column }}</a></td>
            <td><a href="#" class="testEdit6" data-column="obj_operator" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_operator" data-value="{{$value->obj_operator}}">{{ $value->obj_operator }}</a></td>
            <td><a href="#" class="testEdit7" data-type="text" data-column="obj_exp_result" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_exp_result">{{ $value->obj_exp_result }}</a></td>
            <td><a href="#" class="testEdit8" data-type="text" data-column="obj_descr" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_descr">{{ $value->obj_descr }}</a></td>
            <td> 
                    @if ($value->obj_active == "1") 
                    <a href="#" class="testEdit9" data-column="obj_active" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_active" data-value="{{$value->obj_active}}">True</a>
                    @elseif ($value->obj_active == "0") 
                    <a href="#" class="testEdit9" data-column="obj_active" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_active" data-value="{{$value->obj_active}}">False</a>
                    @endif 
            </td>
            <td><a href="#" class="testEdit10" data-type="text" data-column="obj_friendly_name" data-url="/tools/objects/update/{{ $value->obj_id }}/{{ $value->obj_descr }}" data-pk="{{$value->obj_id}}" data-title="change" data-name="obj_friendly_name">{{ $value->obj_friendly_name }}</a></td>
            <td><a class="btn btn-primary btn-md fa fa-calendar" title="Edit" href="{{ URL::to('objects/'.$value->obj_id.'/'.$value->obj_descr.'/schedules') }}"> Show Schedules</a></td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
</div>
</div>
</div>
<!-- Create Modal -->
<div id="create-object" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                    <form method="POST" action="{{ URL::to('objects') }}" accept-charset="UTF-8">
                    {!! csrf_field() !!}
                             <div class="col-sm-12">
                                <strong>{!! Form::label('obj_id', 'Validation ID:') !!}</strong>
                               {!! Form::text('obj_id', session('next_obj_id'), array('class' => 'form-control', 'readonly' => 'readonly')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('obj_name', 'Table or View Name:') !!}</strong>
                                {!! Form::text('obj_name', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('obj_schema', 'Schema of Table / View:') !!}</strong>
                                {!! Form::text('obj_schema', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong>{!! Form::label('obj_type', 'Validation Type:') !!}</strong>
                                {!! Form::select('obj_type', array('' => '', 'count' => 'count', 'min' => 'min', 'sum' => 'sum', 'max' => 'max'), null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('obj_where', 'Where Statement (optional):') !!}</strong>
                                {!! Form::text('obj_where', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('obj_column', 'Column to use for Validation:') !!}</strong>
                                {!! Form::text('obj_column', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong>{!! Form::label('obj_operator', 'Operator:') !!}</strong>
                                {!! Form::select('obj_operator', array('' => '', '<' => '<', '>' => '>', '>=' => '>=', '<=' => '<=', '=' => '='), null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('obj_exp_result', 'Expected Results:') !!}</strong>
                                {!! Form::text('obj_exp_result', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('obj_descr', 'Description of Validation:') !!}</strong>
                                {!! Form::text('obj_descr', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('obj_active', 'Is Validation Active?') !!}</strong>
                                 {!! Form::select('obj_active', array('1' => 'True', '0' => 'False'), null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('obj_friendly_name', 'Friendly Validation Subject:') !!}</strong>
                                {!! Form::text('obj_friendly_name', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                            <div class="col-sm-12 text-center">
                            {!! Form::submit('Create Record', array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                    {!! Form::close() !!}
                    </div>
                 </div>
</div>
</div>
</div>
@endsection
@section('js')
<script>

$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit2').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit3').editable({
        type: 'select',
        source: [
            {value: 'count', text: 'count'},
            {value: 'min', text: 'min'},
            {value: 'sum', text: 'sum'},
            {value: 'max', text: 'max'}
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });
    $('.testEdit4').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit5').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit6').editable({
        type: 'select',
        source: [
            {value: '<', text: '<'},
            {value: '>', text: '>'},
            {value: '<=', text: '<='},
            {value: '>=', text: '>='},
            {value: '=', text: '='}
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit7').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit8').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit9').editable({
        type: 'select',
        source: [
            {value: '1', text: 'True'},
            {value: '0', text: 'False'}
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit10').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

});

$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "scrollX": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
        "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
        "pageLength": 5
} );
});

</script>
@endsection