@extends('dbmgmt::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Activity Log</h3>
</div>
<div class="container-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5><button type="button" class="buttons btn btn-default btn-circle btn-sm fa fa-refresh" onclick="window.location.reload();">
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-striped table-condensed" id="sortable">
    <thead>
        <tr>
            <th>Event Timestamp</th>
            <th class="select-filter">Category</th>
            <th>Entity</th>
            <th>Message</th>
            <th>Severity</th>
            <th>Notification Status</th>
        </tr>
    </thead>
    <tfoot>
    <tr>
    <th></th>
    <th>Category</th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($activity_logs as $key => $value)
        <tr>
            <td>{{ $value->event_timestamp }}</td>
            <td>{{ $value->category }}</td>
            <td>{{ $value->entity }}</td>
            <td>{{ $value->message }}</td>
            <td>{{ $value->severity }}</td>
            <td>{{ $value->notification_status }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script>

$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
        initComplete: function () {
            this.api().columns('.select-filter').every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
});
</script>
@endsection