@extends('common::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-download" aria-hidden="true"></i> Download Reports</h3>
    <div class="dropdown">
	  <button class="btn btn-default dropdown-toggle test" type="button" data-toggle="dropdown">Select Account
	  <span class="caret"></span></button>
	  <ul class="dropdown-menu scrollable-menu collapse" id="test">
		 @foreach(session('download_reports_available') as $folder)
				<li><a href="{{ URL::to('download_reports/'.$folder.'}/subfolder') }}">{!!$folder!!}</a></li>
			@endforeach
	  </ul>
	</div>
</div>
<div class="container-fluid">
<div class="alert alert-info">
Download any reports issued for {!! $report_group !!} easily via our web portal. If you'd like to subscribe to a report to receive it in your email, go to <code>Request A Report</code> tab.
</div>

  <div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-folder"></i></span>
      <h5>Sub-Directory</h5>
    </div>
    <div class="widget-content" >
		<ul style="list-style: none;">
				@foreach($scanned_directory as $key)
				<li><span class="fa fa-folder" style="margin-right: 5px;" aria-hidden="true"></span> <a href="{{ URL::to('download_reports/'.$report_group.'/subfolder/'.$key.'/files') }}">{!!$key!!}</a></li>
				<hr>
				@endforeach
		</ul>
	</div>
  </div>

</div>

@endsection
