@extends('common::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-file-text" aria-hidden="true"></i> Clean Desk Policy</h3>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="alert alert-info">
This form is used to record Clean Desk Policy Violations.
</div>
<div class="container-fluid">

<div class="col-md-7 col-md-offset-2">
<div class="panel panel-default">
<div class="panel-heading"><h3>Clean Desk Policy Form</h3></div>
<div class="panel-body">

<form method="post" action="{{ URL::to('clean_desk_policy') }}">
{!! csrf_field() !!}
<div class="form-group">
<label>Client: </label>
<select class="form-control" name="client" required>
  <option value=""></option>
    @foreach($clients as $key => $value)
    <option value="{!!$value->client_id!!}">{!!$value->client!!}</option>
  @endforeach
</select>
</div>

<div class="form-group">
<label>Location: </label>
<select class="form-control" name="site" required>
  <option value=""></option>
  @foreach($sites as $key => $value)
    <option value="{!!$value->site_id!!}">{!!$value->site!!}</option>
  @endforeach
</select>
</div>

<div class="form-group">
                <label>Date:</label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="date_time" name="date_time">
                </div>
              </div>

<div class="form-group">
  <label>Clean Desktop Violations:</label>
  <select class="form-control" name="cdv" required>
  <option value=""></option>
  <option value="1">True</option>
  <option value="0">False</option>
</select>
</div>

<div class="form-group">
  <label>Clean Desktop Violation Information:</label>
  <textarea class="form-control" name="cdvi" rows="3" placeholder="Enter ..."></textarea>
</div>

<div class="form-group">
  <label>Cell Phone Violations:</label>
  <select class="form-control" name="cpv" required>
  <option value=""></option>
  <option value="1">True</option>
  <option value="0">False</option>
</select>
</div>

<div class="form-group">
  <label>Cell Phone Violations Information:</label>
  <textarea class="form-control" name="cpvi" rows="3" placeholder="Enter ..."></textarea>
</div>

<div class="form-group">
  <label>Additional Comments:</label>
  <textarea class="form-control" name="addtl_comments" rows="3" placeholder="Enter ..."></textarea>
</div>


{!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
</form>
</div>
</div>
</div>
@endsection
@section('js')
<script>
$('#date_time').datetimepicker();
</script>
@endsection