@extends('common::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-check-square-o" aria-hidden="true"></i> Pending Approval</h3>
</div>
  <div class="alert alert-info">
  What's shown here are requests pending an Admin's approval, all <code>Subscribe</code> or <code>Unsubscribe</code> requests are shown here.
  </div>
<div class="container-fluid">

<div class="col-lg-4">
  <div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-clock-o"></i></span>
      <h5>Pending Approval</h5>
    </div>
    <div class="widget-content" >
          @if(!empty($pending_reports))
          @foreach($pending_reports as $key=>$value)
          <?php
          $text = date_parse(". $value->request_datetime .");
          $monthNum  = $text['month'];
          $monthName = date('F', mktime(0, 0, 0, $monthNum, 10));
          ?>
              <article class="assigned">
                  <div class="assigned-date"><label style="text-align: center">Date Requested:</label>
                    <p class="assigned-month">{!! $text['year']!!} {!!$monthName!!} </p>
                    <p class="assigned-day">{!! $text['day'] !!}</p>
                  </div>

                  <div class="assigned-desc">
                    <h5 class="assigned-desc-header">Report Name: {!! $value->name !!}</h5>
                    <p class="assigned-desc-detail">
                    <span class="assigned-desc-time"></span><i>Requested by: <b>{!! $value->recip_name !!}</b></i>
                    </p>
                     <p class="assigned-desc-detail"><i>Requested at: <b>{!! $value->request_datetime !!}</b></i>
                     </p>
                    <p class="assigned-desc-detail">
                    <span class="assigned-desc-time"></span>
                    <code>
                    @if (($value->request_type) == 'add')
                    Subcribe
                    @else (($value->request_type) == 'delete')
                    Unsubscribe
                    @endif
                    </code> is pending Approval.
                    </p>
                  </div>
                </article>
                <hr>
          @endforeach 
          @else
          <p> No Pending Reports at the moment. </p>
          @endif 
    </div>
  </div>
</div>

<div class="col-lg-8">
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                      <li class="active"><a href="#tab1default" data-toggle="tab">Approved Requests</a></li>
                                      <li><a href="#tab2default" data-toggle="tab">Rejected Requests</a></li>
                            </ul>
    </div>
<div class="panel-body">
<div class="tab-content">
        <div class="tab-pane fade in active" id="tab1default">
        <div class="container-fluid">
          <div class="widget-box">
            <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-check-square-o"></i></span>
              <h5>Approved Requests</h5>
            </div>
            <div class="widget-content" >

                            <table class="table-bordered table-condensed" id="approved">
                                <thead>
                                    <tr>
                                        <th>Report Name</th>
                                        <th>Date Requested</th>
                                        <th>Approver</th>
                                        <th>Date Approved</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($approved_reports as $key => $value)
                                    <tr>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->request_datetime }}</td>
                                        <td>{{ $value->approver }}</td>
                                        <td>{{ $value->datetime }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

            </div>
          </div>
        </div>
        </div>

        <div class="tab-pane fade" id="tab2default">
        <div class="container-fluid">
          <div class="widget-box">
            <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-ban"></i></span>
              <h5>Rejected Requests</h5>
            </div>
            <div class="widget-content" >

                            <table class="table-bordered table-condensed" id="rejected">
                                <thead>
                                    <tr>
                                        <th>Report Name</th>
                                        <th>Date Requested</th>
                                        <th>Approver</th>
                                        <th>Date Rejected</th>
                                        <th>Reason for Rejection</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($rejected_reports as $key => $value)
                                    <tr>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->request_datetime }}</td>
                                        <td>{{ $value->approver }}</td>
                                        <td>{{ $value->datetime }}</td>
                                        <td>{{ $value->comment }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


            </div>
          </div>
        </div>
        </div> 
</div>
</div>
</div>
</div>


</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $('#approved').DataTable( {
        "lengthMenu": [[10, 25, 50, 75, -1], [10, 25, 50, 75, "All"]],
        "pageLength": 10,
        "language": {
            "emptyTable": "No Reports Available"
        },
        "searching": false,
         "ordering": false,
         "select": true
} );
});

$(document).ready(function(){
    $('#rejected').DataTable( {
        "lengthMenu": [[10, 25, 50, 75, -1], [10, 25, 50, 75, "All"]],
        "pageLength": 10,
        "language": {
            "emptyTable": "No Reports Available"
        },
        "searching": false,
         "ordering": false,
         "select": true
} );
});
</script>
@endsection
