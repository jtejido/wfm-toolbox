@extends('autoreports::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-list-ul" aria-hidden="true"></i> Recipients</h3>
    <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#create-recipient">Create Record</button>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid">
       {!! $html->table(['class' => 'table-bordered table-condensed table-hover']) !!}
    </div>
</div>
</div>
</div>
<!-- Create Modal -->
<div id="create-recipient" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" action="{{ URL::to('recipients') }}">
                            {!! csrf_field() !!}
                            <div class="col-sm-12">
                                <strong>{!! Form::label('name', 'Name:') !!}</strong>
                               {!! Form::text('name', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('email_address', 'Email Address:') !!}</strong>
                                {!! Form::text('email_address', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('internal_external', 'Internal/External:') !!}</strong>
                                <select class="form-control" name="internal_external">
                                  @foreach($int_ext as $key => $value)
                                    <option value="{!! $value['internal_external'] !!}">{!! $value['internal_external'] !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('company', 'Company:') !!}</strong>
                                {!! Form::text('company', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('s3_id', 'S3 ID:') !!}</strong>
                                {!! Form::text('s3_id', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                            <div class="col-sm-12 text-center">
                            {!! Form::submit('Create Record', array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                        </form>
                    </div>
                 </div>
</div>
</div>
</div>
@endsection
@section('js')
<script>
$(document).ajaxComplete(function () {
          $.fn.editable.defaults.mode = 'inline';

            $('.email_address').editable({
                params: function(params) {
                    // add additional params from data-attributes of trigger element
                    params.name = $(this).editable().data('name');
                    return params;
                },
                error: function(response, newValue) {
                    if(response.status === 500) {
                        return 'Make sure the Email is not already taken.';
                    } else {
                        return response.responseText;
                        // return "Error.";
                    }
                }
            });

            $('.name').editable({
                params: function(params) {
                    // add additional params from data-attributes of trigger element
                    params.name = $(this).editable().data('name');
                    return params;
                },
                error: function(response, newValue) {
                    if(response.status === 500) {
                        return 'Server error. Check entered data.';
                    } else {
                        return response.responseText;
                        // return "Error.";
                    }
                }
            });

            $('.internal_external').editable({
                type: 'select',
                source: [
                    {value: 'Internal', text: 'Internal'},
                    {value: 'External', text: 'External'}
                ],
                params: function(params) {
                    // add additional params from data-attributes of trigger element
                    params.name = $(this).editable().data('name');
                    return params;
                },
                error: function(response, newValue) {
                    if(response.status === 500) {
                        return 'Server error. Check entered data.';
                    } else {
                        return response.responseText;
                        // return "Error.";
                    }
                }
             });

            $('.company').editable({
                params: function(params) {
                    // add additional params from data-attributes of trigger element
                    params.name = $(this).editable().data('name');
                    return params;
                },
                error: function(response, newValue) {
                    if(response.status === 500) {
                        return 'Server error. Check entered data.';
                    } else {
                        return response.responseText;
                        // return "Error.";
                    }
                }
            });

            $('.s3_id').editable({
                params: function(params) {
                    // add additional params from data-attributes of trigger element
                    params.name = $(this).editable().data('name');
                    return params;
                },
                error: function(response, newValue) {
                    if(response.status === 500) {
                        return 'Server error. Check entered data.';
                    } else {
                        return response.responseText;
                        // return "Error.";
                    }
                }
            });
        });
</script>
{!! $html->scripts() !!}
@endsection
