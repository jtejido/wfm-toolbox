@extends('autoreports::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Approval Queue</h3>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
    <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
          <h5>All Items</h5>
        </div>
        <div class="widget-content" >
            <div class="row-fluid">
                {!! $html->table(['class' => 'table-bordered table-condensed table-hover']) !!}
            </div>
        </div>
    </div>
</div>


<div id="approve" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Approve Request?</h4>
              </div>
                    <div class="modal-body">
                            <div class="container-fluid">
                            <div class="col-sm-12">
                            <form method="post" action="{{ URL::to('approval_queue/approve') }}"> 
                                {!! csrf_field() !!}
                                <label for="lead_status">TO/CC/BCC</label>
                                <select class="form-control" name="to_cc_bcc">
                                <option value=''></option>
                                <option value='to'>To</option>
                                <option value='cc'>CC</option>
                                <option value='bcc'>BCC</option>
                                </select>

                                <label for="lead_status">Email Type</label>
                                <select class="form-control" name="email_type">
                                <option value=''></option>
                                <option value='Body'>Body</option>
                                <option value='Body & Attachment'>Body & Attachment</option>
                                <option value='Link'>Link</option>
                                <option value='Internal Attachment'>Internal Attachment</option>
                                <option value='External Attachment'>External Attachment</option>
                                </select>

                                <input type="hidden" id="buttonText" name="request_id"/>
                                <input type="hidden" id="buttonText2" name="rid"/>
                                <input type="hidden" id="buttonText3" name="recipient_id"/>
                                <input type="hidden" id="buttonText4" name="request_type"/>
                                <br /><br />
                            <button class="btn-primary btn-lg fa fa-check" type="submit">Save</button>
                            </form> 
                            </div>
                            </div>
                    </div>
        </div>
    </div>
</div>


<div id="reject" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reject Request?</h4>
              </div>
                    <div class="modal-body">
                            <div class="container-fluid">
                            <div class="col-sm-12">
                            <form method="post" action="{{ URL::to('approval_queue/reject') }}"> 
                                {!! csrf_field() !!}

                                <strong>
                                    <label for="comment">Reason for Rejection:</label>
                                </strong>
                                <input type="text" name="comment"/>
                                <input type="hidden" id="buttonText5" name="request_id"/>
                                <br /><br />
                            <button class="btn-primary btn-lg fa fa-check" type="submit">Save</button>
                            </form> 
                            </div>
                            </div>
                    </div>
        </div>
    </div>
</div>
@endsection
@section('js')

<script>
$(document).ready(function () {
$('#dataTableBuilder').on("click", ".approve", function () {
    var newText =  $(this).data('request-id');
    var newText2 =  $(this).data('rid');
    var newText3 =  $(this).data('recipient-id');
    var newText4 =  $(this).data('request-type');
  
    $(".modal-body #buttonText").val( newText );
    $(".modal-body #buttonText2").val( newText2 );
    $(".modal-body #buttonText3").val( newText3 );
    $(".modal-body #buttonText4").val( newText4 );
});

$('#dataTableBuilder').on("click", ".reject", function () {
    var newText5 =  $(this).data('request-id');

  
    $(".modal-body #buttonText5").val( newText5 );

});
});
</script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('vendor/datatables/buttons.server-side.js') }}"></script>
{!! $html->scripts() !!}
@endsection