@extends('autoreports::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-list-ul" aria-hidden="true"></i> Reports Management
    </h3>

<p>Edit Frequency</p>

</div>
@if(count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif

<div class="container-fluid">
<div class="col-sm-8 col-centered">
<div class="panel panel-default">
<div class="panel-heading"><h3>Frequency Details</h3></div>
<div class="panel-body">
@foreach($frequency as $key => $value)

<form method="POST" action="{{ URL::to('frequency/'.$value->rid.'/'.$value->day.'/'.$value->scheduled_time.'/put') }}">
 {!! csrf_field() !!}

        {!! Form::label('rid', 'Report ID:') !!}

        {!! Form::text('rid', $value->rid, array('class' => 'form-control', 'readonly' => 'readonly')) !!}

        {!! Form::label('day', 'Day:') !!}

        <select class="form-control" id="days" name="day" readonly disabled>
        <option value="monday" <?php if($value->day == "monday"){ echo "selected"; } ?>>Monday</option>
        <option value="tuesday" <?php if($value->day == "tuesday"){ echo "selected"; } ?>>Tuesday</option>
        <option value="wednesday" <?php if($value->day == "wednesday"){ echo "selected"; } ?>>Wednesday</option>
        <option value="thursday" <?php if($value->day == "thursday"){ echo "selected"; } ?>>Thursday</option>
        <option value="friday" <?php if($value->day == "friday"){ echo "selected"; } ?>>Friday</option>
        <option value="saturday" <?php if($value->day == "saturday"){ echo "selected"; } ?>>Saturday</option>
        <option value="sunday" <?php if($value->day == "sunday"){ echo "selected"; } ?>>Sunday</option>
        <option value="01" <?php if($value->day == "01"){ echo "selected"; } ?>>01</option>
        <option value="02" <?php if($value->day == "02"){ echo "selected"; } ?>>02</option>
        <option value="03" <?php if($value->day == "03"){ echo "selected"; } ?>>03</option>
        <option value="04" <?php if($value->day == "04"){ echo "selected"; } ?>>04</option>
        <option value="05" <?php if($value->day == "05"){ echo "selected"; } ?>>05</option>
        <option value="06" <?php if($value->day == "06"){ echo "selected"; } ?>>06</option>
        <option value="07" <?php if($value->day == "07"){ echo "selected"; } ?>>07</option>
        <option value="08" <?php if($value->day == "08"){ echo "selected"; } ?>>08</option>
        <option value="09" <?php if($value->day == "09"){ echo "selected"; } ?>>09</option>
        <option value="10" <?php if($value->day == "10"){ echo "selected"; } ?>>10</option>
        <option value="11" <?php if($value->day == "11"){ echo "selected"; } ?>>11</option>
        <option value="12" <?php if($value->day == "12"){ echo "selected"; } ?>>12</option>
        <option value="13" <?php if($value->day == "13"){ echo "selected"; } ?>>13</option>
        <option value="14" <?php if($value->day == "14"){ echo "selected"; } ?>>14</option>
        <option value="15" <?php if($value->day == "15"){ echo "selected"; } ?>>15</option>
        <option value="16" <?php if($value->day == "16"){ echo "selected"; } ?>>16</option>
        <option value="17" <?php if($value->day == "17"){ echo "selected"; } ?>>17</option>
        <option value="18" <?php if($value->day == "18"){ echo "selected"; } ?>>18</option>
        <option value="19" <?php if($value->day == "19"){ echo "selected"; } ?>>19</option>
        <option value="20" <?php if($value->day == "20"){ echo "selected"; } ?>>20</option>
        <option value="21" <?php if($value->day == "21"){ echo "selected"; } ?>>21</option>
        <option value="22" <?php if($value->day == "22"){ echo "selected"; } ?>>22</option>
        <option value="23" <?php if($value->day == "23"){ echo "selected"; } ?>>23</option>
        <option value="24" <?php if($value->day == "24"){ echo "selected"; } ?>>24</option>
        <option value="25" <?php if($value->day == "25"){ echo "selected"; } ?>>25</option>
        <option value="26" <?php if($value->day == "26"){ echo "selected"; } ?>>26</option>
        <option value="27" <?php if($value->day == "27"){ echo "selected"; } ?>>27</option>
        <option value="28" <?php if($value->day == "28"){ echo "selected"; } ?>>28</option>
        <option value="29" <?php if($value->day == "29"){ echo "selected"; } ?>>29</option>
        <option value="30" <?php if($value->day == "30"){ echo "selected"; } ?>>30</option>
        <option value="31" <?php if($value->day == "31"){ echo "selected"; } ?>>31</option>
        <option value="wd 1" <?php if($value->day == "wd 1"){ echo "selected"; } ?>>WD 1</option>
        <option value="wd 2" <?php if($value->day == "wd 2"){ echo "selected"; } ?>>WD 2</option>
        <option value="wd 3" <?php if($value->day == "wd 3"){ echo "selected"; } ?>>WD 3</option>
        <option value="wd 4" <?php if($value->day == "wd 4"){ echo "selected"; } ?>>WD 4</option>
        <option value="wd 5" <?php if($value->day == "wd 5"){ echo "selected"; } ?>>WD 5</option>
        <option value="wd 6" <?php if($value->day == "wd 6"){ echo "selected"; } ?>>WD 6</option>
        <option value="wd 7" <?php if($value->day == "wd 7"){ echo "selected"; } ?>>WD 7</option>
        <option value="wd 8" <?php if($value->day == "wd 8"){ echo "selected"; } ?>>WD 8</option>
        <option value="wd 9" <?php if($value->day == "wd 9"){ echo "selected"; } ?>>WD 9</option>
        <option value="wd 10" <?php if($value->day == "wd 10"){ echo "selected"; } ?>>WD 10</option>
        <option value="wd 11" <?php if($value->day == "wd 11"){ echo "selected"; } ?>>WD 11</option>
        <option value="wd 12" <?php if($value->day == "wd 12"){ echo "selected"; } ?>>WD 12</option>
        <option value="wd 13" <?php if($value->day == "wd 13"){ echo "selected"; } ?>>WD 13</option>
        <option value="wd 14" <?php if($value->day == "wd 14"){ echo "selected"; } ?>>WD 14</option>
        <option value="wd 15" <?php if($value->day == "wd 15"){ echo "selected"; } ?>>WD 15</option>
        <option value="wd 16" <?php if($value->day == "wd 16"){ echo "selected"; } ?>>WD 16</option>
        <option value="wd 17" <?php if($value->day == "wd 17"){ echo "selected"; } ?>>WD 17</option>
        <option value="wd 18" <?php if($value->day == "wd 18"){ echo "selected"; } ?>>WD 18</option>
        <option value="wd 19" <?php if($value->day == "wd 19"){ echo "selected"; } ?>>WD 19</option>
        <option value="wd 20" <?php if($value->day == "wd 20"){ echo "selected"; } ?>>WD 20</option>
        <option value="wd 21" <?php if($value->day == "wd 21"){ echo "selected"; } ?>>WD 21</option>
        <option value="wd 22" <?php if($value->day == "wd 22"){ echo "selected"; } ?>>WD 22</option>
        <option value="wd 23" <?php if($value->day == "wd 23"){ echo "selected"; } ?>>WD 23</option>
        <option value="wd 24" <?php if($value->day == "wd 24"){ echo "selected"; } ?>>WD 24</option>
        <option value="wd 25" <?php if($value->day == "wd 25"){ echo "selected"; } ?>>WD 25</option>
        </select>

        {!! Form::label('scheduled_time', 'Scheduled Time:') !!}

        <select class="form-control" name="scheduled_time">
        <?php
        $s_start = "00:00:00";
        $s_end = "23:45:00";
        $stStart = strtotime($s_start);
        $stEnd = strtotime($s_end);
        $sstime = strtotime($value->scheduled_time);
        $stNow = $stStart;
        $time_sc = date("H:i:s",$sstime);
        while($stNow <= $stEnd){
          $stime= date("H:i:s",$stNow);
          echo "<option value = '".$stime."' ";
          if($time_sc == $stime){
              echo "selected";
          }
          echo ">".$stime."</option>";
          $stNow = strtotime('+15 minutes',$stNow);
        }
        ?>
        </select>

        {!! Form::label('expected_complete_time', 'Expected Complete Time:') !!}

        <select class="form-control" name="expected_complete_time">
        <?php
        $e_start = "00:00:00";
        $e_end = "23:45:00";
        $eStart = strtotime($e_start);
        $eEnd = strtotime($e_end);
        $etime = strtotime($value->expected_complete_time);
        $eNow = $eStart;
        $e_time_sc = date("H:i:s",$etime);
        while($eNow <= $eEnd){
          $estime= date("H:i:s",$eNow);
          echo "<option value = '".$estime."' ";
          if($e_time_sc == $estime){
              echo "selected";
          }
          echo ">".$estime."</option>";
          $eNow = strtotime('+15 minutes',$eNow);
        }
        ?>
        </select>

        {!! Form::label('sla_time', 'SLA Time::') !!}

        <select class="form-control" name="sla_time">
        <?php
        $start = "00:00:00";
        $end = "23:45:00";
        $tStart = strtotime($start);
        $tEnd = strtotime($end);
        $stime = strtotime($value->sla_time);
        $tNow = $tStart;
        $time_sla = date("H:i:s",$stime);
        while($tNow <= $tEnd){
          $time= date("H:i:s",$tNow);
          echo "<option value = '".$time."' ";
          if($time_sla == $time){
              echo "selected";
          }
          echo ">".$time."</option>";
          $tNow = strtotime('+15 minutes',$tNow);
        }
        ?>
        </select>
        
        {!! Form::label('expire_time', 'Hours until report Expires (can be left blank to expire at end of calendar day):') !!}

        {!! Form::text('expire_time', $value->expire_time, array('class' => 'form-control')) !!}

        {!! Form::label('time_zone', 'Time Zone:') !!}
        <select class="form-control" type="text" name="time_zone">  
                                <option value="" <?php if($value->time_zone == ""){ echo "selected"; } ?>></option>
                                <option value='America/Los_Angeles' <?php if($value->time_zone == "America/Los_Angeles"){ echo "selected"; } ?>>America/Los_Angeles </option>
                                <option value='Europe/London' <?php if($value->time_zone == "Europe/London"){ echo "selected"; } ?>>Europe/London </option>
                                <option value='US/Mountain' <?php if($value->time_zone == "US/Mountain"){ echo "selected"; } ?>>US/Mountain </option>
                                <option value='US/Eastern' <?php if($value->time_zone == "US/Eastern"){ echo "selected"; } ?>>US/Eastern </option>
                                <option value='GMT' <?php if($value->time_zone == "GMT"){ echo "selected"; } ?>>GMT </option>
                                <option value='Europe/Bucharest' <?php if($value->time_zone == "Europe/Bucharest"){ echo "selected"; } ?>>Europe/Bucharest </option>
                                <option value='Europe/Istanbul' <?php if($value->time_zone == "Europe/Istanbul"){ echo "selected"; } ?>>Europe/Istanbul </option>
                                <option value="US/Alaska" <?php if($value->time_zone == "US/Alaska"){ echo "selected"; } ?>>US/Alaska</option>
                                <option value="US/Central" <?php if($value->time_zone == "US/Central"){ echo "selected"; } ?>>US/Central</option>
                                <option value="US/Eastern" <?php if($value->time_zone == "US/Eastern"){ echo "selected"; } ?>>US/Eastern</option>
                                <option value="US/Mountain" <?php if($value->time_zone == "US/Mountain"){ echo "selected"; } ?>>US/Mountain</option>
                                <option value="US/Pacific" <?php if($value->time_zone == "US/Pacific"){ echo "selected"; } ?>>US/Pacific</option>
                                <option value="US/Hawaii" <?php if($value->time_zone == "US/Hawaii"){ echo "selected"; } ?>>US/Hawaii</option>
                                <option value="Africa/Johannesburg" <?php if($value->time_zone == "Africa/Johannesburg"){ echo "selected"; } ?>>Africa/Johannesburg</option>
                                <option value="Asia/Manila" <?php if($value->time_zone == "Asia/Manila"){ echo "selected"; } ?>>Asia/Manila</option>
                                <option value="Asia/Tokyo" <?php if($value->time_zone == "Asia/Tokyo"){ echo "selected"; } ?>>Asia/Tokyo</option>
                                <option value="Asia/Hong_Kong" <?php if($value->time_zone == "Asia/Hong_Kong"){ echo "selected"; } ?>>Asia/Hong_Kong</option>
                                <option value="Asia/Kuala_Lumpur" <?php if($value->time_zone == "Asia/Kuala_Lumpur"){ echo "selected"; } ?>>Asia/Kuala_Lumpur</option>
                                <option value="Europe/Amsterdam" <?php if($value->time_zone == "Europe/Amsterdam"){ echo "selected"; } ?>>Europe/Amsterdam</option>
                                <option value="Europe/London" <?php if($value->time_zone == "Europe/London"){ echo "selected"; } ?>>Europe/London</option>
                                <option value="Europe/Istanbul" <?php if($value->time_zone == "Europe/Istanbul"){ echo "selected"; } ?>>Europe/Istanbul</option>
         </select>  
         <br /><br />
     {!! Form::submit('Save this Record', array('class' => 'btn btn-primary btn-md fa fa-check')) !!}
    <a href="{{ URL::to('reports/'.$value->rid.'/edit') }}" class="btn btn-success btn-sm"><i class="fa fa-close"></i> &nbsp; CANCEL</a>
{!! Form::hidden('_method', 'PUT') !!}
{!! Form::close() !!}
@endforeach
</div>
</div>
</div>
</div>
@endsection
