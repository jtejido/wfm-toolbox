@extends('autoreports::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-list-ul" aria-hidden="true"></i> Reports Management
    <a style="float: right;" href="{{ URL::to('reports') }}" class="btn btn-success btn-md"><i class="fa fa-undo"></i> Back to Reports</a>
    </h3>
@foreach($report as $key => $value)
<p>Edit: <i>{{ $value->name }} </i></p>
@endforeach
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<br /><br />
<div class="col-sm-5">
<div class="panel panel-default">
<div class="panel-heading"><h3>Report Details</h3></div>
<div class="panel-body">
@foreach($report as $key => $value_rep)
<form method="POST" action="{{ URL::to('reports/'.$value_rep->rid.'/put') }}">
{!! csrf_field() !!}

        {!! Form::label('rid', 'Report ID') !!}
        
        {!! Form::text('rid', $value_rep->rid, array('class' => 'form-control', 'readonly' => 'readonly')) !!}

        {!! Form::label('client', 'Name of Client for Report:') !!}

        <select class="form-control" id="client" name="client">
        <option value="">Not Listed</option>
        @foreach(session('clientdd') as $key => $value)
        <option value="{!! $value !!}" @if(($value_rep->client) == $value) selected @endif>{!! $value !!}</option>
        @endforeach
        </select>
        </br>
        <input class="form-control" name="client_not_listed" type="text" style="display: none" placeholder="Please type the Client here">

        

 
        @if ($is_template->client == "Template Report") 
            {!! Form::label('name', 'Name of Report:') !!}
            <input class="form-control" id="name" name="name" type="text" value="" style="display: none">
            <select class="form-control" name="updatetemplate">
                <option value=""></option>
                @foreach(session('template_dd') as $key => $value)
                <option value="{!! $value->template_name !!}"@if((($value_rep->client) == "Template Report") and (($value_rep->name) == $value->template_name)) selected @endif>{!! $value->template_name !!}</option>
                @endforeach
            </select>
        @else
            {!! Form::label('name', 'Name of Report:') !!}
            {!! Form::text('name', $value_rep->name, array('class' => 'form-control', 'id' => 'name')) !!}
            <select class="form-control" name="updatetemplate" style="display: none">
                <option value=""></option>
                @foreach(session('template_dd') as $key => $value)
                <option value="{!! $value->template_name !!}"@if((($value_rep->client) == "Template Report") and (($value_rep->name) == $value->template_name)) selected @endif>{!! $value->template_name !!}</option>
                @endforeach
            </select>
        @endif


        <input class="form-control" name="where_condition" type="text" style="display: none" placeholder="Please type the Where Statement">
        <input class="form-control" name="final_name" type="text" style="display: none" placeholder="Please type the Template Final Name">

        

        {!! Form::label('internal_external', 'Target Audience for Report:') !!}

        <select class="form-control" id="internal_external" name="internal_external">
        @foreach(session('internal_externaldd') as $key => $value)
            <option value="{!! $value['internal_external'] !!}" @if(($value_rep->internal_external) == $value['internal_external']) selected @endif>{!! $value['internal_external'] !!}</option>
        @endforeach
        </select>

        {!! Form::label('encryption', 'Password for Encryption:') !!}

        {!! Form::text('encryption', $value_rep->encryption, array('class' => 'form-control')) !!}

        {!! Form::label('isdateappended', 'Date Format for Attachment:') !!}

        <select class="form-control" id="isdateappended" name="isdateappended">
        @foreach(session('isdateappendeddd') as $key => $value)
            <option value="{!! $value['isdateappended'] !!}" @if(($value_rep->isdateappended) == $value['isdateappended']) selected @endif>{!! $value['isdateappended'] !!}</option>
        @endforeach
        </select>

        {!! Form::label('first_day_of_week', 'First Day of Week:') !!}

        <select class="form-control" id="first_day_of_week" name="first_day_of_week">
        @foreach(session('first_day_of_weekdd') as $key => $value)
            <option value="{!! $value['first_day_of_week'] !!}" @if(($value_rep->first_day_of_week) == $value['first_day_of_week']) selected @endif>{!! $value['first_day_of_week'] !!}</option>
        @endforeach
        </select>

        {!! Form::label('savewithmacro', 'Final File Type for Report:') !!}

        <select class="form-control" id="savewithmacro" name="savewithmacro">
        @foreach(session('savewithmacrodd') as $key => $value)
            <option value="{!! $value['savewithmacro'] !!}" @if(($value_rep->savewithmacro) == $value['savewithmacro']) selected @endif>{!! $value['savewithmacro'] !!}</option>
        @endforeach
        </select>

        {!! Form::label('zip_file_format', 'ZIP File Format:') !!}

        {!! Form::text('zip_file_format', $value_rep->zip_file_format, array('class' => 'form-control')) !!}

        {!! Form::label('deleteconnections', 'Remove Pivot Cache?:') !!}

        <select class="form-control" id="deleteconnections" name="deleteconnections">
        @foreach(session('deleteconnectionsdd') as $key => $value)
            <option value="{!! $value['deleteconnections'] !!}" @if(($value_rep->deleteconnections) == $value['deleteconnections']) selected @endif>{!! $value['deleteconnections'] !!}</option>
        @endforeach
        </select>

        {!! Form::label('nameofemailbodysheet', 'Sheet Name for Body Message:') !!}

        {!! Form::text('nameofemailbodysheet', $value_rep->nameofemailbodysheet, array('class' => 'form-control')) !!}

        {!! Form::label('custom_email_subject', 'Custom Email Subject:') !!}

        {!! Form::text('custom_email_subject', $value_rep->custom_email_subject, array('class' => 'form-control')) !!}

        {!! Form::label('customemailtext', 'Custom Email Text:') !!}

        {!! Form::text('customemailtext', $value_rep->customemailtext, array('class' => 'form-control')) !!}

        {!! Form::label('custom_attachment_name', 'Custom Attachment Name:') !!}

        {!! Form::text('custom_attachment_name', $value_rep->custom_attachment_name, array('class' => 'form-control')) !!}

        {!! Form::label('run_region', 'Run Region:') !!}

        <select class="form-control" id="run_region" name="run_region">
        @foreach(session('run_regiondd') as $key => $value)
            <option value="{!! $value['run_region'] !!}" @if(($value_rep->run_region) == $value['run_region']) selected @endif>{!! $value['run_region'] !!}</option>
        @endforeach
        </select>

        {!! Form::label('report_status', 'Report Status:') !!}

        <select class="form-control" id="report_status" name="report_status">
                <option value="Active" @if(($value_rep->report_status) == 'Active') selected @endif>Active</option>
                <option value="Manual" @if(($value_rep->report_status) == 'Manual') selected @endif>Manual</option>
                <option value="Paused" @if(($value_rep->report_status) == 'Paused') selected @endif>Paused</option>
                <option value="Retired" @if(($value_rep->report_status) == 'Retired') selected @endif>Retired</option>
                <option value="WIP" @if(($value_rep->report_status) == 'WIP') selected @endif>WIP</option>
                </select>

        {!! Form::label('validationtables', 'Validation IDs:') !!}

        {!! Form::text('validationtables', $value_rep->validationchecks, array('class' => 'form-control')) !!}

        {!! Form::submit('Save', array('class' => 'btn btn-primary btn-sm')) !!}
        <a class="btn btn-primary btn-md fa fa-play" title="Run" href="{{ URL::to('reports/'.$value_rep->rid.'/run') }}"> &nbsp; Run this Report</a>
        <a href="/tools/reports" class="btn btn-success btn-sm"><i class="fa fa-close"></i> &nbsp; CANCEL</a>
{!! Form::hidden('_method', 'PUT') !!}
{!! Form::close() !!}
@endforeach
</div>
</div>
</div>

<div class="col-sm-7">
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
                            <ul class="nav nav-tabs">

                                    @if ($is_template->client == "Template Report")
                                      <li class="active"><a href="#tab1default" data-toggle="tab">Report Template</a></li>
                                      <li><a href="#tab2default" data-toggle="tab">Recipients</a></li>
                                    @else 
                                      <li class="active"><a href="#tab2default" data-toggle="tab">Recipients</a></li>
                                    @endif

                                    <li><a href="#tab3default" data-toggle="tab">Frequency</a></li>
                            </ul>
    </div>
<div class="panel-body">
<div class="tab-content">

        @if ($is_template->client == "Template Report")
        <div class="tab-pane fade in active" id="tab1default">
                <div class="container-fluid">
                <small style="float: right;"><i>Columns marked with * are editable</i></small>
                <br />
                <div class="widget-box">
                    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
                      <h5>Reports using this Template</h5>
                    </div>
                    <div class="widget-content" >
                    <div class="row-fluid">
                        <table class="table-striped table-condensed" id="templates">
                            <thead>
                                <tr>
                                    <th>Name*</th>
                                    <th>Where Condition*</th>
                                    <th>Template Name</th>

                                </tr>
                            </thead>
                            <tbody>
                            @foreach($report_templates as $key => $value)
                                <tr>
                                    <td><a href="#" class="name" data-type="text" data-column="name" data-url="/tools/report_template/update/{{ $value->rid }}" data-pk="{{$value->rid}}" data-title="change" data-name="name">{{ $value->name }}</a></td>
                                    <td><a href="#" class="where_condition" data-type="text" data-column="where_condition" data-url="/tools/report_template/update/{{ $value->rid }}" data-pk="{{$value->rid}}" data-title="change" data-name="where_condition">{{ $value->where_condition }}</a></td>
                                    <td>{{ $value->template }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                </div>
        </div>
        @endif




        @if ($is_template->client != "Template Report")
         <div class="tab-pane fade in active" id="tab2default">
        @else
         <div class="tab-pane fade" id="tab2default">
        @endif
  
            <div class="container-fluid">
            <small style="float: right;"><i>Columns marked with * are editable</i></small>
            <br />
                <div class="widget-box">
                    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
                      <h5>Recipients of the Report </h5><button type="button" href="#" class="buttons btn btn-default btn-sm" data-toggle="modal" data-target="#create-report">Add Recipient</button>
                    </div>
                    <div class="widget-content" >
                    <div class="row-fluid">
                        <table class="table-striped table-condensed" id="recipients">
                            <thead>
                                <tr>
                                    <th>Email_Address</th>
                                    <th>Name</th>
                                    <th>Email_type*</th>
                                    <th>To_cc_bcc*</th>
                                    <th>Start_dte*</th>
                                    <th>End_dte*</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($report_recipient as $key => $value)
                                <tr>
                                    <td>{{ $value->email_address }}</td>
                                    <td>{{ $value->name }}</td>
                                    <td><a href="#" class="email_type" data-column="email_type" data-url="/tools/report_recipient/update/{{ $value->rid }}/{{ $value->recipient_id }}/{{ $value->email_type }}" data-pk="{{$value->rid}}" data-title="change" data-name="email_type" data-value="{{$value->email_type}}">{{ $value->email_type }}</a></td>
                                    <td><a href="#" class="to_cc_bcc" data-column="to_cc_bcc" data-url="/tools/report_recipient/update/{{ $value->rid }}/{{ $value->recipient_id }}/{{ $value->email_type }}" data-pk="{{$value->rid}}" data-title="change" data-name="to_cc_bcc" data-value="{{$value->to_cc_bcc}}">{{ $value->to_cc_bcc }}</a></td>
                                    <td><a href="#" class="start_dte" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="YYYY/MM/DD" data-template="YYYY / MMM / D" data-column="start_dte" data-url="/tools/report_recipient/update/{{ $value->rid }}/{{ $value->recipient_id }}/{{ $value->email_type }}" data-pk="{{$value->rid}}" data-title="change" data-name="start_dte" data-value="{{ $value->start_dte }}">{{ $value->start_dte }}</a></td>
                                    <td><a href="#" class="end_dte" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="YYYY/MM/DD" data-template="YYYY / MMM / D" data-column="end_dte" data-url="/tools/report_recipient/update/{{ $value->rid }}/{{ $value->recipient_id }}/{{ $value->email_type }}" data-pk="{{$value->rid}}" data-title="change" data-name="end_dte" data-value="{{ $value->end_dte }}">{{ $value->end_dte }}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                </div>
        </div>

        <div class="tab-pane fade" id="tab3default">
            <div class="widget-box">
                    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
                      <h5>Recipients of the Report </h5><button type="button" href="#" class="buttons btn btn-default btn-sm" data-toggle="modal" data-target="#add-frequency">Add Frequency</button>
                    </div>
                    <div class="widget-content" >
                    <div class="row-fluid">
                        <table class="table-striped table-condensed" id="frequency">
                            <thead>
                                <tr>
                                    <th>Day</th>
                                    <th>Scheduled Time</th>
                                    <th>SLA Time</th>
                                    <th>Timezone</th>
                                    <th>Expected Complete Time</th>
                                    <th>Expire Time</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($frequency as $key => $value)
                                <tr>
                                    <td>{{ $value->day }}</td>
                                    <td>{{ $value->scheduled_time }}</td>
                                    <td>{{ $value->sla_time }}</td>
                                    <td>{{ $value->time_zone }}</td>
                                    <td>{{ $value->expected_complete_time }}</td>
                                    <td>{{ $value->expire_time }}</td>
                                    <td>
                                        <ul class="list-inline">
                                        <li>
                                            <a class="btn btn-primary btn-sm fa fa-pencil-square-o" href="{{ URL::to('frequency/'.$value->rid.'/'.$value->day.'/'.$value->scheduled_time.'/edit') }}"> Edit</a>
                                        </li>
                                        <li>
                                            <form method="POST" action="{{ URL::to('frequency/'.$value->rid.'/'.$value->day.'/'.$value->scheduled_time.'/delete') }}">
                                            {!! csrf_field() !!}
                                            <button type="submit" class="btn btn-primary btn-sm fa fa-trash"> Delete</button>
                                            <input name="_method" type="hidden" value="DELETE">
                                            </form>
                                        </li>
                                        </ul>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                </div>
        </div>
</div>
</div>
</div>
</div>
</div>

<div id="add-frequency" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">

                    <form method="POST" action="{{ URL::to('reports/'.$report_rid.'/edit/frequency') }}" id="add_frequency" name="frequency">
                    {!! csrf_field() !!}

                                    <label>RID:</label> <input class="form-control" type="text" name="newrid" value="{!! $report_rid !!}" readonly>

                                    <label>Day:</label> <select class="form-control" id="days" name="newday[]" multiple>
                                                                    <option value="monday">Monday</option>
                                                                    <option value="tuesday">Tuesday</option>
                                                                    <option value="wednesday">Wednesday</option>
                                                                    <option value="thursday">Thursday</option>
                                                                    <option value="friday">Friday</option>
                                                                    <option value="saturday">Saturday</option>
                                                                    <option value="sunday">Sunday</option>
                                                                    <option value="01">01</option>
                                                                    <option value="02">02</option>
                                                                    <option value="03">03</option>
                                                                    <option value="04">04</option>
                                                                    <option value="05">05</option>
                                                                    <option value="06">06</option>
                                                                    <option value="07">07</option>
                                                                    <option value="08">08</option>
                                                                    <option value="09">09</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                    <option value="19">19</option>
                                                                    <option value="20">20</option>
                                                                    <option value="21">21</option>
                                                                    <option value="22">22</option>
                                                                    <option value="23">23</option>
                                                                    <option value="24">24</option>
                                                                    <option value="25">25</option>
                                                                    <option value="26">26</option>
                                                                    <option value="27">27</option>
                                                                    <option value="28">28</option>
                                                                    <option value="29">29</option>
                                                                    <option value="30">30</option>
                                                                    <option value="31">31</option>
                                                                    <option value="wd 1">WD 1</option>
                                                                    <option value="wd 2">WD 2</option>
                                                                    <option value="wd 3">WD 3</option>
                                                                    <option value="wd 4">WD 4</option>
                                                                    <option value="wd 5">WD 5</option>
                                                                    <option value="wd 6">WD 6</option>
                                                                    <option value="wd 7">WD 7</option>
                                                                    <option value="wd 8">WD 8</option>
                                                                    <option value="wd 9">WD 9</option>
                                                                    <option value="wd 10">WD 10</option>
                                                                    <option value="wd 11">WD 11</option>
                                                                    <option value="wd 12">WD 12</option>
                                                                    <option value="wd 13">WD 13</option>
                                                                    <option value="wd 14">WD 14</option>
                                                                    <option value="wd 15">WD 15</option>
                                                                    <option value="wd 16">WD 16</option>
                                                                    <option value="wd 17">WD 17</option>
                                                                    <option value="wd 18">WD 18</option>
                                                                    <option value="wd 19">WD 19</option>
                                                                    <option value="wd 20">WD 20</option>
                                                                    <option value="wd 21">WD 21</option>
                                                                    <option value="wd 22">WD 22</option>
                                                                    <option value="wd 23">WD 23</option>
                                                                    <option value="wd 24">WD 24</option>
                                                                    <option value="wd 25">WD 25</option>
                                                                    </select>

                                    <label>Scheduled Time:</label> <select id="newsched" class="form-control" name="newsched">
                                    <?php
                                        $s_start = "00:00:00";
                                        $s_end = "23:45:00";
                                        $stStart = strtotime($s_start);
                                        $stEnd = strtotime($s_end);
                                        $stNow = $stStart;
                                        while($stNow <= $stEnd){
                                          $stime= date("H:i:s",$stNow);
                                          echo "<option value = '".$stime."'>".$stime."</option>";
                                          $stNow = strtotime('+15 minutes',$stNow);
                                        }
                                        ?>
                                    </select>

                                    <label>Expected Complete Time:</label> <select id="newect" class="form-control" name="newect">
                                    <?php
                                        $e_start = "00:00:00";
                                        $e_end = "23:45:00";
                                        $eStart = strtotime($e_start);
                                        $eEnd = strtotime($e_end);
                                        $eNow = $eStart;
                                        while($eNow <= $eEnd){
                                          $estime= date("H:i:s",$eNow);
                                          echo "<option value = '".$estime."'>".$estime."</option>";
                                          $eNow = strtotime('+15 minutes',$eNow);
                                        }
                                        ?>
                                    </select>
                                    <label>SLA Time:</label> <select id="newsla" class="form-control" name="newsla">
                                        <?php
                                            $start = "00:00:00";
                                            $end = "23:45:00";
                                            $tStart = strtotime($start);
                                            $tEnd = strtotime($end);
                                            $tNow = $tStart;
                                            while($tNow <= $tEnd){
                                              $time= date("H:i:s",$tNow);
                                              echo "<option value = '".$time."'>".$time."</option>";
                                              $tNow = strtotime('+15 minutes',$tNow);
                                            }
                                        ?>
                                    </select>
                                    <label>Hours until report Expires (can be left blank to expire at end of calendar day):</label> <input class="form-control" type="text" name="newexp" id="expire_time" value="" placeholder="HH:MM:SS" />

                                <label>Time Zone:</label> <select id="newtz" class="form-control" type="text" name="newtz">
                                <option value=""></option>
                                <option value='America/Los_Angeles'>America/Los_Angeles </option>
                                <option value='Europe/London'>Europe/London </option>
                                <option value='US/Mountain'>US/Mountain </option>
                                <option value='US/Eastern'>US/Eastern </option>
                                <option value='GMT'>GMT </option>
                                <option value='Europe/Bucharest'>Europe/Bucharest </option>
                                <option value='Europe/Istanbul'>Europe/Istanbul </option>
                                <option value="US/Alaska">US/Alaska</option>
                                <option value="US/Central">US/Central</option>
                                <option value="US/Eastern">US/Eastern</option>
                                <option value="US/Mountain">US/Mountain</option>
                                <option value="US/Pacific">US/Pacific</option>
                                <option value="US/Hawaii">US/Hawaii</option>
                                <option value="Africa/Johannesburg">Africa/Johannesburg</option>
                                <option value="Asia/Manila">Asia/Manila</option>
                                <option value="Asia/Tokyo">Asia/Tokyo</option>
                                <option value="Asia/Hong_Kong">Asia/Hong_Kong</option>
                                <option value="Asia/Kuala_Lumpur">Asia/Kuala_Lumpur</option>
                                <option value="Europe/Amsterdam">Europe/Amsterdam</option>
                                <option value="Europe/London">Europe/London</option>
                                <option value="Europe/Istanbul">Europe/Istanbul</option>
                                </select>   
    <br /><br /><br />                           
    {!! Form::submit('Add Frequency', array('class' => 'btn-primary','form' => 'add_frequency')) !!}
    {!! Form::close() !!}
    </div>
</div>
</div>
</div>
</div>

<div id="create-report" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">


                    <form method="POST" action="{{ URL::to('reports/'.$report_rid.'/edit/report_recipient') }}" id="recipient">
                    {!! csrf_field() !!}
 
                    <div class="row">
                    <div class="col-sm-2" style="padding:0px !important;">
                    <p>RID</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>Recipient's Name</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>Email Type</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>TO/CC/BCC</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>Start Date</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>End Date</p>
                    </div>
                    </div>
            <div id="entry1" class="clonedInput">
            <div class="row">
                <h6 id="reference" name="reference" class="heading-reference" style="color:#000 !important;"><b>Recipient 1</b></h6>
                <div class="col-sm-2" style="padding:0px !important;">
                 <input class="input_rid form-control" type="text" name="ID1_rid" id="recip_next_rid" value="{!! $report_rid !!}" placeholder="rid">
                 </div>

                <div class="ui-widget">
                <div class="col-sm-2">
                 <input class="input_recipient form-control" type="text" id="ID1_recipient" name="ID1_recipient" placeholder="recipient name">
                 <input type="hidden" class="input_recipient_id" id="ID1_recipient_id" name="ID1_recipient_id">
                 </div>
                 </div>
                  
                <div class="col-sm-2">
                    <select class="select_eml form-control" name="ID1_email_type" id="email_type">
                        <option value="" selected="selected" disabled="disabled">
                            Email type
                        </option>
                        <option value="Body">Body</option>
                        <option value="Body & Attachment">Body & Attachment</option>
                        <option value="Link">Link</option>
                        <option value="Internal Attachment">Internal Attachment</option>
                        <option value="External Attachment">External Attachment</option>
                        </select><!-- end .select_ttl -->
                </div>
                <div class="col-sm-2">
                    <select class="select_tcb form-control" name="ID1_to_cc_bcc" id="to_cc_bcc">
                        <option value="" selected="selected" disabled="disabled">
                            To_CC_BCC
                        </option>
                        <option value="to">To</option>
                        <option value="cc">CC</option>
                        <option value="bcc">BCC</option>
                        </select><!-- end .select_ttl -->
                </div>
                <div class="col-sm-2">
                <input class="input_sd form-control" type="date" name="ID1_start_dte" id="start_dte" value="" placeholder="start_dte">
                </div>
                <div class="col-sm-2">
                <input class="input_ed form-control" type="date" name="ID1_end_dte" id="end_dte" value="" placeholder="end_dte">
                </div>

            </div>
            <!-- end #entry1 -->
            <br /><br />
            </div>

            <div class="btn-group" id="addDelButtons">
                <input class="btn btn-success btn-md" type="button" id="btnAdd" value="Add Recipient">
                <input class="btn btn-success btn-md" type="button" id="btnDel" value="Remove Recipient above">
            </div>
            <br /><br /><br /><br />
    {!! Form::submit('Add Recipient', array('class' => 'btn-primary','form' => 'recipient', 'id' => 'export-btn')) !!}
    {!! Form::close() !!}
    </div>

</div>
</div>
</div>
</div>
@endsection
@section('js')

<script>
$('#days').multiSelect();


    function submitform() {
    var y = document.forms["frequency"]["newtz"].value;
    if (y == null || y == "") {
        alertify.alert("Timezone must be selected");
        return false;
    }
    else {
        return true;
    }
    }

function submitform2() {
    var date = document.getElementById("expire_time").value;
    if (date.match(/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/) || date == null || date == "") {
        return true;
    } else {
        alertify.alert("Invalid Expire Time format: Expire Time should be in HH:MM:SS format.");
        return false;
    }
    }

function validateForm() {
return !!(submitform() & submitform2());
form.submit();
}

$('select[name=client]').on('change', function(){
    var input = document.getElementById("name");
    if($(this).val() == "Template Report"){
        $('input[name=final_name]').show();
        $('input[name=where_condition]').show();
        $('input[name=name]').hide().val(input.value);
    }else {
        $('input[name=final_name]').hide().val('');
        $('input[name=where_condition]').hide().val('');
        $('input[name=name]').show().val(input.value);
    }
});

$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.name').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.where_condition').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });


    $('.email_type').editable({
        type: 'select',
        source: [
            {value: 'Body', text: 'Body'},
            {value: 'Body & Attachment', text: 'Body & Attachment'},
            {value: 'Link', text: 'Link'},
            {value: 'Internal Attachment', text: 'Internal Attachment'},
            {value: 'External Attachment', text: 'External Attachment'}
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.to_cc_bcc').editable({
        type: 'select',
        source: [
            {value: 'to', text: 'To'},
            {value: 'cc', text: 'CC'},
            {value: 'bcc', text: 'BCC'}
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });
    $('.start_dte').editable({
        combodate: { maxYear: 2018 },
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.end_dte').editable({
        combodate: { maxYear: 2018 },
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });   
});
$(document).ready(function(){
    $('#recipients').DataTable( {
        "stateSave": false,
        "lengthMenu": [[5, 25, 50, 75, -1], [5, 25, 50, 75, "All"]],
        "pageLength": 5
} );
});
$(document).ready(function(){
    $('#frequency').DataTable( {
        "stateSave": false,
        "lengthMenu": [[5, 25, 50, 75, -1], [5, 25, 50, 75, "All"]],
        "pageLength": 5
} );
});
$(document).ready(function(){
    $('#templates').DataTable( {
        "stateSave": false,
        "lengthMenu": [[5, 25, 50, 75, -1], [5, 25, 50, 75, "All"]],
        "pageLength": 5
} );
});

$( function() {
    $("#ID1_recipient").autocomplete({
          scroll: true,
          autoFocus: true,
          minLength: 3,
          appendTo: "#create-report",
          source: function( request, response ) {
            $.ajax({
              url: "{{ URL::to('reports/searchrecipient') }}",
              dataType: "json",
              data: {
                  searchText: request.term
              },
              success: function( data ) {
               response($.map(data, function(item) {
                    return {
                        label: item.name,
                        value: item.id
                    };
                }));
              }
            });
          },
          select: function( event, ui) {
            $(this).val(ui.item.label);
            $("#ID1_recipient_id").val(ui.item.value);
            return false;
          },
          change: function (e, ui) {
                if (!(ui.item)) e.target.value = "";
          }
    } );
} );
 </script>
@endsection