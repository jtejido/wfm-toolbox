@extends('autoreports::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-list-ul" aria-hidden="true"></i> Reports Management</h3>
    <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#create-report">Create Record</button>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
<div class="row-fluid">
{!! $html->table(['class' => 'table-bordered table-condensed table-hover'], true) !!}
</div>
</div>
</div>
</div>
<!-- Copy Report Modal -->
<div id="copyreport" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Which Setup do you want to copy?</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="get" action="{{ URL::to('reports/copy/copy') }}"> 
                        {!! csrf_field() !!}
                                    <div class="row">
                                        <input type="checkbox" name="copy1" value="Report"/> Report Details 
                                        <input type="checkbox" name="copy2" value="Recipients"/> Report Recipients 
                                        <input type="checkbox" name="copy3" value="Frequency"/> Frequency 
                                    </div><br /><br />


                                <input type="hidden" name="rid" id="rid"/>
                                <input type="hidden" name="rid_new" value="{{session('next_rid')}}"/>
                            <button class="btn btn-primary btn-lg" type="submit"> Copy</button>
                        </form>  
                    </div>
            </div>
</div>
</div>
</div>
<!-- Create Modal -->
<div id="create-report" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Report</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                    <form method="POST" action="{{ URL::to('reports') }}" accept-charset="UTF-8" id="add_report" name="report">
                    {!! csrf_field() !!}
                                            <div id = "add_report">
                                            <h3>Report Details</h3>
                                            <section>
                                            <div class="form-group">
                                                <strong>{!! Form::label('rid', 'Report ID') !!}</strong>
                                                {!! Form::text('rid', session('next_rid'), array('class' => 'form-control', 'readonly' => 'readonly')) !!}
                                            </div>

                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Select the client's name or enter a new client's name if it does not exist.">
                                                <strong> {!! Form::label('client', 'Name of Client for Report:') !!}</strong>
                                                </span>
                                                <select class="form-control" id="client" name="client">
                                                <option value="">Not Listed</option>
                                                @foreach(session('clientdd') as $key => $value)
                                                <option value="{!! $value !!}">{!! $value !!}</option>
                                                @endforeach
                                                </select>
                                                </br>
                                                <input class="form-control" name="client_not_listed" type="text" style="display: none;" placeholder="Please type the Client here">
                                             </div>
                                            
                                            <div class="form-group">
                                                <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Enter the name of the report in the format of Date Type - Client Name - Report Name. Example: Daily - Google Cost Report">
                                                <strong>{!! Form::label('name', 'Report Name') !!}</strong>
                                                </span>
                                                {!! Form::text('name', null, array('class' => 'form-control')) !!}
                                            <select class="form-control" name="updatetemplate" style="display: none">
                                                <option value=""></option>
                                                @foreach(session('template_dd') as $key => $value)
                                                <option value="{!! $value->template_name !!}">{!! $value->template_name !!}</option>
                                                @endforeach
                                            </select>
                                                
                                            </div>
                                            
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Select the audience (Internal Xerox employees, or External if this report is meant to be used by clients as well).">
                                                <strong> {!! Form::label('internal_external', 'Target Audience for Report:') !!}</strong>
                                                </span>
                                                <select class="form-control" id="internal_external" name="internal_external">
                                                @foreach(session('internal_externaldd') as $key => $value)
                                                <option value="{!! $value['internal_external'] !!}">{!! $value['internal_external'] !!}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="If this report needs to be ZIP'd & encrypted, please enter the password you'd like to use for the file.">
                                                <strong> {!! Form::label('encryption', 'Password for Encryption:') !!}</strong>
                                                </span>{!! Form::text('encryption', null, array('class' => 'form-control')) !!}
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Select the date format for the attachment.">
                                                <strong> {!! Form::label('isdateappended', 'Date Format for Attachment:') !!}</strong>
                                                </span>
                                                <select class="form-control" id="isdateappended" name="isdateappended">
                                                @foreach(session('isdateappendeddd') as $key => $value)
                                                <option value="{!! $value['isdateappended'] !!}">{!! $value['isdateappended'] !!}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Select the first day of week for weekly reports. If this is not a weekly report, leave it as Monday.">
                                                <strong> {!! Form::label('first_day_of_week', 'First Day of Week:') !!}</strong>
                                                </span>
                                                <select class="form-control" id="first_day_of_week" name="first_day_of_week">
                                                <option value="monday">Monday</option>
                                                <option value="sunday">Sunday</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Select the final file type for this report.">
                                                <strong> {!! Form::label('savewithmacro', 'Final File Type for Report:') !!}</strong>
                                                </span>
                                                <select class="form-control" id="savewithmacro" name="savewithmacro">
                                                @foreach(session('savewithmacrodd') as $key => $value)
                                                <option value="{!! $value['savewithmacro'] !!}">{!! $value['savewithmacro'] !!}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="If the file is ZIP'd, please enter the file format for the attachment inside of the ZIP for the report. (this should match the Final File Type)">
                                               <strong> {!! Form::label('zip_file_format', 'ZIP File Format:') !!}</strong>
                                                </span>{!! Form::text('zip_file_format', null, array('class' => 'form-control')) !!}
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="If this report needs to retain pivot functions (i.e. expand / collapse) select True.">
                                                <strong> {!! Form::label('deleteconnections', 'Remove Pivot Cache?:') !!}</strong>
                                                </span>
                                                <select class="form-control" id="deleteconnections" name="deleteconnections">
                                                @foreach(session('deleteconnectionsdd') as $key => $value)
                                                <option value="{!! $value['deleteconnections'] !!}">{!! $value['deleteconnections'] !!}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Enter the sheet name of the excel file that should be used optionally as the email body.">
                                                <strong> {!! Form::label('nameofemailbodysheet', 'Sheet Name for Body Message:') !!}</strong>
                                                </span>{!! Form::text('nameofemailbodysheet', null, array('class' => 'form-control')) !!}
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Optionally change the subject of the email that is sent.">
                                                <strong>  {!! Form::label('customemailtext', 'Custom Email Text:') !!}</strong>
                                                </span>{!! Form::text('customemailtext', null, array('class' => 'form-control')) !!}
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Optionally change the name of the final attachment saved.">
                                                <strong> {!! Form::label('custom_attachment_name', 'Custom Attachment Name:') !!}</strong>
                                                </span>{!! Form::text('custom_attachment_name', null, array('class' => 'form-control')) !!}
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Select the Region that will process this report.">
                                                <strong>{!! Form::label('run_region', 'Run Region:') !!}</strong>
                                                </span>
                                                <select class="form-control" id="run_region" name="run_region">
                                                @foreach(session('run_regiondd') as $key => $value)
                                                <option value="{!! $value['run_region'] !!}">{!! $value['run_region'] !!}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Select the current status for this report.">
                                                 <strong>{!! Form::label('report_status', 'Report Status:') !!}</strong>
                                                 </span>
                                                 <select class="form-control" id="report_status" name="report_status">
                                                @foreach(session('report_statusdd') as $key => $value)
                                                <option value="{!! $value['report_status'] !!}">{!! $value['report_status'] !!}</option>
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                 <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Enter as comma seporated values the appropriate obj_id values.">
                                                 <strong>{!! Form::label('validationtables', 'Validation IDs:') !!}</strong>
                                                 </span>{!! Form::text('validationtables', null, array('class' => 'form-control')) !!}
                                            </div>
                                            </section>
                    <h3>Report Recipients</h3>
                    <section>
                    <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#create-recipient">Add Recipient Record</button><br/><br/>
                    <div class="row">
                    <div class="col-sm-2" style="padding:0px !important;">
                    <p>RID</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>Recipient's Name</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>Email Type</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>TO/CC/BCC</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>Start Date</p>
                    </div>
                    <div class="col-sm-2" style="padding-left:1em !important;">
                    <p>End Date</p>
                    </div>
                    </div>
            <div id="entry1" class="clonedInput">
            <div class="row">
                <h6 id="reference" name="reference" class="heading-reference" style="color:#000 !important;"><b>Recipient 1</b></h6>
                <div class="col-sm-2" style="padding:0px !important;">
                 <input class="input_rid form-control" type="text" name="ID1_rid" id="recip_next_rid" value="{!! session('next_rid') !!}" placeholder="rid">
                 </div>

                 <div class="ui-widget">
                <div class="col-sm-2">
                 <input class="input_recipient form-control" type="text" id="ID1_recipient" name="ID1_recipient" placeholder="recipient name">
                 <input type="hidden" class="input_recipient_id" id="ID1_recipient_id" name="ID1_recipient_id">
                 </div>
                 </div>
                 
                <div class="col-sm-2">
                    <select class="select_eml form-control" name="ID1_email_type" id="email_type">
                        <option value="" selected="selected" disabled="disabled">
                            Email type
                        </option>
                        <option value="Body">Body</option>
                        <option value="Body & Attachment">Body & Attachment</option>
                        <option value="Link">Link</option>
                        <option value="Internal Attachment">Internal Attachment</option>
                        <option value="External Attachment">External Attachment</option>
                        </select><!-- end .select_ttl -->
                </div>
                <div class="col-sm-2">
                    <select class="select_tcb form-control" name="ID1_to_cc_bcc" id="to_cc_bcc">
                        <option value="" selected="selected" disabled="disabled">
                            To_CC_BCC
                        </option>
                        <option value="to">To</option>
                        <option value="cc">CC</option>
                        <option value="bcc">BCC</option>
                        </select><!-- end .select_ttl -->
                </div>
                <div class="col-sm-2">
                <input class="input_sd form-control" type="date" name="ID1_start_dte" id="start_dte" value="" placeholder="start_dte">
                </div>
                <div class="col-sm-2">
                <input class="input_ed form-control" type="date" name="ID1_end_dte" id="end_dte" value="" placeholder="end_dte">
                </div>

            </div>
            <!-- end #entry1 -->
            <br /><br />
            </div>

            <div class="btn-group" id="addDelButtons">
                <input class="btn btn-success btn-md" type="button" id="btnAdd" value="Add Recipient">
                <input class="btn btn-success btn-md" type="button" id="btnDel" value="Remove Recipient above">
            </div>

                    </section>

            <h3>Report Frequency</h3>
                    <section>
                        <table>
                                <tr>
                                    <td>RID:</td> <td><input class="form-control" type="text" name="newrid" value="{!!session('next_rid')!!}" readonly></td>
                                </tr>
                                <tr>
                                    <td>Day:</td> <td>
        
                                                                <select class="form-control" id="days" name="newday[]" multiple>
                                                                <optgroup label="Days">
                                                                    <option value="monday">Monday</option>
                                                                    <option value="tuesday">Tuesday</option>
                                                                    <option value="wednesday">Wednesday</option>
                                                                    <option value="thursday">Thursday</option>
                                                                    <option value="friday">Friday</option>
                                                                    <option value="saturday">Saturday</option>
                                                                    <option value="sunday">Sunday</option>
                                                                </optgroup>
                                                                <optgroup label="Month Days">
                                                                    <option value="01">01</option>
                                                                    <option value="02">02</option>
                                                                    <option value="03">03</option>
                                                                    <option value="04">04</option>
                                                                    <option value="05">05</option>
                                                                    <option value="06">06</option>
                                                                    <option value="07">07</option>
                                                                    <option value="08">08</option>
                                                                    <option value="09">09</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                    <option value="19">19</option>
                                                                    <option value="20">20</option>
                                                                    <option value="21">21</option>
                                                                    <option value="22">22</option>
                                                                    <option value="23">23</option>
                                                                    <option value="24">24</option>
                                                                    <option value="25">25</option>
                                                                    <option value="26">26</option>
                                                                    <option value="27">27</option>
                                                                    <option value="28">28</option>
                                                                    <option value="29">29</option>
                                                                    <option value="30">30</option>
                                                                    <option value="31">31</option>
                                                                </optgroup>
                                                                <optgroup label="Week Days">
                                                                    <option value="wd 1">WD 1</option>
                                                                    <option value="wd 2">WD 2</option>
                                                                    <option value="wd 3">WD 3</option>
                                                                    <option value="wd 4">WD 4</option>
                                                                    <option value="wd 5">WD 5</option>
                                                                    <option value="wd 6">WD 6</option>
                                                                    <option value="wd 7">WD 7</option>
                                                                    <option value="wd 8">WD 8</option>
                                                                    <option value="wd 9">WD 9</option>
                                                                    <option value="wd 10">WD 10</option>
                                                                    <option value="wd 11">WD 11</option>
                                                                    <option value="wd 12">WD 12</option>
                                                                    <option value="wd 13">WD 13</option>
                                                                    <option value="wd 14">WD 14</option>
                                                                    <option value="wd 15">WD 15</option>
                                                                    <option value="wd 16">WD 16</option>
                                                                    <option value="wd 17">WD 17</option>
                                                                    <option value="wd 18">WD 18</option>
                                                                    <option value="wd 19">WD 19</option>
                                                                    <option value="wd 20">WD 20</option>
                                                                    <option value="wd 21">WD 21</option>
                                                                    <option value="wd 22">WD 22</option>
                                                                    <option value="wd 23">WD 23</option>
                                                                    <option value="wd 24">WD 24</option>
                                                                    <option value="wd 25">WD 25</option>
                                                                </optgroup>
                                                                </select>
                                                    </td>
                                </tr>
                                <tr>
                                    <td>Scheduled Time:</td> <td>
                                    <select id="newsched" class="form-control" name="newsched">
                                    <?php
                                        $s_start = "00:00:00";
                                        $s_end = "23:45:00";
                                        $stStart = strtotime($s_start);
                                        $stEnd = strtotime($s_end);
                                        $stNow = $stStart;
                                        while($stNow <= $stEnd){
                                          $stime= date("H:i:s",$stNow);
                                          echo "<option value = '".$stime."'>".$stime."</option>";
                                          $stNow = strtotime('+15 minutes',$stNow);
                                        }
                                        ?>
                                    </select>
                                    </td>
                                </tr>
                                        <tr>
                                    <td>Expected Complete Time:</td> <td>
                                    <select id="newect" class="form-control" name="newect">
                                    <?php
                                        $e_start = "00:00:00";
                                        $e_end = "23:45:00";
                                        $eStart = strtotime($e_start);
                                        $eEnd = strtotime($e_end);
                                        $eNow = $eStart;
                                        while($eNow <= $eEnd){
                                          $estime= date("H:i:s",$eNow);
                                          echo "<option value = '".$estime."'>".$estime."</option>";
                                          $eNow = strtotime('+15 minutes',$eNow);
                                        }
                                        ?>
                                    </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>SLA Time:</td> <td>
                                    <select id="newsla" class="form-control" name="newsla">
                                        <?php
                                            $start = "00:00:00";
                                            $end = "23:45:00";
                                            $tStart = strtotime($start);
                                            $tEnd = strtotime($end);
                                            $tNow = $tStart;
                                            while($tNow <= $tEnd){
                                              $time= date("H:i:s",$tNow);
                                              echo "<option value = '".$time."'>".$time."</option>";
                                              $tNow = strtotime('+15 minutes',$tNow);
                                            }
                                        ?>
                                    </select>
                                    </td>
                                </tr>
                                        <tr>
                                    <td>Hours until report Expires (can be left blank to expire at end of calendar day):</td> <td>
                                    <input class="form-control" type="text" name="newexp" id="expire_time" value="" placeholder="HH:MM:SS">
                                    </td>
                                </tr>
                                <tr>
                                <td>Time Zone:</td> <td>
                                <select id="newtz" class="form-control" type="text" name="newtz">
                                <option value=''></option>
                                <option value='America/Los_Angeles'>America/Los_Angeles </option>
                                <option value='Europe/London'>Europe/London </option>
                                <option value='US/Mountain'>US/Mountain </option>
                                <option value='US/Eastern'>US/Eastern </option>
                                <option value='GMT'>GMT </option>
                                <option value='Europe/Bucharest'>Europe/Bucharest </option>
                                <option value='Europe/Istanbul'>Europe/Istanbul </option>
                                <option value="US/Alaska">US/Alaska</option>
                                <option value="US/Central">US/Central</option>
                                <option value="US/Eastern">US/Eastern</option>
                                <option value="US/Mountain">US/Mountain</option>
                                <option value="US/Pacific">US/Pacific</option>
                                <option value="US/Hawaii">US/Hawaii</option>
                                <option value="Africa/Johannesburg">Africa/Johannesburg</option>
                                <option value="Asia/Manila">Asia/Manila</option>
                                <option value="Asia/Tokyo">Asia/Tokyo</option>
                                <option value="Asia/Hong_Kong">Asia/Hong_Kong</option>
                                <option value="Asia/Kuala_Lumpur">Asia/Kuala_Lumpur</option>
                                <option value="Europe/Amsterdam">Europe/Amsterdam</option>
                                <option value="Europe/London">Europe/London</option>
                                <option value="Europe/Istanbul">Europe/Istanbul</option>
                                </select>   
                                </td>
                                </tr>
                        </table>
                    </section>
                    <h3>Where Statement</h3>
                    <section>
                   <div id="template_yes" style="display: none;">
                        <div class="form-group">
                        <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Enter a where statement to be used for querying.">
                        <strong> {!! Form::label('where_condition', 'Where Condition:') !!}</strong>
                        </span>{!! Form::text('where_condition', null, array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                        <span data-tooltip data-options="hover_delay: 50;" class="has-tip" title="Optionally change the name of the final attachment saved.">
                        <strong> {!! Form::label('final_name', 'Final Report Name:') !!}</strong>
                        </span>{!! Form::text('final_name', null, array('class' => 'form-control')) !!}
                        </div>
                   </div>
                   <div id="template_no" style="display: none;">
                   <p>This is not a template Report. Skip this Step and click Finish.</p>
                   </div>
                    </section>
                    {!! Form::close() !!}
                    </div>        
            </div>
        
</div>
</div>
</div>
<div id="create-recipient" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Recipient</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">

                            <div class="col-sm-12">
                                <strong><label for="temp_name">Name:</label></strong>
                               <input class="form-control" name="temp_name" type="text" id="temp_name" required>
                            </div>
                            <div class="col-sm-12">
                                <strong> <label for="email_address">Email Address:</label></strong>
                                <input class="form-control" name="email_address" type="text" id="email_address" required>
                            </div>
                            <div class="col-sm-12">
                                <strong> <label for="internal_external">Internal/External:</label></strong>
                                <select class="form-control" name="internal_external" id="internal_external" required>
                                  <option value="Internal">Internal</option>
                                <option value="External">External</option>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <strong> <label for="company">Company:</label></strong>
                                <input class="form-control" name="company" type="text" id="company" required>
                            </div>
                            <div class="col-sm-12">
                                <strong> <label for="s3_id">S3 ID:</label></strong>
                                <input class="form-control" name="s3_id" type="text" id="s3_id">
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                            <div class="col-sm-12 text-center">
                            <input class="btn btn-primary btn-lg" type="submit" onclick="addRecipientRecord()" value="Create Record">
                            </div>

                 </div>
</div>
</div>
</div>

@endsection
@section('js')
<script>
$(document).ready(function () {
    $("#dataTableBuilder").on('click', '.copyreport', function() {
        $('#rid').val($(this).data('id'));
    });
});

function addRecipientRecord() {
    // get values
    var temp_name = $("#temp_name").val();
    var email_address = $("#email_address").val();
    var internal_external = $("#internal_external").val();
    var company = $("#company").val();
    var s3_id = $("#s3_id").val();
 
    // Add record
    $.ajax({
            type:'post',
            url:"{{ URL::to('reports/add_recipient_record') }}",
            data: {
                    temp_name: temp_name,
                    email_address: email_address,
                    internal_external: internal_external,
                    company: company,
                    s3_id: s3_id
                },

            success:function(data){
                $("#create-recipient").modal("hide");
                $("#name").val("");
                $("#email_address").val("");
                $("#internal_external").val("");
                $("#company").val("");
                $("#s3_id").val("");
                alertify.alert("Recipient record successfully saved!");
            },
              error: function(data){
                $("#name").val("");
                $("#email_address").val("");
                $("#internal_external").val("");
                $("#company").val("");
                $("#s3_id").val("");
                alertify.alert("Error Submitting Record!");
              }
       });
}


var form = $("#add_report");

form.children("#add_report").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    onStepChanging: function (event, currentIndex, newIndex)
    {
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
              onFinished: function (event, currentIndex)
            {
                var date = document.getElementById("expire_time").value;
                var y = document.forms["report"]["newtz"].value;
                $("select[name='newday[]']").each(function(){
                if( $(this).val() &&  $(this).val() != '') {
                    if (y == null || y == "") {
                        alertify.alert("Timezone must be selected");
                        return false;
                    }
                    else {
                        
                        if (date.match(/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/) || date == null || date == "") {
                        form.submit();
                        } 
                        else {
                            alertify.alert("Invalid Expire Time format: Expire Time should be in HH:MM:SS format.");
                            return false;
                        }
                    }
                }
                else {
                        if (date.match(/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/) || date == null || date == "") {
                        form.submit();
                        } 
                        else {
                            alertify.alert("Invalid Expire Time format: Expire Time should be in HH:MM:SS format.");
                            return false;
                        }
                }
                });

            }
})

.validate();


  // run multiselect callbacks
$('#days').multiSelect();

$( function() {
    $("#ID1_recipient").autocomplete({
          scroll: true,
          autoFocus: true,
          minLength: 3,
          appendTo: "#create-report",
          source: function( request, response ) {
            $.ajax({
              url: "{{ URL::to('reports/searchrecipient') }}",
              dataType: "json",
              data: {
                  searchText: request.term
              },
              success: function( data ) {
               response($.map(data, function(item) {
                    return {
                        label: item.name,
                        value: item.id
                    };
                }));
              }
            });
          },
          select: function( event, ui) {
            $(this).val(ui.item.label);
            $("#ID1_recipient_id").val(ui.item.value);
            return false;
          },
          change: function (e, ui) {
                if (!(ui.item)) e.target.value = "";
          }
    } );
} );

</script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('vendor/datatables/buttons.server-side.js') }}"></script>
{!! $html->scripts() !!}
<script src="{{ URL::to('js/reports.js') }}"></script>
@endsection