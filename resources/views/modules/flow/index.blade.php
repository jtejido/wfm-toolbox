@extends('flow::layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home <a class="pull-right btn btn-primary btn-xs" href="{{ URL::to('flow/folder/create') }}" role="button">New Folder</a></div>

                <div class="panel-body">
                    <ul class="list-group">
                      @foreach($folders as $folder)
                        <a href="{{ URL::to('flow/folder/'.$folder->folder_id.'') }}" class="list-group-item"><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span> {{ $folder->name }} <span class="badge">{{ $folder->processes->count() }}</span></a>
                      @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection