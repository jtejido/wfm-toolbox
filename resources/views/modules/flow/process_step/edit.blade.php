@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> {{ $step->name }} <a class="pull-right btn btn-danger btn-xs" href="{{ URL::to('flow/process/'.$step->process->process_id.'/step/'.$step->step_id.'/delete') }}" role="button">Delete</a></div>

                <div class="panel-body">
                  <form method="POST" action="{{ URL::to('flow/process/'.$step->process->process_id.'/step/'.$step->step_id.'') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="name">Step</label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="Step Name" value="{{ $step->name }}" />
                    </div>
                    <div class="form-group">
                      <label for="step">Details <small>(optional)</small></label>
                      <textarea id="step" name="description" class="form-control step" rows="3">{!! $html_step !!}</textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Update</button>
                    <a href="{{ URL::to('flow/process/'.$step->process_id.'') }}" class="btn btn-default">Cancel</a>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
      $('#step').summernote();
  });
</script>
@endsection
