@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> {{ $process->name }}
                  <div class="btn-group pull-right" role="group">
                    <a class="btn btn-primary btn-xs" href="{{ URL::to('flow/process/'.$process->process_id.'/step/create') }}" role="button">New Step</a>
                    <a class="btn btn-warning btn-xs" href="{{ URL::to('flow/process/'.$process->process_id.'/edit') }}" role="button">Edit Process</a>
                    <a class="btn btn-default btn-xs" href="{{ URL::to('flow/process/'.$process->process_id.'/export') }}" role="button">Export</a>
                    <a class="btn btn-default btn-xs" href="{{ URL::to('flow/process/'.$process->process_id.'/sort') }}" role="button">Sort</a>
                    <a class="btn btn-default btn-xs" href="{{ URL::to('flow/folder/'.$process->folder_id.'') }}" role="button">Go Back</a>
                  </div>
                </div>
                <div class="panel-body">
                  <p class="lead">{{ $process->description }}</p>
                  <p><strong>Edited by:</strong> {{ $process->user->full_name }}</p>
                  <p><strong>Last Updated:</strong> {{ $process->updated_at }}</p>
                    <div class="list-group">
                      @foreach($process->steps as $step)
                        <a href="{{ URL::to('flow/process/'.$process->process_id.'/step/'.$step->step_id.'') }}" class="list-group-item">
                          <h4 class="list-group-item-heading">{{ $loop->iteration }}. {{ $step->name }}</h4>
                        </a>
                      @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
