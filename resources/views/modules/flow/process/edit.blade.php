@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> {{ $process->name }} <a class="pull-right btn btn-danger btn-xs" href="{{ URL::to('flow/process/'.$process->process_id.'/delete') }}" role="button">Delete</a></div>

                <div class="panel-body">
                  <form method="POST" action="{{ URL::to('flow/process/'.$process->process_id.'') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="{{ $process->name }}" value="{{ $process->name }}" />
                    </div>
                    <div class="form-group">
                      <label for="step">Description</label>
                      <textarea id="step" name="description" class="form-control step" rows="3">{{ $process->description }}</textarea>
                    </div>
                    <div class="form-group">
                      <label for="step">Description</label>
                      <select id="folder_id" name="folder_id" class="form-control">
                        @foreach($folders as $folder)
                          <option value="{{ $folder->folder_id }}" @if($process->folder_id == $folder->folder_id) selected @endif>{{ $folder->name }}</option>
                        @endforeach
                      </select>
                    </div>
                    <button type="submit" class="btn btn-default">Update</button>
                    <a href="{{ URL::to('flow/process/'.$process->process_id.'') }}" class="btn btn-default">Cancel</a>

                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
