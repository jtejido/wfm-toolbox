
<h1>{{ $process->name }}</h1>
<p>{{ $process->description }}</p>
<p><strong>Created by:</strong> {{ $process->user->name }}</p>
<p><strong>Last Edited:</strong> {{ $process->updated_at }}</p>
<hr/>

@foreach($process->steps as $step)
  <h4>{{ $loop->iteration }}. {{ $step->name }}</h4>
  @if(!empty($step->step))
    <p >{!! $step->step !!}</p>
  @endif
@endforeach

<hr/>
<p><strong>Internal Use Only</strong></p>
<p>&copy; {{ date('Y') }}</p>
