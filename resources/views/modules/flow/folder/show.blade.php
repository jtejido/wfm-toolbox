@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> {{ $folder->name }}
                  <div class="btn-group pull-right" role="group">
                    <a class="btn btn-primary btn-xs" href="{{ URL::to('flow/process/create/'.$folder->folder_id.'') }}" role="button">Create Process</a>
                    <a class="btn btn-warning btn-xs" href="{{ URL::to('flow/folder/'.$folder->folder_id.'/edit') }}" role="button">Edit Folder</a>
                    <a class="btn btn-default btn-xs" href="{{  URL::to('flow') }}" role="button">Go Back</a>
                  </div>
                </div>

                  <div class="panel-body">

                  <p class="lead">{{ $folder->description or $folder->name }}</p>
                  <p><strong>Edited by:</strong> {{ $folder->user->full_name }}</p>
                  <p><strong>Last Updated:</strong> {{ $folder->updated_at }}</p>

                  <div class="list-group">
                    @foreach($folder->processes as $process)
                      <a href="{{ URL::to('flow/process/'.$process->process_id.'') }}" class="list-group-item"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> {{ $process->name }} <span class="badge">{{ $process->steps->count() }}</span></a>
                    @endforeach
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
