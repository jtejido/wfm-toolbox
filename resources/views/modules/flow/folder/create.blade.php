@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> New Folder</div>

                <div class="panel-body">
                  <form method="POST" action="{{ URL::to('flow/folder/create') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="Folder Name" />
                    </div>
                    <div class="form-group">
                      <label for="step">Description</label>
                      <textarea id="step" name="description" class="form-control step" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Create</button>
                    <a href="{{ URL::to('flow') }}" class="btn btn-default">Cancel</a>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
