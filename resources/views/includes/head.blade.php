    <!-- HERE COMES THE HEADERS! -->
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">
    <title>D&PM Toolbox</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>

     <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/skin-yellow-light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/simple-sidebar.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://mystats.services.conduent.com/tools/js/html5shiv.min.js"></script>
    <script src="https://mystats.services.conduent.com/tools/js/respond.min.js"></script>
    <![endif]-->


  