        <aside class="main-sidebar">
            <section class="sidebar">
              <ul class="sidebar-menu">
                <li class="header">D&PM Toolbox</li>
                        @if (session('is_admin') == 1)
                        @include('partials.modules.admin')
                        @endif

                        @if (!empty(session('toolbox_modules')))
                        @foreach(session('toolbox_modules') as $key => $value)
                        @include('partials.modules.' .$value)
                        @endforeach
                        @endif
                        <li {{(Request::is('help') ? 'class=active' : '')}}>
                          <a href="/tools/help">
                            <i class="fa fa-info-circle"></i> <span>Help</span>
                          </a>
                        </li>
              </ul>
            </section>
        </aside>



