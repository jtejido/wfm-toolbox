<!DOCTYPE html>
<html lang="en">
  <head>
@include('includes.head')
<style type="text/css">
body {
    background: url(https://d1hw6n3yxknhky.cloudfront.net/022518627_prevstill.jpeg) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
.form-signin
{
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin
{
    margin-bottom: 10px;
}

.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 30px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}

.account-wall
{
    margin-top: 100px;
    padding: 30px 30px 20px 30px;
    background-color: #f7f7f7;
    background: rgba(247, 247, 247, .6);
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    height: 600px;
}

.profile-img
{
    margin: 0 auto 5px;
    display: block;
}

</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-6 col-md-offset-3">
            <div class="account-wall">
                <img class="profile-img" src="{!! asset('/app/img/conduent-logo.svg') !!}" class="img-responsive" height="55" alt="Xerox Logo">
                <H3 align="center">Data & Performance Management Toolbox</H3>
                        @if (null !== Session::get('error'))
                          <div class="alert alert-warning" style="margin:0px;">
                          {!! Session::get('error') !!}
                          </div>

                        @endif
                        @if (null !== Session::get('message'))
                          <div class="alert alert-info" style="margin:0px;">
                          {!! Session::get('message') !!}
                          </div>
                        @endif
                <form class="form-signin" action="login" method="post">
                {!! csrf_field() !!}
                <H6 align="center" style="font-size: 16px;">Login Method</H6>
                <select id="inputAuthentication" name="authmethod" class="form-control">
                    <option value="internal2"> Login using your Xerox WIN ID and Password</option>
                    <option value="internal"> Login using your Conduent WIN ID and Password</option>
                    <option value="external"> Login using your XBSEurope desktop account</option>
                </select>
                 <br/>
                <H6 align="center" style="font-size: 16px;">Username</H6>
                <input type="text" class="form-control" name="username" placeholder="User Name" required autofocus>
                <br/>
                <H6 align="center" style="font-size: 16px;">Password</H6>
                <input type="password" class="form-control" name="password" placeholder="Password" autofocus>
                <input class="btn btn-lg btn-primary btn-block" align="center" type="submit" name="submit" value="Sign In">
                </form>
            </div>
        </div>
    </div>
</div>
 </body>
</html>
