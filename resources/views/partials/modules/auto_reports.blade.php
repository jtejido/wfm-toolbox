        <li {{((Request::is('reports')) || (Request::is('template_config')) || (Request::is('recipients')) || (Request::is('reports_to_run')) || (Request::is('current_day_tracker')) || (Request::is('manual_waiting')) || (Request::is('approval_queue')) ? 'class=active treeview' : 'class=treeview')}}>
          <a href="#">
            <i class="fa fa-list-ul"></i> <span>Auto Reports</span>
            <span class="pull-right-container">
            @if (Session::get('approval_count') > 0)
              <small class="label pull-right bg-red">!</small>
            @else
              <i class="fa fa-angle-left pull-right"></i>
            @endif
            </span>
          </a>
          <ul class="treeview-menu">
                        @if ((Session::get('auto_reports_admin') == '1'))                        
                        <li {{(Request::is('reports') ? 'class=active' : '')}}><a href="{!! asset('/reports') !!}">&nbsp;&nbsp;Reports</a></li>
                        <li {{(Request::is('template_config') ? 'class=active' : '')}}><a href="{!! asset('/template_config') !!}">&nbsp;&nbsp;Template Config</a></li>
                        <li {{(Request::is('recipients') ? 'class=active' : '')}}><a href="{!! asset('/recipients') !!}">&nbsp;&nbsp;Recipients</a></li>
                        <li {{(Request::is('reports_to_run') ? 'class=active' : '')}}><a href="{!! asset('/reports_to_run') !!}">&nbsp;&nbsp;Reports to Run</a></li>
                        <li {{(Request::is('current_day_tracker') ? 'class=active' : '')}}><a href="{!! asset('/current_day_tracker') !!}">&nbsp;&nbsp;Current Day Tracker</a></li> 
                        <li {{(Request::is('manual_waiting') ? 'class=active' : '')}}><a href="{!! asset('/manual_waiting') !!}">&nbsp;&nbsp;Manual Waiting</a></li> 
                        @endif
                        @if ((Session::get('auto_reports_approver') == '1') || (Session::get('is_approver') == '1'))
                        <li {{(Request::is('approval_queue') ? 'class=active' : '')}}><a href="{!! asset('/approval_queue') !!}">&nbsp;&nbsp;Approval Queue

                        @if (Session::get('approval_count') > 0)
                          <span class="pull-right-container">
                            <small class="label pull-right bg-red">{!! session('approval_count') !!}</small>
                          </span>
                          @endif
                          </a></li>
                        @endif
          </ul>
        </li>