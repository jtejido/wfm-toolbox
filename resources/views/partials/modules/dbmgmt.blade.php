        <li {{((Request::is('notify')) || (Request::is('notify_recipient')) || (Request::is('objects')) || (Request::is('result')) || (Request::is('schedule')) || (Request::is('activity_log')) ? 'class=active treeview' : 'class=treeview')}}>
          <a href="#">
            <i class="fa fa-wrench"></i> <span>DB Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                          <li {{(Request::is('notify') ? 'class=active' : '')}}><a href="{!! asset('/notify') !!}">&nbsp;&nbsp;Notify</a></li>
                          <li {{(Request::is('notify_recipient') ? 'class=active' : '')}}><a href="{!! asset('/notify_recipient') !!}">&nbsp;&nbsp;Notify Recipient</a></li>
                          <li {{(Request::is('objects') ? 'class=active' : '')}}><a href="{!! asset('/objects') !!}">&nbsp;&nbsp;Validation</a></li>
                          <li {{(Request::is('result') ? 'class=active' : '')}}><a href="{!! asset('/result') !!}">&nbsp;&nbsp;Result</a></li>
                          <li {{(Request::is('schedule') ? 'class=active' : '')}}><a href="{!! asset('/schedule') !!}">&nbsp;&nbsp;Schedule</a></li>
                          <li {{(Request::is('activity_log') ? 'class=active' : '')}}><a href="{!! asset('/activity_log') !!}">&nbsp;&nbsp;Activity Log</a></li>
          </ul>
        </li>