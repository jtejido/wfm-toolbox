        <li {{((Request::is('elements')) || (Request::is('sources')) || (Request::is('integration')) || (Request::is('costcentre')) || (Request::is('contract')) || (Request::is('customer')) || (Request::is('ibg')) || (Request::is('language')) || (Request::is('region')) || (Request::is('subcontract')) || (Request::is('subcontract')) || (Request::is('timezones')) || (Request::is('sites')) || (Request::is('client')) || (Request::is('country')) ? 'class=active treeview' : 'class=treeview')}}>
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Elements</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                          <li {{(Request::is('elements') ? 'class=active' : '')}}><a href="{!! asset('/elements') !!}">&nbsp;&nbsp;Elements</a></li>
                          <li {{(Request::is('elements_tree_view') ? 'class=active' : '')}}><a href="{!! asset('/elements_tree_view') !!}">&nbsp;&nbsp;Elements Tree Chart</a></li>
                        <li {{(Request::is('sources') ? 'class=active' : '')}}><a href="{!! asset('/sources') !!}">&nbsp;&nbsp;Sources</a></li>
                        <li {{(Request::is('integration') ? 'class=active' : '')}}><a href="{!! asset('/integration') !!}">&nbsp;&nbsp;Integration</a></li>
                        <li {{(Request::is('costcentre') ? 'class=active' : '')}}><a href="{!! asset('/costcentre') !!}">&nbsp;&nbsp;CostCentre</a></li>
                        <li {{(Request::is('contract') ? 'class=active' : '')}}><a href="{!! asset('/contract') !!}">&nbsp;&nbsp;Contract</a></li>
                        <li {{(Request::is('customer') ? 'class=active' : '')}}><a href="{!! asset('/customer') !!}">&nbsp;&nbsp;Customer</a></li>
                        <li {{(Request::is('ibg') ? 'class=active' : '')}}><a href="{!! asset('/ibg') !!}">&nbsp;&nbsp;IBG</a></li>
                        <li {{(Request::is('language') ? 'class=active' : '')}}><a href="{!! asset('/language') !!}">&nbsp;&nbsp;Language</a></li>
                        <li {{(Request::is('region') ? 'class=active' : '')}}><a href="{!! asset('/region') !!}">&nbsp;&nbsp;Region</a></li>
                        <li {{(Request::is('subcontract') ? 'class=active' : '')}}><a href="{!! asset('/subcontract') !!}">&nbsp;&nbsp;SubContract</a></li>
                        <li {{(Request::is('timezones') ? 'class=active' : '')}}><a href="{!! asset('/timezones') !!}">&nbsp;&nbsp;Timezones</a></li>
                        <li {{(Request::is('sites') ? 'class=active' : '')}}><a href="{!! asset('/sites') !!}">&nbsp;&nbsp;Sites</a></li>
                        <li {{(Request::is('client') ? 'class=active' : '')}}><a href="{!! asset('/client') !!}">&nbsp;&nbsp;Client</a></li>
                        <li {{(Request::is('country') ? 'class=active' : '')}}><a href="{!! asset('/country') !!}">&nbsp;&nbsp;Country</a></li>
          </ul>
        </li>