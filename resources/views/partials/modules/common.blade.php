                    <li {{(Request::is('subscribed_reports') ? 'class=active' : '')}}>
                      <a href="{!! asset('/subscribed_reports') !!}">
                        <i class="fa fa-area-chart"></i> <span>Subscribed Reports</span></a>
                    </li>
                    <li {{(Request::is('request_report') ? 'class=active' : '')}}>
                      <a href="{!! asset('/request_report') !!}">
                        <i class="fa fa-search-plus"></i> <span>Request Report</span></a>
                    </li>
                    <li {{(Request::is('pending_approval') ? 'class=active' : '')}}>
                      <a href="{!! asset('/pending_approval') !!}">
                        <i class="fa fa-check-square-o"></i> <span>Pending Approval</span>

			<span class="pull-right-container">
                          <small class="label pull-right bg-red">{!! session('pending_count') !!}</small>
                        </span>
			</a>
                    </li>
                    <li {{(Request::is('download_reports') ? 'class=active' : '')}}>
                      <a href="{!! asset('/download_reports') !!}">
                        <i class="fa fa-download"></i> <span>Download Report</span></a>
                    </li>
                    <li {{(Request::is('clean_desk_policy') ? 'class=active' : '')}}>
                      <a href="{!! asset('/clean_desk_policy') !!}">
                        <i class="fa fa-file-text"></i> <span>Clean Desk Policy</span></a>
                    </li>

