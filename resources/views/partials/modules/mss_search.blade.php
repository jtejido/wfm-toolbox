        <li {{((Request::is('mss_search')) || (Request::is('wfm_search')) ? 'class=active treeview' : 'class=treeview')}}>
          <a href="#">
            <i class="fa fa-search"></i> <span>MSS Search</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
                        <li {{(Request::is('mss_search') ? 'class=active' : '')}}><a href="{!! asset('/mss_search') !!}">&nbsp;&nbsp;MSS Search</a></li>
                        <li {{(Request::is('wfm_search') ? 'class=active' : '')}}><a href="{!! asset('/wfm_search') !!}">&nbsp;&nbsp;WFM Search</a></li>
          </ul>
        </li>