    <header class="main-header">
    <a href="{!! asset('/') !!}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="{!! asset('/app/img/vectorpaint_symbol.png') !!}" height="28"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{!! asset('/app/img/conduent-logo.png') !!}" height="60"></span>
    </a>
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">{{session('total')}}</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have {{session('total')}} notifications</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="{!! asset('/pending_approval') !!}">
                          <i class="fa fa-warning text-yellow"></i> You have {{session('pending_rejected')}} requests rejected today.
                        </a>
                      </li>
                      <li>
                        <a href="{!! asset('/pending_approval') !!}">
                          <i class="fa fa-check text-green"></i> You have {{session('pending_approved')}} requests approved today.
                        </a>
                      </li>
			@if (Session::get('auto_reports_approver') == '1')
                      <li>
                        <a href="{!! asset('/approval_queue') !!}">
                          <i class="fa fa-clock-o text-red"></i> You have {{session('approval_count')}} requests pending an approval.
                        </a>
                      </li>
                      @endif
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <a style="font-size: 15px;" href="{!! asset('/logout') !!}"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
              </li>
              
            </ul>
          </div>
        </nav>
      </header>
