@extends('app')
@section('content')
<div id="block_error">
        <div>
         <h2>Error 403. &nbspOops it's Access Denied/Forbidden.</h2>
        <p>
        It appears that the access to this page or the action made against the previous page is <b>Unauthorized</b>.<br />
        @if (session('imposter') == 1)
        It shows that you're impostering an employee. Since we log all changes made to any pages, we wish to avoid bypassing the security of this site. Please logout and log-in as normal if you wish to make any changes.
        @else
        You may need to check your permissions by either raising a ticket or asking your direct superior. Once the proper permission/s have been given, you'll have to log-out and re log-in to the App.
        @endif
        </p>
        <p>
        For any concerns or questions, please raise a ticket to our <a href="https://mystats.services.xerox.com/support">Support</a> portal.
        </p>
        </div>
</div>
@endsection
