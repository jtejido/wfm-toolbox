@extends('app')
@section('content')
<div id="block_error">
        <div>
         <h2>Error 500. &nbspOops it's Internal Server Error.</h2>
        <p>
        The site encountered an Internal Error or misconfiguration and was unable to complete your request.<br />
        Please raise a ticket and inform us of the url and what you were trying to achieve that causes this error.
        </p>
        <p>
        For any concerns or questions, raise a ticket to our <a href="https://mystats.services.xerox.com/support">Support</a> portal.
        </p>
        </div>
</div>
@endsection
