<!DOCTYPE html>
<html lang="en">
  <head>
@include('includes.head')
<style type="text/css">
body {
    background: url({{ asset('/app/img/6782117-cool-looking-up-wallpaper.jpg') }}) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

.account-wall
{
    margin-top: 100px;
    padding: 30px 30px 20px 30px;
    background-color: #f7f7f7;
    background: rgba(247, 247, 247, .7);
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    height: 400px;
}

.profile-img
{
    margin: 0 auto 5px;
    display: block;
}

</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-6 col-md-offset-3">
            <div class="account-wall">
                <img class="profile-img" src="{!! asset('/app/img/Xerox_2008_Logo.png') !!}" class="img-responsive" height="55" alt="Xerox Logo">
                <H3 align="center">Data & Performance Management Toolbox</H3>
                <H1 align="center"><span style="font-size:80px;" class="fa fa-wrench"></span></H1>
                <br />
                <H4 align="center">We are working on maintaining this part of the site at the moment. Please come back to it after an hour.</H4>
            </div>
        </div>
    </div>
</div>
 </body>
</html>
