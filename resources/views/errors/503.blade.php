@extends('app')
@section('content')
<div id="block_error">
        <div>
         <h2>Error 503. &nbspOops it's Service Unavailable.</h2>
        <p>
        We are working on maintaining this part of the site at the moment. Please come back to it later.<br />
        We'll be up and running soon, please keep an eye on this page.
        </p>
        <p>
        For any concerns or questions, please raise a ticket to our <a href="https://mystats.services.xerox.com/support">Support</a> portal.
        </p>
        </div>
</div>
@endsection
