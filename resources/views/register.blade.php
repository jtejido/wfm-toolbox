@extends('app')
@section('content')
<div class="container-fluid">
<div class="alert alert-info">
  An Email Address for your account isn't registered in this app, Please enter your Email Address below. Take note that this is only done one time. To change the email address you registered, you'll have to raise a Support ticket.
</div>
@if (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="col-sm-12">
 <div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-clock-o"></i></span>
      <h5>Register Email Address</h5>
    </div>
    <div class="widget-content" >
		<form method="post" action="/tools/register/submit">
						<br />
						<div class="form-group">
		                    <label for="email" class="control-label">Email Address: </label>
		                    <input type="text" placeholder="Email" class="form-control" name="email_address">
		                </div>
		                <div class="form-group">
		                    <label for="company" class="control-label">Company: </label>
		                    <input type="text" id="company" placeholder="Company" class="form-control" name="company">
		                </div>
		                <div class="form-group">
		                    <label for="internal_external" class="control-label">Internal or External? </label>
		                    <select id="internal_external" class="form-control" name="internal_external">
		                    <option></option>
                            <option value="Internal">Internal</option>
                            <option value="External">External</option>
                        </select>
		                </div>
		                <br />
		                <div class="form-group">
		                    <button type="submit" class="btn btn-primary">Submit</button>
		                </div>
		 {!! csrf_field() !!}
		</form>

	</div>
</div>
</div>
</div>
@endsection