@extends('elements::layouts.app')
@section('content')
  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> CostCentres</h3> 
  </div>

<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
<div class="row-fluid" >
<table class="table-bordered table-condensed" id="sortable">
<thead>
<tr>
<th>Costcentre_id</th>
<th>Costcentre*</th>
<th>Description*</th>
<th>Site_id*</th>
<th>Contract_id*</th>
<th>Src_type_id*</th>
<th>Client_id*</th>
</tr>
</thead>
<tfoot>
<tr>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
<tbody>
@foreach($costcentres as $key => $value)
 <tr>
            <td>{{ $value->costcentre_id }}</td>
            <td><a href="#" class="testEdit" data-type="text" data-column="costcentre" data-url="{{route('costcentre/update', ['costcentre_id'=>$value->costcentre_id])}}" data-pk="{{$value->costcentre_id}}" data-title="change" data-name="costcentre">{{ $value->costcentre }}</a></td>
            <td><a href="#" class="testEdit2" data-type="text" data-column="description" data-url="{{route('costcentre/update', ['costcentre_id'=>$value->costcentre_id])}}" data-pk="{{$value->costcentre_id}}" data-title="change" data-name="description">{{ $value->description }}</a></td>
            <td><a href="#" class="testEdit3" data-column="site_id" data-url="{{route('costcentre/update', ['costcentre_id'=>$value->costcentre_id])}}" data-pk="{{$value->costcentre_id}}" data-title="change" data-name="site_id">{{ $value->site }}</a></td>
            <td><a href="#" class="testEdit4" data-column="contract_id" data-url="{{route('costcentre/update', ['costcentre_id'=>$value->costcentre_id])}}" data-pk="{{$value->costcentre_id}}" data-title="change" data-name="contract_id">{{ $value->contract }}</a></td>
            <td><a href="#" class="testEdit5" data-type="text" data-column="src_type_id" data-url="{{route('costcentre/update', ['costcentre_id'=>$value->costcentre_id])}}" data-pk="{{$value->costcentre_id}}" data-title="change" data-name="src_type_id">{{ $value->src_type_id }}</a></td>
            <td><a href="#" class="testEdit6" data-column="client_id" data-url="{{route('costcentre/update', ['costcentre_id'=>$value->costcentre_id])}}" data-pk="{{$value->costcentre_id}}" data-title="change" data-name="client_id">{{ $value->client }}</a></td>
</tr>
 @endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">

$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit2').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit3').editable({
        type: 'select',
        source: [
        <?php foreach($sites as $key => $value){ ?>
            {value: '<?php echo $value->site_id; ?>', text: '<?php echo $value->site; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

    $('.testEdit4').editable({
        type: 'select',
        source: [
        <?php foreach($contracts as $key => $value){ ?>
            {value: '<?php echo $value->contract_id; ?>', text: '<?php echo $value->contract; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

    $('.testEdit5').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit6').editable({
        type: 'select',
        source: [
        <?php foreach($clients as $key => $value){ ?>
            {value: '<?php echo $value->client_id; ?>', text: '<?php echo $value->client; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

});

  $(document).ready(function() {
    $('#sortable').DataTable({
      "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
      "order": [[ 1, "desc" ]],
        initComplete: function () {
            this.api().columns('.select-filter').every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
} );
} );
</script>
@endsection