@extends('elements::layouts.app')
@section('content')
  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> Language</h3> 
  </div>
<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
<div class="row-fluid" >
<table class="table-bordered table-condensed" id="sortable">
<thead>
<tr>
 <th>Language_id</th>
 <th>Language*</th>
 <th>Iso_2char*</th>   
</tr>
</thead>
<tfoot>
<tr>
<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
<tbody>
@foreach($languages as $key => $value)
 <tr>
            <td>{{ $value->language_id }}</td>
            <td><a href="#" class="testEdit" data-type="text" data-column="language" data-url="{{route('language/update', ['language_id'=>$value->language_id])}}" data-pk="{{$value->language_id}}" data-title="change" data-name="language">{{ $value->language }}</a></td>
            <td><a href="#" class="testEdit2" data-type="text" data-column="iso_2char" data-url="{{route('language/update', ['language_id'=>$value->language_id])}}" data-pk="{{$value->language_id}}" data-title="change" data-name="iso_2char">{{ $value->iso_2char }}</a></td>
</tr>
 @endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });
    $('.testEdit2').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });
});

  $(document).ready(function() {
    $('#sortable').DataTable({
      "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
        initComplete: function () {
            this.api().columns('.select-filter').every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
} );
} );

</script>
@endsection