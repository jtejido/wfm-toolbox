@extends('elements::layouts.app')
@section('content')
<script type='text/javascript'>
window.onload = loadCategories;
      <?php
        echo "var categories = $jsonCats; \n";
        echo "var subcats = $jsonSubCats; \n";
        echo "var subsubcats = $jsonSubSubCats; \n";
      ?>
      function loadCategories(){
        var select = document.getElementById("categoriesSelect");
        select.onchange = updateSubCats;
        for(var i = 0; i < categories.length; i++){
          select.options[i] = new Option(categories[i].customer,categories[i].customer_id);          
        }
      }
      function updateSubCats(){
        var customer_id = this.value;
        var subcatSelect = document.getElementById("subcatsSelect")
    subcatSelect.onchange = updateSubSubCats;;
        for(var i = 0; i < subcats[customer_id].length; i++){
          subcatSelect.options[i] = new Option(subcats[customer_id][i].contract,subcats[customer_id][i].contract_id);
        }
      }
    function updateSubSubCats(){
        var subsubcatSelect = this;
        var contract_id = this.value;
        var subsubcatSelect = document.getElementById("subsubcatsSelect");
        subsubcatSelect.options.length = 0; //delete all options if any present
        for(var i = 0; i < subsubcats[contract_id].length; i++){
          subsubcatSelect.options[i] = new Option(subsubcats[contract_id][i].subcontract,subsubcats[contract_id][i].subcontract_id);
        }
      }
</script>

  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> Elements</h3> 
    <p>Please select an Integration from the dropdown list.</p>
  </div>

<div class="dropdown">
  <button class="btn btn-primary dropdown-toggle dd" type="button" data-toggle="dropdown">Select Integration
  <span class="caret"></span></button>
  <ul class="dropdown-menu scrollable-menu collapse" id="dd">
    @foreach($integration as $key => $value)
    <li><a href="{{ URL::to('elements/'.$value->src_type_id.'') }}">{!!$value->integration!!}</a></li>
    @endforeach
  </ul>
</div>
@if(Session::has('message'))
<br />
                <div class="alert alert-success">
                  {!!Session::get('message')!!}
                </div>
@endif
<br /><br />
<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
            <form method="post" action="ElementController@bulk_update" class="form-inline">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="lead_status">For selected rows, change </label>
                {!! Form::select('bulk_name', $test_columns, [], ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="lead_status"> to</label>

                {!! Form::text('bulk_value', null, ['class' => 'form-control'])!!}

                <select class="form-control" name="updatelanguage" style="display: none">
                  <option value=""></option>
                  @foreach($languages as $key => $value)
                  <option value="{!! $value->language_id !!}">{!! $value->language !!}</option>
                  @endforeach
                </select>

                <select class="form-control" name="updatesite" style="display: none">
                  <option value=""></option>
                  @foreach($sites as $key => $value)
                  <option value="{!! $value->site_id !!}">{!! $value->site !!}</option>
                  @endforeach
                </select>

                <select class="form-control" name="updatesubcontract_id_cat" id="categoriesSelect" style="display: none">
                </select>
                <select class="form-control" name="updatesubcontract_id_subcat" id="subcatsSelect" style="display: none">
                </select>
                <select class="form-control" name="updatesubcontract_id" id="subsubcatsSelect" style="display: none">
                </select>

                <select class="form-control" name="updatecostcentre" style="display: none">
                  <option value=""></option>
                  @foreach($costcentres as $key => $value)
                  <option value="{!! $value->costcentre_id !!}">{!! $value->costcentre !!}</option>
                  @endforeach
                </select>

                <select class="form-control" name="updatetimezone" style="display: none">
                  <option value=""></option>
                  @foreach($timezones as $key => $value)
                  <option value="{!! $value->timezone_name !!}">{!! $value->timezone_name !!}</option>
                  @endforeach
                </select>
            </div>
             <input type="hidden" name="src_type_id" value="{!! $src_type_id !!}">
            <button class="btn btn-primary">Save</button>
<hr>
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-striped table-condensed" id="sortable">
<thead>
<tr>
<th width="10px"><input type="checkbox" id="checkAll" /></th>
<th>Src_id</th>
<th>Element_name*</th>
<th>Src_type_id</th>
<th>Subcontract_id</th>
<th class="select-filter">Language_id*</th>
<th class="select-filter">Site_id*</th>
<th class="select-filter">Costcentre_id*</th>
<th>Element_source_timezone*</th>
<th>Report_timezone*</th>
</tr>
</thead>
<tfoot>
<tr>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
<tbody>
@foreach($elements as $key => $value)
 <tr>
        <td width="10px"> <input type="checkbox" name="ids_to_edit[]" value="{{$value->src_id}}" /> </td>
            <td>{{ $value->src_id }}</td>
            <td><a href="#" class="testEdit" data-type="text" data-column="element_name" data-url="{{route('elements/update', ['src_type_id'=>$value->src_type_id, 'src_id'=>$value->src_id])}}" data-pk="{{$value->src_id}}" data-title="change" data-name="element_name">{{$value->element_name}}</a></td>
            <td>{{ $value->src_type_id }}</td>
            <td>{{ $value->subcontract }}</td>
            <td><a href="#" class="testEdit2" data-column="language_id" data-url="{{route('elements/update', ['src_type_id'=>$value->src_type_id, 'src_id'=>$value->src_id])}}" data-pk="{{$value->src_id}}" data-title="change" data-name="language_id">{{ $value->language }}</a></td>
            <td><a href="#" class='testEdit3' data-column="site_id" data-url="{{route('elements/update', ['src_type_id'=>$value->src_type_id, 'src_id'=>$value->src_id])}}" data-pk="{{$value->src_id}}" data-title="change" data-name="site_id">{{ $value->site }}</a></</td>
            <td><a href="#" class='testEdit4' data-column="costcentre_id" data-url="{{route('elements/update', ['src_type_id'=>$value->src_type_id, 'src_id'=>$value->src_id])}}" data-pk="{{$value->src_id}}" data-title="change" data-name="costcentre_id">{{ $value->costcentre }}</a></td>
            <td><a href="#" class='testEdit5' data-column="element_source_timezone" data-url="{{route('elements/update', ['src_type_id'=>$value->src_type_id, 'src_id'=>$value->src_id])}}" data-pk="{{$value->src_id}}" data-title="change" data-name="element_source_timezone">{{ $value->element_source_timezone }}</a></td>
            <td><a href="#" class='testEdit6' data-column="report_timezone" data-url="{{route('elements/update', ['src_type_id'=>$value->src_type_id, 'src_id'=>$value->src_id])}}" data-pk="{{$value->src_id}}" data-title="change" data-name="report_timezone">{{ $value->report_timezone }}</a></td>
</tr>
 @endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $('select[name=bulk_name]').on('change', function(){
    if($(this).val() == "language_id"){
        $('select[name=updatelanguage]').show();
        $('select[name=updatesite]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updatesubcontract_id_cat]').hide().val('');
        $('select[name=updatesubcontract_id_subcat]').hide().val('');
        $('select[name=updatecostcentre]').hide().val('');
        $('select[name=updatetimezone]').hide().val('');
    }
    else if($(this).val() == "site_id"){
        $('select[name=updatesite]').show();
        $('select[name=updatelanguage]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updatesubcontract_id_cat]').hide().val('');
        $('select[name=updatesubcontract_id_subcat]').hide().val('');
        $('select[name=updatesubcontract_id]').hide().val('');
        $('select[name=updatecostcentre]').hide().val('');
        $('select[name=updatetimezone]').hide().val('');
    }
    else if($(this).val() == "subcontract_id"){
        $('select[name=updatesite]').hide().val('');
        $('select[name=updatelanguage]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updatesubcontract_id_cat]').show();
        $('select[name=updatesubcontract_id_subcat]').show();
        $('select[name=updatesubcontract_id]').show();
        $('select[name=updatecostcentre]').hide().val('');
        $('select[name=updatetimezone]').hide().val('');
    }
    else if($(this).val() == "costcentre_id"){
        $('select[name=updatesite]').hide().val('');
        $('select[name=updatelanguage]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updatesubcontract_id_cat]').hide().val('');
        $('select[name=updatesubcontract_id_subcat]').hide().val('');
        $('select[name=updatesubcontract_id]').hide().val('');
        $('select[name=updatecostcentre]').show();
        $('select[name=updatetimezone]').hide().val('');
    }
    else if($(this).val() == "element_source_timezone"){
        $('select[name=updatesite]').hide().val('');
        $('select[name=updatelanguage]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updatesubcontract_id_cat]').hide().val('');
        $('select[name=updatesubcontract_id_subcat]').hide().val('');
        $('select[name=updatesubcontract_id]').hide().val('');
        $('select[name=updatecostcentre]').hide().val('');
        $('select[name=updatetimezone]').show();
    }
    else if($(this).val() == "report_timezone"){
        $('select[name=updatesite]').hide().val('');
        $('select[name=updatelanguage]').hide().val('');
        $('input[name=bulk_value]').hide().val('');
        $('select[name=updatesubcontract_id_cat]').hide().val('');
        $('select[name=updatesubcontract_id_subcat]').hide().val('');
        $('select[name=updatesubcontract_id]').hide().val('');
        $('select[name=updatecostcentre]').hide().val('');
        $('select[name=updatetimezone]').show();
    }
    else {
        $('select[name=updatelanguage]').hide().val('');
        $('select[name=updatesite]').hide().val('');
        $('input[name=bulk_value]').show().val('');
        $('select[name=updatesubcontract_id_cat]').hide().val('');
        $('select[name=updatesubcontract_id_subcat]').hide().val('');
        $('select[name=updatesubcontract_id]').hide().val('');
        $('select[name=updatecostcentre]').hide().val('');
        $('select[name=updatetimezone]').hide().val('');
    }
});

$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit2').editable({
        type: 'select',
        source: [
        <?php foreach($languages as $key => $value){ ?>
            {value: <?php echo $value->language_id; ?>, text: '<?php echo $value->language; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

        $('.testEdit3').editable({
        type: 'select',
        source: [
        <?php foreach($sites as $key => $value){ ?>
            {value: <?php echo $value->site_id; ?>, text: '<?php echo $value->site; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

     $('.testEdit4').editable({
        type: 'select',
        source: [
        <?php foreach($costcentres as $key => $value){ ?>
            {value: <?php echo $value->costcentre_id; ?>, text: '<?php echo $value->costcentre; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

    $('.testEdit5').editable({
        type: 'select',
        source: [
        <?php foreach($timezones as $key => $value){ ?>
            {value: '<?php echo $value->timezone_name; ?>', text: '<?php echo $value->timezone_name; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });

    $('.testEdit6').editable({
        type: 'select',
        source: [
        <?php foreach($timezones as $key => $value){ ?>
            {value: '<?php echo $value->timezone_name; ?>', text: '<?php echo $value->timezone_name; ?>'},
        <?php } ?>
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });
});

$(document).ready(function() {
    $('#sortable').DataTable({
      "stateSave": true,
        "scrollX": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
      "columnDefs": [
        { "orderable": false, "targets": 0 },
        { "targets": [5,6,7], type: "html" }
      ],
      "order": [[ 1, "desc" ]],
    initComplete: function () {
        this.api().columns('.select-filter').every( function () {
            var column = this;
            var select = $('<select name="filter"><option value=""></option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            column.data().unique().sort().each( function ( d, j ) {
               // For first column
               // ignore HTML tags 
                if((column.index() == 5) || (column.index() == 6) || (column.index() == 7)){ d = $(d).text(); }
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );
    }
} );
    var usedNames = {};
    $('select[name="filter"] > option').each(function(){
      if(usedNames[this.text]) {
          $(this).remove();
      } else {
          usedNames[this.text] = this.value;
      }
  });
} );



 $("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });


</script>
@endsection