@extends('elements::layouts.app')
@section('content')
<script type='text/javascript'>
window.onload = loadCategories;
      <?php
        echo "var categories = $jsonCats; \n";
        echo "var subcats = $jsonSubCats; \n";
        echo "var subsubcats = $jsonSubSubCats; \n";
        echo "var subsubsubcats = $jsonSubSubSubCats; \n";
        echo "var subsubsubsubcats = $jsonSubSubSubSubCats; \n";
        echo "var subsubsubsubsubcats = $jsonSubSubSubSubSubCats; \n";
      ?>
      function loadCategories(){
        var select = document.getElementById("categoriesSelect");
        select.onchange = updateSubCats;
        for(var i = 0; i < categories.length; i++){
          select.options[i] = new Option(categories[i].client,categories[i].client_id);          
        }
      }
      function updateSubCats(){
        var client_id = this.value;
        var subcatSelect = document.getElementById("subcatsSelect");
        document.getElementById("subsubcatsSelect").length = 0;
        document.getElementById("subsubsubcatsSelect").length = 0;
        document.getElementById("subsubsubsubcatsSelect").length = 0;
        document.getElementById("subsubsubsubsubcatsSelect").length = 0;
        subcatSelect.options.length = 0; //delete all options if any present
        subcatSelect.onchange = updateSubSubCats;
        for(var i = 0; i < subcats[client_id].length; i++){
          subcatSelect.options[i] = new Option(subcats[client_id][i].customer,subcats[client_id][i].customer_id);
        }
        subcatSelect.prepend(new Option('All', '-1', true, true));
        $("#subcatsSelect option:first").attr("selected", "selected");
      }
    function updateSubSubCats(){
        var subsubcatSelect = this;
        var customer_id = this.value;
        var subsubcatSelect = document.getElementById("subsubcatsSelect");
        document.getElementById("subsubsubcatsSelect").length = 0;
        document.getElementById("subsubsubsubcatsSelect").length = 0;
        document.getElementById("subsubsubsubsubcatsSelect").length = 0;
        subsubcatSelect.options.length = 0; //delete all options if any present
        subsubcatSelect.onchange = updateSubSubSubCats;
        for(var i = 0; i < subsubcats[customer_id].length; i++){
          subsubcatSelect.options[i] = new Option(subsubcats[customer_id][i].contract,subsubcats[customer_id][i].contract_id);
        }
        subsubcatSelect.prepend(new Option('All', '-1', true, true));
        $("#subsubcatsSelect option:first").attr("selected", "selected");
      }

      function updateSubSubSubCats(){
        var subsubsubcatSelect = this;
        var contract_id = this.value;
        var subsubsubcatSelect = document.getElementById("subsubsubcatsSelect");
        document.getElementById("subsubsubsubcatsSelect").length = 0;
        document.getElementById("subsubsubsubsubcatsSelect").length = 0;
        subsubsubcatSelect.options.length = 0; //delete all options if any present
        subsubsubcatSelect.onchange = updateSubSubSubSubCats;
        for(var i = 0; i < subsubsubcats[contract_id].length; i++){
          subsubsubcatSelect.options[i] = new Option(subsubsubcats[contract_id][i].subcontract,subsubsubcats[contract_id][i].subcontract_id);
        }
        subsubsubcatSelect.prepend(new Option('All', '-1', true, true));
        $("#subsubsubcatsSelect option:first").attr("selected", "selected");
      }

      function updateSubSubSubSubCats(){
        var subsubsubsubcatSelect = this;
        var subcontract_id = this.value;
        var subsubsubsubcatSelect = document.getElementById("subsubsubsubcatsSelect");
        document.getElementById("subsubsubsubsubcatsSelect").length = 0;
        subsubsubsubcatSelect.options.length = 0; //delete all options if any present
        subsubsubsubcatSelect.onchange = updateSubSubSubSubSubCats;
        for(var i = 0; i < subsubsubsubcats[subcontract_id].length; i++){
          subsubsubsubcatSelect.options[i] = new Option(subsubsubsubcats[subcontract_id][i].site,subsubsubsubcats[subcontract_id][i].site_id);
        }
        subsubsubsubcatSelect.prepend(new Option('All', '-1', true, true));
        $("#subsubsubsubcatsSelect option:first").attr("selected", "selected");
      }

      function updateSubSubSubSubSubCats(){
        var subsubsubsubsubcatSelect = this;
        var subcontract_id = document.getElementById("subsubsubcatsSelect").value;
        var site_id = this.value;
        var subsubsubsubsubcatSelect = document.getElementById("subsubsubsubsubcatsSelect");
        subsubsubsubsubcatSelect.options.length = 0; //delete all options if any present
        for(var i = 0; i < subsubsubsubsubcats[site_id][subcontract_id].length; i++){
          subsubsubsubsubcatSelect.options[i] = new Option(subsubsubsubsubcats[site_id][subcontract_id][i].integration,subsubsubsubsubcats[site_id][subcontract_id][i].src_type_id);
        }
        subsubsubsubsubcatSelect.prepend(new Option('All', '-1', true, true));
        $("#subsubsubsubsubcatsSelect option:first").attr("selected", "selected");
      }


</script>


  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> Elements</h3> 
    <p>Please select a filter from the dropdown list.</p>
  </div>
  <div class="row">

<div class="col-md-12">
<ul class="list-inline">
<li>
<div class="form-group">
<label>Filter by:</label>
<select class="form-control" id="element_view_filter">
  <option value="">Select...</option>
  <option value="1">Client/Contract</option>
  <option value="2">Site</option>
  <option value="3">Integration</option>
</select>
</div>
</li>
<li id="filter_site" style="display: none;">
<select class="form-control" id="element_view_filter_site">
  @foreach($site as $key => $value)
  <option value="{!!$value->site_id!!}">{!!$value->element_site!!}</option>
  @endforeach
</select>
</li>
<li id="filter_integration" style="display: none;">
<select class="form-control" id="element_view_filter_integration">
  @foreach($integration as $key => $value)
  <option value="{!!$value->src_type_id!!}">{!!$value->integration!!}</option>
  @endforeach
</select>
</li>
<li id="filter_contract" style="display: none;">
  <ul class="list-inline">
    <li>
        <div class="form-group">
              <label>Client</label>
              <select class="form-control" name="client_id" id="categoriesSelect">
                    </select>
        </div>
      
    </li>
    <li>
        <div class="form-group">
              <label>Customer</label>
              <select class="form-control" name="customer_id" id="subcatsSelect">
                    </select>
        </div>
      
    </li>
    <li>
        <div class="form-group">
              <label>Contract</label>
              <select class="form-control" name="contract_id" id="subsubcatsSelect">
                    </select>
        </div>
    </li>
    <li>
        <div class="form-group">
              <label>Sub-Contract</label>
              <select class="form-control" name="subcontract_id" id="subsubsubcatsSelect">
                    </select>
        </div>
    </li>
    <li>
        <div class="form-group">
              <label>Site</label>
              <select class="form-control" name="site_id" id="subsubsubsubcatsSelect">
                    </select>
        </div>
    </li>
      <li>
        <div class="form-group">
              <label>Integration</label>
              <select class="form-control" name="src_type_id" id="subsubsubsubsubcatsSelect">
                    </select>
        </div>
    </li>
  </ul>
</li>

<li>
<button id="submitfilter" type="button" class="btn btn-primary" onclick="submitfilter()">Submit</button>
</li>

</ul>
</div>

</div>
<div class="container-fluid">

<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                      <li class="active"><a href="#explorerview" data-toggle="tab">Explorer View</a></li>
                                      <li><a href="#orgview" data-toggle="tab">Org Chart View</a></li>
                            </ul>
    </div>
<div class="panel-body">
<div class="tab-content">
        <div class="tab-pane fade in active" id="explorerview">
          <span style="display: none;" id="searchtree">
          <div class="row">
          <div class="col-md-6">
          <ul class="list-inline">
            <li><input class="form-control" id="explorer-container-search" type="text" placeholder="Search Node..."></li>
            <li><button type="button" class="btn btn-primary" id="btn-tree-filter-node">Filter Nodes</button></li>
            <li><button type="button" class="btn btn-primary" id="btn-tree-cancel">Clear</button></li>
            <li id="collapse_tree_button" style="display:none;"><button type="button" class="btn btn-primary" id="collapse_tree"><i class="fa fa-compress"></i> Collapse All</button></li>
            <li id="uncollapse_tree_button" style="display:none;"><button type="button" class="btn btn-primary" id="uncollapse_tree"><i class="fa fa-expand"></i> Expand All</button></li>
          </ul>
          </div>
          </div>
          </span>
          <div class="container-fluid">
            <div id="explorer-container"></div>
            <div id="treeloading" style="display:none;"><img src="{{ URL::to('app/img/light_blue_material_design_loading.gif') }}" alt="Be patient..." /></div>
          </div>
        </div>
        <div class="tab-pane fade" id="orgview">
        <span style="display: none;" id="searchorg">
          <div class="row">
            <div class="col-md-8">
            <ul class="list-inline">
            <li><input class="form-control" type="text" id="key-word" placeholder="Search Node..."></li>
            <li><button type="button" class="btn btn-primary" id="btn-filter-node">Filter Nodes</button></li>
            <li><button type="button" class="btn btn-primary" id="btn-cancel">Clear</button></li>
            <li><button class="btn btn-primary reset" type="button"><i class="fa fa-refresh"></i> Reset View</button></li>
            <li><p>Once the chart is loaded, you can pan/drag the chart by holding left-click and zoom in/out by scrolling up/down.</p></li>
            </ul>
            </div>
          </div>
        </span>
          <div class="container-fluid">
            <div id="chart-container"></div>
            <div id="orgloading" style="display:none;"><img src="{{ URL::to('app/img/light_blue_material_design_loading.gif') }}" alt="Be patient..." /></div>
          </div>
          </div>
</div>
</div>
</div>

</div>

</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function(){
    $(".test").click(function(){
        $("#test").collapse('toggle');
    });
});

 $('#element_view_filter').on('change', function(){
    if($(this).val() == "1"){
        $('#filter_contract').show();
        $('#filter_site').hide();
        $('#filter_integration').hide();
    }
    else if($(this).val() == "2"){
        $('#filter_site').show();
        $('#filter_contract').hide();
        $('#filter_integration').hide();
    }
    else if($(this).val() == "3"){
        $('#filter_integration').show();
        $('#filter_site').hide();
        $('#filter_contract').hide();
    }
});

function submitfilter()
{

    // parameter setting
    var json = null;
    if ($('#element_view_filter').val() == null || $('#element_view_filter').val() == ''){
      alertify.alert('Please select a filter from the dropdown');
      return;
    }
    else {
    // reset everything
    $("#searchtree").show();
    $("#searchorg").show();
    $('#uncollapse_tree_button').show();
    $("#explorer-container").jstree('destroy');
    $('#chart-container').empty();
    // show loading img
    $("#treeloading").show();
    $("#orgloading").show();
    if ($('#element_view_filter').val() == "1"){
      var client_id = $('#categoriesSelect').val();
      var depth = 5;
      var customer_id_option = document.getElementById("subcatsSelect");
      var contract_id_option = document.getElementById("subsubcatsSelect");
      var subcontract_id_option = document.getElementById("subsubsubcatsSelect");
      var site_id_option = document.getElementById("subsubsubsubcatsSelect");
      var src_type_id_option = document.getElementById("subsubsubsubsubcatsSelect");
      var url_post_client = 'elements_tree_view/initdata/client/'+client_id+'?';
      var url_post = 'elements_tree_view/initdata/client/'+client_id+'?';
        if (customer_id_option.options.length != 0){
          var customer_id = $('#subcatsSelect').val();
          var url_post_customer = url_post_client+'&customer_id='+customer_id;
          var url_post = url_post_customer;
        }
        if (contract_id_option.options.length != 0){
          var contract_id = $('#subsubcatsSelect').val();
          var url_post_contract = url_post_customer+'&contract_id='+contract_id;
          var url_post = url_post_contract;
        }
        if (subcontract_id_option.options.length != 0){
          var subcontract_id = $('#subsubsubcatsSelect').val();
          var url_post_subcontract = url_post_contract+'&subcontract_id='+subcontract_id;
          var url_post = url_post_subcontract;
        }
        if (site_id_option.options.length != 0){
          var site_id = $('#subsubsubsubcatsSelect').val();
          var url_post_site = url_post_subcontract+'&site_id='+site_id;
          var url_post = url_post_site;
        }
        if (src_type_id_option.options.length != 0){
          var src_type_id = $('#subsubsubsubsubcatsSelect').val();
          var url_post_integration = url_post_site+'&src_type_id='+src_type_id;
          var url_post = url_post_integration;
        }
    }
    else if ($('#element_view_filter').val() == "2"){
      var site_id = $('#element_view_filter_site').val();
      var depth = 2;
      var url_post = 'elements_tree_view/initdata/site/'+site_id;
    }
    else if ($('#element_view_filter').val() == "3"){
      var src_type_id = $('#element_view_filter_integration').val();
      var url_post = 'elements_tree_view/initdata/integration/'+src_type_id;
    }
      $.ajax({
          'async': true,
          'url': url_post,
          'dataType': "json",
          'success': function (data) {
              $("#treeloading").hide();
              $("#orgloading").hide();
              json = data;
                $('#explorer-container').jstree({
                      'core': {
                          'data': json
                      },
                      "plugins" : ["wholerow", "search"]
                  });

                $('#chart-container').orgchart({
                  'data' : json,
                  'depth': depth,
                  'nodeContent': 'title',
                  'pan': true,
                  'zoom': true,
                });
          }
      });
    }
}

$('#uncollapse_tree').on('click', function() {
  $('#explorer-container').jstree('open_all');
 $('#collapse_tree_button').show();
 $('#uncollapse_tree_button').hide();
});

$('#collapse_tree').on('click', function() {
  $('#explorer-container').jstree('close_all');
 $('#uncollapse_tree_button').show();
 $('#collapse_tree_button').hide();
});

function filterNodes(keyWord) {
  if(!keyWord.length) {
    alertify.alert('Please type a keyword.');
    return;
  } else {
    var $chart = $('.orgchart');

    // distinguish the matched nodes and the unmatched nodes according to the given key word
    $chart.find('.node').filter(function(index, node) {
        return $(node).text().toLowerCase().indexOf(keyWord) > -1;
      }).addClass('matched')
      .closest('table').parents('table').find('tr:first').find('.node').addClass('retained');
    // hide the unmatched nodes
    $chart.find('.matched,.retained').each(function(index, node) {
      $(node).removeClass('slide-up')
        .closest('.nodes').removeClass('hidden')
        .siblings('.lines').removeClass('hidden');
      var $unmatched = $(node).closest('table').parent().siblings().find('.node:first:not(.matched,.retained)')
        .closest('table').parent().addClass('hidden');
      $unmatched.parent().prev().children().slice(1, $unmatched.length * 2 + 1).addClass('hidden');
    });
    // hide the redundant descendant nodes of the matched nodes
    $chart.find('.matched').each(function(index, node) {
      if (!$(node).closest('tr').siblings(':last').find('.matched').length) {
        $(node).closest('tr').siblings().addClass('hidden');
      }
    });
  }
}
function clearFilterResult() {
  $('.orgchart')
    .find('.node').removeClass('matched retained')
    .end().find('.hidden').removeClass('hidden')
    .end().find('.slide-up, .slide-left, .slide-right').removeClass('slide-up slide-right slide-left');
}
$('#btn-tree-filter-node').on('click', function() {
      var v = $('#explorer-container-search').val();
      $('#explorer-container').jstree('search', v);
 });
$('#btn-tree-cancel').on('click', function() {
      $('#explorer-container-search').val('');
      $('#explorer-container').jstree().clear_search();
 });

$('#explorer-container-search').on('keyup', function(event) {
  if (event.which === 13) {
          var v = $('#explorer-container-search').val();
          $('#explorer-container').jstree('search', v);
  }
});
$('.reset').click(function() {
  $('.orgchart').css('transform','');
});
$('#btn-filter-node').on('click', function() {
  clearFilterResult();
  filterNodes($('#key-word').val());
});
$('#btn-cancel').on('click', function() {
  clearFilterResult();
  filterNodes($('#key-word').val(''));
});
$('#key-word').on('keyup', function(event) {
  if (event.which === 13) {
    clearFilterResult();
    filterNodes(this.value);
  } else if (event.which === 8 && this.value.length === 0) {
    clearFilterResult();
  }
});


</script>
@endsection