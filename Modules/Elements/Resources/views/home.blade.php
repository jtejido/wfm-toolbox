@extends('elements::layouts.app')
@section('content')
  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> Elements</h3> 
    <p>Please select an Integration from the dropdown list.</p>
  </div>
<div class="dropdown">
  <button class="btn btn-primary dropdown-toggle test" type="button" data-toggle="dropdown">Select Integration
  <span class="caret"></span></button>
  <ul class="dropdown-menu scrollable-menu collapse" id="test">
@foreach($integration as $key => $value)
	   <li><a href="{{ URL::to('elements/'.$value->src_type_id.'') }}">{!!$value->integration!!}</a></li>
@endforeach
  </ul>
</div>
@endsection
