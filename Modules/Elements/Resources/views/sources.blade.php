@extends('elements::layouts.app')
@section('content')
  <div class="hero-unit">
    <h3><i class="fa fa-tachometer" aria-hidden="true"></i> Sources</h3> 
  </div>
<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-bordered table-condensed" id="sortable">
<thead>
<tr>
<th class="select-filter">Source_Schema*</th>
<th class="select-filter">Source_Table*</th>
<th>Src_id</th>
<th>Src_type_id</th>
<th class="select-filter">Integration_Type*</th>
<th>Element_Name*</th>
<th>Source_Where*</th>
</tr>
</thead>
<tfoot>
<tr>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
</tr>
</tfoot>
<tbody>
@foreach($sources as $key => $value)
 <tr>
            <td><a href="#" class="testEdit5" data-type="text" data-column="source_schema" data-url="{{route('sources/update', ['source_schema'=>$value->source_schema, 'source_table'=>$value->source_table, 'src_id'=>$value->src_id, 'src_type_id'=>$value->src_type_id, 'integration_type'=>$value->integration_type])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="source_schema">{{ $value->source_schema }}</a></td>
            <td><a href="#" class="testEdit4" data-type="text" data-column="source_table" data-url="{{route('sources/update', ['source_schema'=>$value->source_schema, 'source_table'=>$value->source_table, 'src_id'=>$value->src_id, 'src_type_id'=>$value->src_type_id, 'integration_type'=>$value->integration_type])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="source_table">{{ $value->source_table }}</a></td>
            <td>{{ $value->src_id }}</td>
            <td>{{ $value->src_type_id }}</td>
            <td><a href="#" class="testEdit3" data-column="integration_type" data-url="{{route('sources/update', ['source_schema'=>$value->source_schema, 'source_table'=>$value->source_table, 'src_id'=>$value->src_id, 'src_type_id'=>$value->src_type_id, 'integration_type'=>$value->integration_type])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="integration_type">{{ $value->integration_type }}</a></td>
            <td><a href="#" class="testEdit" data-type="text" data-column="element_name" data-url="{{route('sources/update', ['source_schema'=>$value->source_schema, 'source_table'=>$value->source_table, 'src_id'=>$value->src_id, 'src_type_id'=>$value->src_type_id, 'integration_type'=>$value->integration_type])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="element_name">{{ $value->element_name }}</a></</td>
            <td><a href="#" class="testEdit2" data-type="text" data-column="source_where" data-url="{{route('sources/update', ['source_schema'=>$value->source_schema, 'source_table'=>$value->source_table, 'src_id'=>$value->src_id, 'src_type_id'=>$value->src_type_id, 'integration_type'=>$value->integration_type])}}" data-pk="{{$value->src_type_id}}" data-title="change" data-name="source_where">{{ $value->source_where }}</a></td>
</tr>
 @endforeach
</tbody>
</table>
</div>
</div>
</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit2').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit3').editable({
        type: 'select',
        source: [
            {value: 'IDM', text: 'IDM'},
            {value: 'Element', text: 'Element'},
        ],
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });
    $('.testEdit4').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit5').editable({
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

});

  $(document).ready(function() {
    $('#sortable').DataTable({
      "stateSave": true,
        "scrollX": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        },
      "columnDefs": [
        { "orderable": false, "targets": 0 },
        { "targets": [1,2,5], type: "html" }
                    ],
      "order": [[ 1, "desc" ]],
        initComplete: function () {
        this.api().columns('.select-filter').every( function () {
            var column = this;
            var select = $('<select name="filter"><option value=""></option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            column.data().unique().sort().each( function ( d, j ) {
               // For first column
               // ignore HTML tags 
                if((column.index() == 1) || (column.index() == 2) || (column.index() == 5))
                { d = $(d).text(); }
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );
    }
} );
    var usedNames = {};
    $('select[name="filter"] > option').each(function(){
      if(usedNames[this.text]) {
          $(this).remove();
      } else {
          usedNames[this.text] = this.value;
      }
  });
} );

</script>
@endsection