<?php

Route::group(['middleware' => ['web', 'auth', 'module:elements'], 'namespace' => 'Modules\Elements\Http\Controllers'], function()
{

            Route::group(['middleware' => ['permission']], function()
            {

                  Route::post('country/update/{country_id}', ['as' => 'country/update', 'uses' => 'CountryController@update']);
                  
                  Route::post('client/update/{client_id}', ['as' => 'client/update', 'uses' => 'ClientController@update']);
                  
                  Route::post('sources/update/{source_schema}/{source_table}/{src_id}/{src_type_id}/{integration_type}', ['as' => 'sources/update', 'uses' => 'SourceController@update']);
                  
                  Route::post('integration/update/{src_type_id}', ['as' => 'integration/update', 'uses' => 'IntegrationController@update']);
                  Route::post('integration/bulk_update', ['as' => 'integration/bulk_update', 'uses' => 'IntegrationController@bulk_update']);
                  
                  Route::post('costcentre/update/{costcentre_id}', ['as' => 'costcentre/update', 'uses' => 'CostCentreController@update']);
                  Route::post('costcentre/bulk_update', ['as' => 'costcentre/bulk_update', 'uses' => 'CostCentreController@bulk_update']);
                  
                  Route::post('contract/update/{contract_id}', ['as' => 'contract/update', 'uses' => 'ContractController@update']);
                  Route::post('contract/bulk_update', ['as' => 'contract/bulk_update', 'uses' => 'ContractController@bulk_update']);
                  
                  Route::post('customer/update/{customer_id}', ['as' => 'customer/update', 'uses' => 'CustomerController@update']);
                  Route::post('customer/bulk_update', ['as' => 'customer/bulk_update', 'uses' => 'CustomerController@bulk_update']);
                  
                  Route::post('ibg/update/{ibg_id}', ['as' => 'ibg/update', 'uses' => 'IBGController@update']);
                  
                  Route::post('language/update/{language_id}', ['as' => 'language/update', 'uses' => 'LanguageController@update']);
                  
                  Route::post('region/update/{region_id}', ['as' => 'region/update', 'uses' => 'RegionController@update']);
                  
                  Route::post('subcontract/update/{subcontract_id}', ['as' => 'subcontract/update', 'uses' => 'SubContractController@update']);
                  
                  Route::post('timezones/update/{timezone_name}', ['as' => 'timezones/update', 'uses' => 'TimeZoneController@update'])->where('timezone_name', '(.*)'); //catch the slash on the variable
                  
                  Route::post('sites/update/{site_id}', ['as' => 'sites/update', 'uses' => 'SiteController@update']);
                  
              	Route::post('elements/update/{src_type_id}/{src_id}', ['as' => 'elements/update', 'uses' => 'ElementController@update']);
              	Route::post('elements/bulk_update', ['as' => 'elements/bulk_update', 'uses' => 'ElementController@bulk_update']);
                  
            });
            Route::get('country', 'CountryController@index');
            Route::get('client', 'ClientController@index');
            Route::get('sources', 'SourceController@index');
            Route::get('integration', 'IntegrationController@index');
            Route::get('timezones', 'TimeZoneController@index');
            Route::get('costcentre', 'CostCentreController@index');
            Route::get('contract', 'ContractController@index');
            Route::get('customer', 'CustomerController@index');
            Route::get('region', 'RegionController@index');
            Route::get('ibg', 'IBGController@index');
            Route::get('language', 'LanguageController@index');
            Route::get('subcontract', 'SubContractController@index');
            Route::get('sites', 'SiteController@index');
            Route::get('elements/{src_type_id}', 'ElementController@view');
            Route::get('elements', 'ElementController@index');
            Route::get('elements_tree_view/initdata/client/{client_id?}', 'ElementController@viewclient');
            Route::get('elements_tree_view/initdata/site/{site_id}', 'ElementController@viewsite');
            Route::get('elements_tree_view/initdata/integration/{src_type_id}', 'ElementController@viewintegration');
            Route::get('elements_tree_view', 'ElementController@viewtreeindex');

});
