<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Contract;
use \App\Library\QueryTrack;

class ContractController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $contracts = DB::table('meta_elements.contract')->select('meta_elements.contract.contract_id', 'meta_elements.contract.contract', 'meta_elements.customer.customer')->leftJoin('meta_elements.customer', 'meta_elements.contract.customer_id', '=', 'meta_elements.customer.customer_id')->get();

        $customers = DB::select(DB::raw("select customer, customer_id from meta_elements.customer order by 1"));


        return view('elements::contract')->with('contracts', $contracts)->with('customers', $customers);


    }


    public function update(Request $request, $contract_id)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = Contract::select()
                ->where('contract_id', '=', $contract_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'contract');
            return response()->json([ 'code'=>200], 200);
    }


}