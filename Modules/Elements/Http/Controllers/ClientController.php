<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Client;
use \App\Library\QueryTrack;

class ClientController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $clients = DB::table('meta_elements.client')->select('meta_elements.client.client_id', 'meta_elements.client.client', 'meta_elements.ibg.ibg', 'meta_elements.region.region', 'meta_elements.client.ibg_id', 'meta_elements.client.region_id')->leftJoin('meta_elements.ibg', 'meta_elements.client.ibg_id', '=', 'meta_elements.ibg.ibg_id')->leftJoin('meta_elements.region', 'meta_elements.client.region_id', '=', 'meta_elements.region.region_id')->get();

        $regions = DB::select(DB::raw("select region, region_id from meta_elements.region order by 1"));
        $ibgs = DB::select(DB::raw("select ibg, ibg_id from meta_elements.ibg order by 1"));

        return view('elements::client')->with('clients', $clients)->with('regions', $regions)->with('ibgs', $ibgs);


    }


    public function update(Request $request, $client_id)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = Client::select()
                ->where('client_id', '=', $client_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'client');
            return response()->json([ 'code'=>200], 200);
    }



}