<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\CostCentre;
use \App\Library\QueryTrack;

class CostCentreController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $costcentres = DB::table('meta_elements.costcentre')->select('meta_elements.costcentre.costcentre_id', 'meta_elements.costcentre.costcentre', 'meta_elements.costcentre.description','meta_elements.site.site', 'meta_elements.contract.contract', 'meta_elements.costcentre.src_type_id', 'meta_elements.client.client')->leftJoin('meta_elements.site', 'meta_elements.costcentre.site_id', '=', 'meta_elements.site.site_id')->leftJoin('meta_elements.contract', 'meta_elements.costcentre.contract_id', '=', 'meta_elements.contract.contract_id')->leftJoin('meta_elements.client', 'meta_elements.costcentre.client_id', '=', 'meta_elements.client.client_id')->get();

        $sites = DB::select(DB::raw("select site, site_id from meta_elements.site order by 1"));
        $contracts = DB::select(DB::raw("select contract, contract_id FROM meta_elements.contract order by 1"));
        $clients = DB::select(DB::raw("select client, client_id FROM meta_elements.client order by 1"));

        return view('elements::costcentre')->with('costcentres', $costcentres)->with('sites', $sites)->with('contracts', $contracts)->with('clients', $clients);


    }


    public function update(Request $request, $costcentre_id)
    {

        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = CostCentre::select()
                ->where('costcentre_id', '=', $costcentre_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'costcentre');
            return response()->json([ 'code'=>200], 200);
        
    }

    public function bulk_update(Request $request)
    {

    }


}