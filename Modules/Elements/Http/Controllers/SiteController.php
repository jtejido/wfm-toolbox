<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Sites;
use \App\Library\QueryTrack;

class SiteController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $sites = DB::table('meta_elements.site')->select('meta_elements.site.site_id', 'meta_elements.site.site', 'meta_elements.site.city', 'meta_elements.site.country', 'meta_elements.region.region', 'meta_elements.site.site_timezome', 'meta_elements.site.daily_hours')->leftJoin('meta_elements.region', 'meta_elements.site.region_id', '=', 'meta_elements.region.region_id')->get();

        $regions = DB::select(DB::raw("select region, region_id from meta_elements.region order by 1"));

        return view('elements::sites')->with('sites', $sites)->with('regions', $regions);


    }


    public function update(Request $request, $site_id)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';

            DB::connection()->enableQueryLog();
            $test = Sites::select()
                ->where('site_id', '=', $site_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'site');
            return response()->json([ 'code'=>200], 200);
    }



}