<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Sources;
use \App\Library\QueryTrack;

class SourceController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $sources = Sources::all();

        return view('elements::sources')->with('sources', $sources);


    }


    public function update(Request $request, $source_schema, $source_table, $src_id, $src_type_id, $integration_type)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';

            DB::connection()->enableQueryLog();
            $test = Sources::select()
                ->where('source_schema', '=', $source_schema)
                ->where('source_table', '=', $source_table)
                ->where('src_id', '=', $src_id)
                ->where('src_type_id', '=', $src_type_id)
                ->where('integration_type', '=', $integration_type)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'source');
            return response()->json([ 'code'=>200], 200);
    }



}