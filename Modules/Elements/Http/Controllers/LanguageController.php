<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Language;
use \App\Library\QueryTrack;

class LanguageController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }


    public function index()
    {

        $languages = Language::all();

        return view('elements::language')->with('languages', $languages);


    }


    public function update(Request $request, $language_id)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = Language::select()
                ->where('language_id', '=', $language_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'language');
            return response()->json([ 'code'=>200], 200);
        
    }



}