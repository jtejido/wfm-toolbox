<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Customer;
use \App\Library\QueryTrack;

class CustomerController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $customers = DB::table('meta_elements.customer')->select('meta_elements.customer.customer_id', 'meta_elements.customer.customer', 'meta_elements.client.client')->leftJoin('meta_elements.client', 'meta_elements.customer.client_id', '=', 'meta_elements.client.client_id')->get();
        $clients = DB::select(DB::raw("select client, client_id from meta_elements.client order by 1"));

        return view('elements::customer')->with('customers', $customers)->with('clients', $clients);


    }


    public function update(Request $request, $customer_id)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = Customer::select()
                ->where('customer_id', '=', $customer_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'customer');
            return response()->json([ 'code'=>200], 200);
    }

    public function bulk_update(Request $request)
    {

    }


}