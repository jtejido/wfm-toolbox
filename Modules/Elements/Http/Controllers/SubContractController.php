<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\SubContract;
use \App\Library\QueryTrack;

class SubContractController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

     public function index()
    {

        $subcontracts = DB::table('meta_elements.subcontract')->select('meta_elements.subcontract.subcontract_id', 'meta_elements.subcontract.subcontract', 'meta_elements.contract.contract')->leftJoin('meta_elements.contract', 'meta_elements.subcontract.contract_id', '=', 'meta_elements.contract.contract_id')->get();

        $contracts = DB::select(DB::raw("select contract, contract_id from meta_elements.contract order by 1"));

        return view('elements::subcontract')->with('subcontracts', $subcontracts)->with('contracts', $contracts);


    }


    public function update(Request $request, $subcontract_id)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = SubContract::select()
                ->where('subcontract_id', '=', $subcontract_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'subcontract');
            return response()->json([ 'code'=>200], 200);
    }



}