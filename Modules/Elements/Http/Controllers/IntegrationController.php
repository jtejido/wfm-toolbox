<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Integration;
use \App\Library\QueryTrack;

class IntegrationController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $integrations = Integration::all();
        $type = Integration::distinct()->select('type')->orderBy('type', 'asc')->get();
        $sub_type = Integration::distinct()->select('sub_type')->orderBy('sub_type', 'asc')->get();
        $element_type = Integration::distinct()->select('element_type')->orderBy('element_type', 'asc')->get();
        $system_name = Integration::distinct()->select('system_name')->orderBy('system_name', 'asc')->get();
        $test_model = new Integration();
        $fillable_columns = $test_model->getFillable();
        foreach ($fillable_columns as $key => $value) {
            $test_columns[$value] = $value;
        }


        return view('elements::integration')->with('integrations', $integrations)->with('type', $type)->with('sub_type', $sub_type)->with('element_type', $element_type)->with('system_name', $system_name)->with('test_columns', $test_columns);


    }


    public function update(Request $request, $src_type_id)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = Integration::select()
                ->where('src_type_id', '=', $src_type_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'integration');
            return response()->json([ 'code'=>200], 200);


    }

    public function bulk_update(Request $request)
    {
        if (Input::has('ids_to_edit') && Input::has('bulk_name') && (Input::has('bulk_value') || Input::has('updatetype') || Input::has('updatesubtype') || Input::has('updateelementtype') || Input::has('updatesystemname') || Input::has('updateticketreq'))) {
            $src_type_ids = Input::get('ids_to_edit');
            $bulk_name = Input::get('bulk_name');
            if (Input::has('updatetype')){
                $bulk_value = Input::get('updatetype');
            }
            elseif (Input::has('updatesubtype')){
                $bulk_value = Input::get('updatesubtype');
            }
            elseif (Input::has('updateelementtype')){
                $bulk_value = Input::get('updateelementtype');
            }
            elseif (Input::has('updatesystemname')){
                $bulk_value = Input::get('updatesystemname');
            }
            elseif (Input::has('updateticketreq')){
                $bulk_value = Input::get('updateticketreq');
            }
            else {
                $bulk_value = Input::get('bulk_value');
            }
            foreach ($src_type_ids as $src_type_id) {
                DB::connection()->enableQueryLog();
                $test = Integration::select()
                    ->where('src_type_id', '=', $src_type_id)
                    ->update([$bulk_name => $bulk_value]);
                    $queries = DB::getQueryLog();
                    $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'ibg');
            }

            $message = "Succesfully updated records.";
        } else {
            $message = "Error. Empty or Wrong data provided.";
            return Redirect::back()->withErrors(array('message' => $message))->withInput();
        }
        return Redirect::back()->with('message', $message);
    }


}