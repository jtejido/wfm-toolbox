<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Region;
use \App\Library\QueryTrack;

class RegionController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $regions = Region::all();

       return view('elements::region')->with('regions', $regions);


    }


    public function update(Request $request, $region_id)
    {
        $column_name = Input::get('name');
        $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = Region::select()
                ->where('region_id', '=', $region_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'region');
            return response()->json([ 'code'=>200], 200);
    }



}