<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\TimeZone;
use \App\Library\QueryTrack;

class TimeZoneController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }


    public function index()
    {

        $timezones = TimeZone::all();

        return view('elements::timezone')->with('timezones', $timezones);


    }


    public function update(Request $request, $timezone_name)
    {
        $column_name = Input::get('name');
        if(Input::get('name') == 'is_dst'){
               $column_value = Input::get('value');
        }
        else {
               $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        }
            DB::connection()->enableQueryLog();
            $test = TimeZone::select()
                ->where('timezone_name', '=', $timezone_name)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'timezone');
            return response()->json([ 'code'=>200], 200);
    }



}