<?php
namespace Modules\Elements\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use PDO;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Elements\Customer;
use \App\Models\Elements\Contract;
use \App\Models\Elements\Client;
use \App\Models\Elements\SubContract;
use \App\Models\Elements\Elements;
use \App\Models\Elements\Sites;
use \App\Models\Elements\Integration;
use \App\Models\Elements\ContractMap;
use \App\Models\Elements\ElementsMap;
use \App\Library\QueryTrack;

class ElementController extends Controller
{
    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }
        
    public function index()
    {

        $integration = DB::select(DB::raw("select integration, src_type_id from meta_elements.integration order by 1"));

        return view('elements::home')->with('integration', $integration);
    }
    public function viewtreeindex()
    {
          $site = ElementsMap::select('site_id', 'element_site')->distinct('site_id')->orderBy('element_site')->get();

          $integration = ElementsMap::select('src_type_id', 'integration')->where('src_type_id', '!=', '52')->distinct('src_type_id')->orderBy('integration')->get();

          $result = ElementsMap::select('client', 'client_id')->distinct('client_id')->orderBy('client')->get();

          foreach($result as $row){
          $clients[] = array("client_id" => $row->client_id, "client" => $row->client);
          }
          $result = ElementsMap::select('client_id', 'customer_id', 'element_customer')->distinct('customer_id')->orderBy('element_customer')->get();

          foreach($result as $row){
            $customers[$row->client_id][] = array("customer_id" => $row->customer_id, "customer" => $row->element_customer);
          }
          $result = ElementsMap::select('contract_id', 'customer_id', 'element_contract')->distinct('customer_id')->orderBy('element_contract')->get();

           foreach($result as $row){
            $contracts[$row->customer_id][] = array("contract_id" => $row->contract_id, "contract" => $row->element_contract);
          }
          $result = ElementsMap::select('subcontract_id', 'contract_id', 'element_subcontract')->distinct('subcontract_id')->orderBy('element_subcontract')->get();

           foreach($result as $row){
            $subcontracts[$row->contract_id][] = array("subcontract_id" => $row->subcontract_id, "subcontract" => $row->element_subcontract);
          }
          $result = ElementsMap::select('site_id', 'subcontract_id', 'element_site')->distinct('site_id')->orderBy('element_site')->get();

           foreach($result as $row){
            $sites[$row->subcontract_id][] = array("site_id" => $row->site_id, "site" => $row->element_site);
          }
          $result = ElementsMap::select('src_type_id', 'site_id', 'subcontract_id', 'integration')->where('src_type_id', '!=', '52')->distinct('src_type_id')->orderBy('integration')->get();

           foreach($result as $row){
            $integrations[$row->site_id][$row->subcontract_id][] = array("src_type_id" => $row->src_type_id, "integration" => $row->integration);
          }

          $jsonCats = json_encode($clients);
          $jsonSubCats = json_encode($customers);
          $jsonSubSubCats = json_encode($contracts);
          $jsonSubSubSubCats = json_encode($subcontracts);
          $jsonSubSubSubSubCats = json_encode($sites);
          $jsonSubSubSubSubSubCats = json_encode($integrations);

        return view('elements::elements_tree_view')->with('site', $site)->with('integration', $integration)->with('jsonCats', $jsonCats)->with('jsonCats', $jsonCats)->with('jsonSubCats', $jsonSubCats)->with('jsonSubSubCats', $jsonSubSubCats)->with('jsonSubSubSubCats', $jsonSubSubSubCats)->with('jsonSubSubSubSubCats', $jsonSubSubSubSubCats)->with('jsonSubSubSubSubSubCats', $jsonSubSubSubSubSubCats);
    }

    public function viewclient(Request $client_id)
    {
        $client_id_temp = $client_id->client_id;
        $client_name = Client::select('client')->where('client_id', '=', $client_id_temp)->first();
        $results['name'] = 'Client Name';
        $results['title'] = $client_name->client;
        $results['text'] = 'Client Name : ' . $client_name->client;
            
        $customer_name = ElementsMap::select('element_customer', 'customer_id', 'client_id')->distinct('element_customer')->where('client_id', '=', $client_id_temp);
        if((isset($_GET['customer_id'])) && ($_GET['customer_id'] != -1)) {
          $customer_name = $customer_name->where('customer_id', $client_id->customer_id);
        }
         $customer_name = $customer_name->get();

        foreach ($customer_name as $customer_name_row)
          {
            $contract_name = ElementsMap::select('element_contract', 'customer_id', 'client_id', 'contract_id')->distinct('element_contract')->where('client_id', '=', $customer_name_row->client_id)->where('customer_id', '=', $customer_name_row->customer_id);
            if((isset($_GET['contract_id'])) && ($_GET['contract_id'] != -1)) {
              $contract_name = $contract_name->where('contract_id', $client_id->contract_id);
            }
            $contract_name = $contract_name->get();
           $results6 = array();

                 foreach ($contract_name as $contract_name_row)
                    {
                     $subcontract_name = ElementsMap::select('element_subcontract', 'subcontract_id', 'contract_id', 'customer_id', 'client_id')->distinct('element_subcontract')->where('client_id', '=', $contract_name_row->client_id)->where('customer_id', '=', $contract_name_row->customer_id)->where('contract_id', '=', $contract_name_row->contract_id);
                      if((isset($_GET['subcontract_id'])) && ($_GET['subcontract_id'] != -1)) {
                        $subcontract_name = $subcontract_name->where('subcontract_id', $client_id->subcontract_id);
                      }              
                     $subcontract_name = $subcontract_name->get();
                    $results5 = array();
                    foreach ($subcontract_name as $subcontract_name_row)
                        {
                        $site_name = ElementsMap::select('element_site', 'subcontract_id', 'contract_id', 'client_id', 'customer_id', 'site_id')->distinct('element_site')->where('client_id', '=', $subcontract_name_row->client_id)->where('customer_id', '=', $subcontract_name_row->customer_id)->where('contract_id', '=', $subcontract_name_row->contract_id)->where('subcontract_id', '=', $subcontract_name_row->subcontract_id);
                        if((isset($_GET['site_id'])) && ($_GET['site_id'] != -1)) {
                          $site_name = $site_name->where('site_id', $client_id->site_id);
                        }   
                        $site_name = $site_name->get();
                        $results4 = array();
                            foreach ($site_name as $site_name_row)
                            {
                                $integration_name = ElementsMap::select('integration', 'src_type_id', 'subcontract_id', 'contract_id', 'client_id', 'customer_id', 'site_id')->distinct('integration')->where('customer_id', '=', $site_name_row->customer_id)->where('site_id', '=', $site_name_row->site_id)->where('contract_id', '=', $site_name_row->contract_id)->where('subcontract_id', '=', $site_name_row->subcontract_id)->where('client_id', '=', $site_name_row->client_id);
                                if((isset($_GET['src_type_id'])) && ($_GET['src_type_id'] != -1)) {
                                  $integration_name = $integration_name->where('src_type_id', $client_id->src_type_id);
                                } 
                                $integration_name = $integration_name->where('src_type_id', '!=', '52')->get();
                                $results3 = array();
                                  foreach ($integration_name as $integration_name_row)
                                  {
                                      $elements = ElementsMap::select()->where('src_type_id', '=', $integration_name_row->src_type_id)->where('customer_id', '=', $integration_name_row->customer_id)->where('site_id', '=', $integration_name_row->site_id)->where('contract_id', '=', $integration_name_row->contract_id)->where('subcontract_id', '=', $integration_name_row->subcontract_id)->where('client_id', '=', $integration_name_row->client_id)->get();
                                      $results2 = array();
                                         foreach ($elements as $element_row)
                                            {
                                              $results2[] = ['name' => 'Element Name', 'title' => $element_row->element, 'text' => 'Element Name : '.$element_row->element];
                                            }
                                      $results3[] = ['name' => 'Integration Name', 'title' => $integration_name_row->integration, 'text' => 'Integration Name : '.$integration_name_row->integration, 'children' => $results2];
                                   }
                              $results4[] = ['name' => 'Site Name', 'title' => $site_name_row->element_site, 'text' => 'Site Name : '.$site_name_row->element_site, 'children' => $results3];
                            }
                          $results5[] = ['name' => 'SubContract Name', 'title' => $subcontract_name_row->element_subcontract, 'text' => 'SubContract Name : '.$subcontract_name_row->element_subcontract, 'children' => $results4];
                        }
                     $results6[] = ['name' => 'Contract Name', 'title' => $contract_name_row->element_contract, 'text' => 'Contract Name : '.$contract_name_row->element_contract, 'children' => $results5];
                    }
            $results['children'][] = ['name' => 'Customer Name', 'title' => $customer_name_row->element_customer, 'text' => 'Customer Name : '.$customer_name_row->element_customer, 'children' => $results6];
        }
        return response()->json($results);
    }


    public function viewsite($site_id)
    {
        $site_name = Sites::select('site')->where('site_id', '=', $site_id)->first();
        $results['name'] = 'Site Name';
        $results['title'] = $site_name->site;
        $results['text'] = 'Site Name : ' . $site_name->site;
            
        $integration_name = ElementsMap::select('integration', 'src_type_id', 'site_id')->distinct('src_type_id')->where('site_id', '=', $site_id)->where('src_type_id', '!=', '52')->get();
              foreach ($integration_name as $integration_name_row)
              {
                if(!empty($integration_name_row->integration)){
                  $elements = ElementsMap::select()->where('src_type_id', '=', $integration_name_row->src_type_id)->where('site_id', '=', $integration_name_row->site_id)->get();
                  $results2 = array();
                     foreach ($elements as $element_row)
                        {
                          if(!empty($element_row->element)){
                            $results2[] = ['name' => 'Element Name', 'title' => $element_row->element, 'text' => 'Element Name : '.$element_row->element];
                          }
                        }
                  $results['children'][] = ['name' => 'Integration Name', 'title' => $integration_name_row->integration, 'text' => 'Integration Name : '.$integration_name_row->integration, 'children' => $results2];
                }  
               }
        return response()->json($results);
    }

        public function viewintegration($src_type_id)
    {
        $integration_name = Integration::select('integration')->where('src_type_id', '=', $src_type_id)->where('src_type_id', '!=', '52')->first();
        $results['name'] = 'Integration Name';
        $results['title'] = $integration_name->integration;
        $results['text'] = 'Integration Name : ' . $integration_name->integration;
            
        $elements = ElementsMap::select()->where('src_type_id', '=', $src_type_id)->get();
                     foreach ($elements as $element_row)
                        {
                          if(!empty($element_row->element)){
                            $results['children'][] = ['name' => 'Element Name', 'title' => $element_row->element, 'text' => 'Element Name : '.$element_row->element];
                          }
                        }
        return response()->json($results);
    }

    public function view($src_type_id)
    {
        $integration = DB::select(DB::raw("select integration, src_type_id from meta_elements.integration order by 1"));
        $languages = DB::select(DB::raw("select language, language_id from meta_elements.language order by 1"));
        $sites = DB::select(DB::raw("select site, site_id from meta_elements.site order by 1"));
        $costcentres = DB::select(DB::raw("select costcentre, costcentre_id FROM meta_elements.costcentre order by 1"));
        $timezones = DB::select(DB::raw("select timezone_name from meta_elements.time_zones order by 1"));
        $test_model = new Elements();
        $fillable_columns = $test_model->getFillable();
        foreach ($fillable_columns as $key => $value) {
            $test_columns[$value] = $value;
        }
        $result = Customer::select('customer', 'customer_id')->orderBy('customer')->get();

          foreach($result as $row){
            $categories[] = array("customer_id" => $row->customer_id, "customer" => $row->customer);
          }
           $result = Contract::select('contract_id', 'customer_id', 'contract')->orderBy('contract')->get();

           foreach($result as $row){
            $subcats[$row->customer_id][] = array("contract_id" => $row->contract_id, "contract" => $row->contract);
          }
            $result = SubContract::select('subcontract_id', 'contract_id', 'subcontract')->orderBy('subcontract')->get();

           foreach($result as $row){
            $subsubcats[$row->contract_id][] = array("subcontract_id" => $row->subcontract_id, "subcontract" => $row->subcontract);
          }
          $jsonCats = json_encode($categories);
          $jsonSubCats = json_encode($subcats);
          $jsonSubSubCats = json_encode($subsubcats);

        $elements = DB::table('meta_elements.elements')->select('meta_elements.elements.src_id', 'meta_elements.elements.element_name', 'meta_elements.elements.src_type_id','meta_elements.subcontract.subcontract', 'meta_elements.language.language', 'meta_elements.site.site', 'meta_elements.costcentre.costcentre','meta_elements.elements.element_source_timezone','meta_elements.elements.report_timezone')->leftJoin('meta_elements.subcontract', 'meta_elements.elements.subcontract_id', '=', 'meta_elements.subcontract.subcontract_id')->leftJoin('meta_elements.language', 'meta_elements.elements.language_id', '=', 'meta_elements.language.language_id')->leftJoin('meta_elements.site', 'meta_elements.elements.site_id', '=', 'meta_elements.site.site_id')->leftJoin('meta_elements.costcentre', 'meta_elements.elements.costcentre_id', '=', 'meta_elements.costcentre.costcentre_id')->where('meta_elements.elements.src_type_id', $src_type_id)->get();


       return view('elements::elements.view')->with('integration', $integration)->with('elements', $elements)->with('test_columns', $test_columns)->with('languages', $languages)->with('src_type_id', $src_type_id)->with('sites', $sites)->with('costcentres', $costcentres)->with('timezones', $timezones)->with('jsonCats', $jsonCats)->with('jsonSubCats', $jsonSubCats)->with('jsonSubSubCats', $jsonSubSubCats);


    }

    public function update(Request $request, $src_type_id, $src_id)
    {
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        
        if( Input::has('name') && Input::has('value')) {
            DB::connection()->enableQueryLog();
            $test = Elements::select()
                ->where('src_type_id', '=', $src_type_id)
                ->where('src_id', '=', $src_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'elements');
            return response()->json([ 'code'=>200], 200);
        }
        
        return response()->json([ 'error'=> 400, 'message'=> 'Not enought params' ], 400);
    }

    public function bulk_update(Request $request)
    {
            $src_type_id = Input::get('src_type_id');
        if (Input::has('ids_to_edit') && Input::has('bulk_name') && (Input::has('bulk_value') || Input::has('updatelanguage') || Input::has('updatesite') || Input::has('updatesubcontract_id') || Input::has('updatecostcentre') || Input::has('updatetimezone'))) {
            $src_ids = Input::get('ids_to_edit');
            $bulk_name = Input::get('bulk_name');
            if (Input::has('updatelanguage')){
                $bulk_value = Input::get('updatelanguage');
            }
            elseif (Input::has('updatesite')){
                $bulk_value = Input::get('updatesite');
            }
            elseif (Input::has('updatesubcontract_id')){
                $bulk_value = Input::get('updatesubcontract_id');
            }
            elseif (Input::has('updatecostcentre')){
                $bulk_value = Input::get('updatecostcentre');
            }
            elseif (Input::has('updatetimezone')){
                $bulk_value = Input::get('updatetimezone');
            }
            else {
                $bulk_value = Input::get('bulk_value');
            }
            foreach ($src_ids as $src_id) {
                DB::connection()->enableQueryLog();
                $test = Elements::select()
                    ->where('src_type_id', '=', $src_type_id)
                    ->where('src_id', '=', $src_id)
                    ->update([$bulk_name => $bulk_value]);
                    $queries = DB::getQueryLog();
                    $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'customer');
            }
            
            $message = "Succesfully updated records.";
        } else {
            $message = "Error. Empty or Wrong data provided.";
            return Redirect::back()->withErrors(array('message' => $message))->withInput();
        }
        return Redirect::back()->with('message', $message);
    }

}