<?php
namespace Modules\MssSearch\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\MSS\MSSSearch;
use \App\Library\SSP;

class MSSSearchController extends Controller
{


    public function index()
    {

        
        return view('msssearch::mss_search');


    }


    public function search()
    {
        
        $model = new MSSSearch;
        $table = $model->getTable();
        $primaryKey = 'win_id';

        $columns = array(
            array( 'db' => 'win_id', 'dt' => 0 ),
            array( 'db' => 'full_name',  'dt' => 1 ),
            array( 'db' => 'job_title',  'dt' => 2 ),
            array( 'db' => 'current_sup_name',   'dt' => 3 ),
            array( 'db' => 'sbu',     'dt' => 4 ),
            array( 'db' => 'cost_center',     'dt' => 5 ),
            array( 'db' => 'lob',     'dt' => 6 )
        );

        $sql_details = array(
            'user' => 'svc.webtools',
            'pass' => 'str0ng3rb3113rf@5t3r',
            'db'   => 'cc_reporting',
            'host' => '10.235.230.63'
        );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }

}