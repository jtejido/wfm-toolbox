<?php
namespace Modules\MssSearch\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\MSS\WFMSearch;
use \App\Library\SSP;

class WFMSearchController extends Controller
{


    public function index()
    {

        
        return view('msssearch::wfm_search');


    }


    public function search()
    {
        
        $model = new WFMSearch;
        $table = $model->getTable();
        $primaryKey = 'win_id';

        $columns = array(
            array( 'db' => 'win_id', 'dt' => 0 ),
            array( 'db' => 'full_name',  'dt' => 1 ),
            array( 'db' => 'global_job_code',  'dt' => 2 ),
            array( 'db' => 'cost_center_id',   'dt' => 3 ),
            array( 'db' => 'city',     'dt' => 4 ),
            array( 'db' => 'state_cd',     'dt' => 5 ),
            array( 'db' => 'work_country',     'dt' => 6 ),
            array( 'db' => 'current_sup_name',     'dt' => 7 ),
            array( 'db' => 'job_code',     'dt' => 8 ),
            array( 'db' => 'job_title',     'dt' => 9 ),
            array( 'db' => 'job_level',     'dt' => 10 ),
            array( 'db' => 'lob',     'dt' => 11 ),
            array( 'db' => 'sbu',     'dt' => 12 ),
            array( 'db' => 'cost_center',     'dt' => 13 ),
            array( 'db' => 'status',     'dt' => 14 ),
            array( 'db' => 'hire_date',     'dt' => 15 ),
            array( 'db' => 'seniority_date',     'dt' => 16 ),
            array( 'db' => 'bus_group_desc',     'dt' => 17 )
        );

        $sql_details = array(
            'user' => 'svc.webtools',
            'pass' => 'str0ng3rb3113rf@5t3r',
            'db'   => 'cc_reporting',
            'host' => '10.235.230.63'
        );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }

}