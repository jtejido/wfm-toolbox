<?php

Route::group(['middleware' => ['web', 'auth', 'module:mss_search'], 'namespace' => 'Modules\MssSearch\Http\Controllers'], function(){

                Route::get('mss_search/executeSearch', ['uses' => 'MSSSearchController@search']);
                Route::get('mss_search', 'MSSSearchController@index');
                Route::get('wfm_search/executeSearch', ['uses' => 'WFMSearchController@search']);
                Route::get('wfm_search', 'WFMSearchController@index');

});
