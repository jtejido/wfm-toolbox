@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> New Process</div>

                <div class="panel-body">
                  <form method="POST" action="{{ URL::to('flow/process/create') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="Process Name" />
                    </div>
                    <div class="form-group">
                      <label for="step">Description</label>
                      <textarea id="step" name="description" class="form-control step" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="step">Folder</label>
                      <select id="folder_id" name="folder_id" class="form-control">
                        @foreach($folders as $folder)
                          <option value="{{ $folder->folder_id }}" @if($folder->folder_id == $folder_id) selected @endif>{{ $folder->name }}</option>
                        @endforeach
                      </select>
                    </div>
                    <button type="submit" class="btn btn-default">Create</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
