@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> {{ $folder->name }} <a class="pull-right btn btn-danger btn-xs" href="{{ URL::to('flow/folder/'.$folder->folder_id.'/delete') }}" role="button">Delete</a></div>

                <div class="panel-body">
                  <form method="POST" action="{{ URL::to('flow/folder/'.$folder->folder_id.'') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="name">Name</label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="Step Name" value="{{ $folder->name }}" />
                    </div>
                    <div class="form-group">
                      <label for="step">Description</label>
                      <textarea id="step" name="description" class="form-control step" rows="3">{{ $folder->description }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Update</button>
                    <a href="{{ URL::to('flow/folder/'.$folder->folder_id.'') }}" class="btn btn-default">Cancel</a>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
