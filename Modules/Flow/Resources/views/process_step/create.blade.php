@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> New Step</div>

                <div class="panel-body">
                  <form method="POST" action="{{ URL::to('flow/process/'.$process->process_id.'/step/create') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="name">Step</label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="Step Name" />
                    </div>
                    <div class="form-group">
                      <label for="step">Details <small>(optional)</small></label>
                      <textarea id="step" name="description" class="form-control step" rows="3"></textarea>
                    </div>
                    <input type="hidden" name="process_id" value="{{ $process->process_id }}" />
                    <button type="submit" class="btn btn-default">Create</button>
                    <a href="{{ URL::to('flow/process/'.$process->process_id.'') }}" class="btn btn-default">Cancel</a>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
      $('#step').summernote();
  });
</script>
@endsection
