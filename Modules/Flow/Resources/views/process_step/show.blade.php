@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> {{ $step->name }}
                  <div class="btn-group pull-right" role="group">
                    <a class="btn btn-warning btn-xs" href="{{ URL::to('flow/process/'.$step->process->process_id.'/step/'.$step->step_id.'/edit') }}" role="button">Edit Step</a>
                    <a class="btn btn-default btn-xs" href="{{ URL::to('flow/process/'.$step->process->process_id.'') }}" role="button">Go Back</a>
                  </div>
                </div>

                <div class="panel-body">
                  <p><strong>Edited by:</strong> {{ $step->user->full_name }}</p>
                  <p><strong>Last Updated:</strong> {{ $step->updated_at }}</p>
                  <hr/>
                    @if(!empty($step->step))
                      <div class="form-group">
                        {!! $string !!}
                      </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
