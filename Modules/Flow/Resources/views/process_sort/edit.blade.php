@extends('flow::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> {{ $process->name }}</div>

                <div class="panel-body">
                  <p>Using the boxes below, change the sort order of the steps in this process.</p>
                  <form method="POST" class="form-inline" action="{{ URL::to('flow/process/'.$process->process_id.'/sort') }}">
                    {{ csrf_field() }}

                    <div class="list-group">
                      @foreach($process->steps as $step)
                        <div class="list-group-item">
                          <h4 class="list-group-item-heading">
                            <input name="{{ $step->step_id }}" class="form-control" value="{{ $loop->iteration }}" style="width: 50px" />
                            {{ $step->name }}
                          </h4>
                        </div>
                      @endforeach
                    </div>

                    <button type="submit" class="btn btn-default">Update</button>
                    <a href="{{ URL::to('flow/process/'.$process->process_id.'') }}" class="btn btn-default">Cancel</a>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection