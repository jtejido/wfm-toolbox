<?php

namespace Modules\Flow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Flow\Folder;

class HomeController extends Controller
{

        public function index()
    {
        $folders = Folder::orderBy('name')->with(['processes'])->get();

        return view('flow::index', compact('folders'));
    }
}
