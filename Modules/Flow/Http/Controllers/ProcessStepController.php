<?php

namespace Modules\Flow\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\Flow\Process, App\Models\Flow\ProcessStep;
use Auth;
use Redirect;
use App\Library\QueryTrack;
use DB;
use Session;

class ProcessStepController extends Controller
{

    public function __construct(QueryTrack $track) {

        $this->track = $track;

    }

    public function show(Request $request, $process_id, $step_id)
    {

        $step = ProcessStep::with(['process', 'user'])->find($step_id);

        $bytea = stream_get_contents($step->step);
        $string = pg_unescape_bytea($bytea);


        return view('flow::process_step.show', compact('step', 'string'));
    }

    public function edit(Request $request, $process_id, $step_id)
    {

        $step = ProcessStep::with('process')->find($step_id);

        $bytea = stream_get_contents($step->step);
        $string = pg_unescape_bytea($bytea);
        $html_step = htmlspecialchars($string);

        return view('flow::process_step.edit', compact('step', 'html_step'));
    }


    public function save(Request $request, $process_id, $step_id)
    {
        DB::connection()->enableQueryLog();

        $step_sentence = !empty($request->description) ? $request->description: '';

        $step = ProcessStep::FindOrFail($step_id);

        $step->name = $request->name;

        $step->step = pg_escape_bytea($step_sentence);

        $step->winid = Auth::id();

        $step->save();

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

        return Redirect::to('flow/process/'.$process_id.'/step/'.$step_id);


    }


    public function newItem($process_id)
    {
        $process = Process::FindOrFail($process_id);

        return view('flow::process_step.create', compact('process'));
    }


    public function create(Request $request, $process_id)
    {

        DB::connection()->enableQueryLog();

        $step_sentence = !empty($request->description) ? $request->description: '';

        $step = new ProcessStep;

        $step->name = $request->name;

        $step->step = pg_escape_bytea($step_sentence);

        $step->process_id = $process_id;

        $step->priority = 0;

        $step->winid = Auth::id();

        $step->save();

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

        return Redirect::to('flow/process/'.$step->process_id);

    }


      public function delete(Request $request, $process_id, $step_id)
      {

          DB::connection()->enableQueryLog();

          $step = ProcessStep::FindOrFail($step_id);

          $step->winid = Auth::id();

          $step->save();

          $step->delete();

          $queries = DB::getQueryLog();

          $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

          return Redirect::to('flow/process/'.$process_id);


      }

}
