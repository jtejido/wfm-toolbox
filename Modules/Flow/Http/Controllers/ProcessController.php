<?php

namespace Modules\Flow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Flow\Process, App\Models\Flow\Folder;
use Auth;
use PDF;
use Redirect;
use App\Library\QueryTrack;
use DB;
use Session;

class ProcessController extends Controller
{

    public function __construct(QueryTrack $track) {

        $this->track = $track;

    }

    public function show(Request $request, $process_id)
    {

        $process = Process::with('steps')->find($process_id);

        return view('flow::process.show', compact('process'));
    }


    public function export(Request $request, $process_id)
    {

        $process = Process::with('steps')->find($process_id);
        $step_temp = array();
        foreach ($process->steps as $step){
            $bytea = stream_get_contents($step->step);
            $string = pg_unescape_bytea($bytea);
            array_push($step_temp, array('name' => $step->name, 'step' => $string));
        }

        $pdf = PDF::loadView('flow::process.export', compact('process', 'step_temp'));

        $name = preg_replace("/(\W)+/", "", $process->name);

        return $pdf->download($name.'.pdf');

    }


    public function newItem($folder_id = 0)
    {

        $folders = Folder::all();

        return view('flow::process.create', compact('folders', 'folder_id'));
    }


    public function edit(Request $request, $process_id)
    {

        $process = Process::with('steps')->find($process_id);

        $folders = Folder::all();

        return view('flow::process.edit', compact('process', 'folders'));
    }


    public function create(Request $request)
    {

        DB::connection()->enableQueryLog();

        $process = new Process;

        $process->name = $request->name;

        $process->description = !empty($request->description) ? $request->description: '';

        $process->folder_id = $request->folder_id;

        $process->winid = Auth::id();

        $process->save();

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

        return Redirect::to('flow/process/'.$process->process_id);


    }


    public function save(Request $request, $process_id)
    {
        
        DB::connection()->enableQueryLog();

        $process = Process::FindOrFail($process_id);

        $process->name = $request->name;

        $process->description = !empty($request->description) ? $request->description: '';

        $process->folder_id = $request->folder_id;

        $process->winid = Auth::id();

        $process->save();

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

        return Redirect::to('flow/process/'.$process->process_id);

    }


    public function delete(Request $request, $process_id)
    {

        DB::connection()->enableQueryLog();

        $process = Process::FindOrFail($process_id);

        $process->winid = Auth::id();

        $process->save();

        $process->delete();

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

        return Redirect::to('flow');


    }
}
