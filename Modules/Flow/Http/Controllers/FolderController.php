<?php

namespace Modules\Flow\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Flow\Folder;
use Auth;
use Redirect;
use App\Library\QueryTrack;
use DB;
use Session;

class FolderController extends Controller
{

    public function __construct(QueryTrack $track) {

        $this->track = $track;

    }


    public function show(Request $request, $folder_id)
    {

        $folder = Folder::with(['processes', 'user'])->find($folder_id);

        return view('flow::folder.show', compact('folder'));
    }


    public function newItem()
    {
        return view('flow::folder.create');
    }


    public function edit(Request $request, $folder_id)
    {

        $folder = Folder::with(['processes'])->find($folder_id);

        return view('flow::folder.edit', compact('folder'));
    }


    public function create(Request $request)
    {
        DB::connection()->enableQueryLog();

        $folder = new Folder;

        $folder->name = $request->name;

        $folder->description = !empty($request->description) ? $request->description: '';

        $folder->winid = Auth::id();

        $folder->save();

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

        return Redirect::to('flow/folder/'.$folder->folder_id);

    }


    public function save(Request $request, $folder_id)
    {
        DB::connection()->enableQueryLog();

        $folder = Folder::FindOrFail($folder_id);

        $folder->name = $request->name;

        $folder->description = !empty($request->description) ? $request->description: '';

        $folder->winid = Auth::id();

        $folder->save();

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

         return Redirect::to('flow/folder/'.$folder->folder_id);

    }


    public function delete(Request $request, $folder_id)
    {
        DB::connection()->enableQueryLog();

        $folder = Folder::FindOrFail($folder_id);

        $folder->winid = Auth::id();

        $folder->save();

        $folder->delete();

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

       return Redirect::to('flow');

    }
}
