<?php

namespace Modules\Flow\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Models\Flow\Process, App\Models\Flow\ProcessStep;
use Redirect;
use App\Library\QueryTrack;
use DB;
use Session;

class ProcessSortController extends Controller
{

    public function __construct(QueryTrack $track) {

        $this->track = $track;

    }

    public function edit(Request $request, $process_id)
    {

        $process = Process::with('steps')->find($process_id);

        return view('flow::process_sort.edit', compact('process'));
    }



    public function save(Request $request, $process_id)
    {

        $sort = $request->except('_token');

        asort($sort);

        $counter = 1;

        DB::connection()->enableQueryLog();

        foreach(array_keys($sort) as $step_id) {

          $step = ProcessStep::FindOrFail($step_id);

          $step->priority = $counter;

          $step->save();

          $counter++;

        }

        $queries = DB::getQueryLog();

        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'flow');

        return Redirect::to('flow/process/'.$process_id);

    }

}
