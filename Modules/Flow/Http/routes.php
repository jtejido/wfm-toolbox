<?php

Route::group(['middleware' => ['web', 'auth', 'module:flow'], 'namespace' => 'Modules\Flow\Http\Controllers'], function()
{
    




    Route::get('flow', 'HomeController@index');

Route::get('flow/folder/create', 'FolderController@newItem');
Route::group(['middleware' => ['permission']], function()
	{
		Route::post('flow/folder/create', 'FolderController@create');
	});
Route::get('flow/folder/{folder_id}', 'FolderController@show');
Route::group(['middleware' => ['permission']], function()
	{
		Route::post('flow/folder/{folder_id}', 'FolderController@save');
	});
Route::get('flow/folder/{folder_id}/edit', 'FolderController@edit');
Route::get('flow/folder/{folder_id}/delete', 'FolderController@delete');

Route::get('flow/process/create/{folder_id}', 'ProcessController@newItem');
Route::group(['middleware' => ['permission']], function()
	{
		Route::post('flow/process/create', 'ProcessController@create');
	});
Route::get('flow/process/{process_id}', 'ProcessController@show');
Route::group(['middleware' => ['permission']], function()
	{
		Route::post('flow/process/{process_id}', 'ProcessController@save');
	});
Route::get('flow/process/{process_id}/edit', 'ProcessController@edit');
Route::get('flow/process/{process_id}/delete', 'ProcessController@delete');
Route::get('flow/process/{process_id}/export', 'ProcessController@export');

Route::get('flow/process/{process_id}/sort', 'ProcessSortController@edit');
Route::group(['middleware' => ['permission']], function()
	{
		Route::post('flow/process/{process_id}/sort', 'ProcessSortController@save');
	});
Route::get('flow/process/{process_id}/step/create', 'ProcessStepController@newItem');
Route::group(['middleware' => ['permission']], function()
	{
		Route::post('flow/process/{process_id}/step/create', 'ProcessStepController@create');
	});
Route::get('flow/process/{process_id}/step/{step_id}', 'ProcessStepController@show');
Route::group(['middleware' => ['permission']], function()
	{
		Route::post('flow/process/{process_id}/step/{step_id}', 'ProcessStepController@save');
	});
Route::get('flow/process/{process_id}/step/{step_id}/edit', 'ProcessStepController@edit');
Route::get('flow/process/{process_id}/step/{step_id}/delete', 'ProcessStepController@delete');
});
