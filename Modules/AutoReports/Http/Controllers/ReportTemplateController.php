<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use \App\Models\Reports\ReportTemplate;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;


class ReportTemplateController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function update($rid)
    {

            $column_name = Input::get('name');
            $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = ReportTemplate::select()
                ->where('rid', '=', $rid)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'edit_report_template');
            return response()->json([ 'code'=>200], 200);
        
    }



}