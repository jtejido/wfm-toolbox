<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use \App\Models\Reports\ReportRecipients;
use Carbon\Carbon;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;


class ReportRecipientController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }


    public function store($rid)
    {
                            $int = 1;
                            $limit = 10; 
                            DB::connection()->enableQueryLog();
                               while($int <= $limit) {
                                    $recipient_counter = 'ID'.$int.'_recipient_id';
                                    $rid_counter = 'ID'.$int.'_rid';
                                    $email_counter = 'ID'.$int.'_email_type';
                                    $tcb_counter = 'ID'.$int.'_to_cc_bcc';
                                    $sd_counter = 'ID'.$int.'_start_dte';
                                    $ed_counter = 'ID'.$int.'_end_dte';
                                    if(!empty(Input::get($recipient_counter))){
                                        
                                            $report_recipient = new ReportRecipients;
                                            $report_recipient->recipient_id       = Input::get($recipient_counter);
                                            $report_recipient->rid       = Input::get($rid_counter);
                                            $report_recipient->email_type = Input::get($email_counter);
                                            $report_recipient->to_cc_bcc = Input::get($tcb_counter);
                                            $report_recipient->start_dte = !empty(Input::get($sd_counter)) ? Input::get($sd_counter) : null;
                                            $report_recipient->end_dte = !empty(Input::get($ed_counter)) ? Input::get($ed_counter) : null;
                                            $report_recipient->save();
                                        ;
                                        }
                                    $int++;
                            }
                            $queries = DB::getQueryLog();
                            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'report_recipients');

                            return Redirect::to('reports/' . $rid . '/edit')->withErrors('Recipients Added!.');
    }

    public function update(Request $request, $rid, $recipient_id, $email_type)
    {

            

            if(Input::get('name') == 'end_dte'){
               $column_value = !empty(Input::get('value')) ? Input::get('value'): null;
            }
            else if(Input::get('name') == 'start_dte'){
               $column_value = !empty(Input::get('value')) ? Input::get('value'): null;
            }
            else {
                   $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
            }
            $column_name = Input::get('name');
        
            DB::connection()->enableQueryLog();
            $test = ReportRecipients::select()
                ->where('rid', '=', $rid)
                ->where('recipient_id', '=', $recipient_id)
                ->where('email_type', '=', $email_type)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'report_recipients');
            return response()->json([ 'code'=>200], 200);
        
    }



}