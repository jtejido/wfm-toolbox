<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Reports\ReportRequest;
use App\Models\Reports\ActionRequest;
use App\Models\Reports\ApprovalQueue;
use App\Models\Reports\ReportRecipients;
use App\ApproverMap;
use DB;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use \App\Library\QueryTrack;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\DataTables\Reports\ApprovalQueueDataTable;

class ApprovalQueueController extends Controller
{

    protected $htmlBuilder; 

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    public function index(Request $request, ApprovalQueueDataTable $dataTable)
    {


        if ($request->ajax()) {
            return $dataTable->ajax();
        }

        $html = $dataTable->html();

        return view('autoreports::approval_queue')->with('html', $html);
    }



    public function approvedelete($request_id,$rid,$recipient_id,$request_type)
    {

                $query = "nextval('app_reports_mgmt.action_request_action_id_seq') as nxt";
                $next_action_id = ActionRequest::selectRaw($query)->value('nxt');
                DB::connection()->enableQueryLog();
                $action = new ActionRequest;
                $action->action_id       = $next_action_id;
                $action->request_id = $request_id; 
                $action->action_type = 'approved';
                $action->action_datetime = Carbon::now();
                $action->approver_id = Session::get('toolbox_s3_id');
                $action->save();

                $recipient = ReportRecipients::select()
                ->where('recipient_id', '=', $recipient_id)
                ->where('rid', '=', $rid)
                ->update(['end_dte' => Carbon::today()]);

                $approved_req = ReportRequest::select()
                ->where('request_id', '=', $request_id)
                ->update(['request_type' => 'approve', 'approver_id' => Session::get('toolbox_s3_id')]);
                $queries = DB::getQueryLog();
                QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'approval_queue');

        Session::flash('message', 'The Request has been Approved.');
        return Redirect::to('approval_queue');

        }


    public function approveadd()
    {

        $rid = Input::get('rid');
        $recipient_id = Input::get('recipient_id');
        $request_id = Input::get('request_id');
        $request_type = Input::get('request_type');
        $to_cc_bcc = Input::get('to_cc_bcc');
        $email_type = Input::get('email_type');

                $query = "nextval('app_reports_mgmt.action_request_action_id_seq') as nxt";
                $next_action_id = ActionRequest::selectRaw($query)->value('nxt');
                DB::connection()->enableQueryLog();
                $action = new ActionRequest;
                $action->action_id       = $next_action_id;
                $action->request_id = $request_id; 
                $action->action_type = 'approved';
                $action->action_datetime = Carbon::now();
                $action->approver_id = Session::get('toolbox_s3_id');
                $action->save();
                $queries = DB::getQueryLog();
                QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'approval_queue');

        $check = ReportRecipients::where('rid' ,'=', $rid)->where('recipient_id' ,'=', $recipient_id)
                        ->first();

                if(empty($check->rid)){
                    DB::connection()->enableQueryLog();
                    $report_recipient = new ReportRecipients;
                    $report_recipient->recipient_id = $recipient_id;
                    $report_recipient->rid = $rid; 
                    $report_recipient->to_cc_bcc = $to_cc_bcc;
                    $report_recipient->email_type = $email_type;
                    $report_recipient->start_dte = Carbon::tomorrow();
                    $report_recipient->save();

                    $approved_req = ReportRequest::select()
                    ->where('request_id', '=', $request_id)
                    ->update(['request_type' => 'approve', 'approver_id' => Session::get('toolbox_s3_id')]);
                    $queries = DB::getQueryLog();
                    QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'approval_queue');

                        Session::flash('message', 'The Request has been Approved.');
                         return Redirect::to('approval_queue');
                }


                else {
                    DB::connection()->enableQueryLog();
                    $recipient = ReportRecipients::select()
                    ->where('recipient_id', '=', $recipient_id)
                    ->where('rid', '=', $rid)
                    ->update(['end_dte' => NULL, 'to_cc_bcc' => $to_cc_bcc, 'email_type' => $email_type]);

                    $approved_req = ReportRequest::select()
                    ->where('request_id', '=', $request_id)
                    ->update(['request_type' => 'approve', 'approver_id' => Session::get('toolbox_s3_id')]);;
                        
                        $queries = DB::getQueryLog();
                        QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'approval_queue');
                        Session::flash('message', 'The Request has been Approved.');
                         return Redirect::to('approval_queue');
                }

        }

    public function reject()
    {

        $request_id = Input::get('request_id');
        $comment = Input::get('comment');


                $query = "nextval('app_reports_mgmt.action_request_action_id_seq') as nxt";
                $next_action_id = ActionRequest::selectRaw($query)->value('nxt');
                DB::connection()->enableQueryLog();
                $action = new ActionRequest;
                $action->action_id       = $next_action_id;
                $action->request_id = $request_id; 
                $action->action_type = 'rejected';
                $action->action_datetime = Carbon::now();
                $action->approver_id = Session::get('toolbox_s3_id');
                $action->comment = $comment;
                $action->save();

                $reject_rep_req = ReportRequest::select()
                ->where('request_id', '=', $request_id)
                ->update(['request_type' => 'rejected', 'approver_id' => Session::get('toolbox_s3_id')]);
                $queries = DB::getQueryLog();
                QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'approval_queue');
            Session::flash('message', 'The Request has been rejected.');
            return Redirect::to('approval_queue');

    }


}
