<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\Reports\TemplateConfig;
use \App\Models\Reports\TemplateConfigDash;
use View;
use Session;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\DataTables\Reports\TemplateConfigDataTable;


class TemplateConfigController extends Controller
{

    protected $htmlBuilder; 

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    public function index(Request $request, TemplateConfigDataTable $dataTable)
    {

        if ($request->ajax()) {
            return $dataTable->ajax();
        }

        $html = $dataTable->html();

        return view('autoreports::template_config')->with('html', $html);

    }


    public function store ()
    {

        $rules = array(
            'template_name'       => 'required',
            'connection_id'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('template_config')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            DB::connection()->enableQueryLog();
            $template_config = new TemplateConfig;
            $template_config->template_name       = Input::get('template_name');
            $template_config->connection_id       = Input::get('connection_id');
            $template_config->db_object = Input::get('db_object'); 
            $template_config->use_where = Input::get('use_where'); 
            $template_config->save();
            $queries = DB::getQueryLog();
            QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'template_config');

            Session::flash('message', 'Record was Successfully Saved!');
            return Redirect::to('template_config');
        }
    }


    public function update(Request $request, $template_name, $connection_id)
    {

        $column_name = Input::get('name');
       $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = TemplateConfig::select()
                ->where('template_name', '=', $template_name)
                ->where('connection_id', '=', $connection_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'template_config');
            return response()->json([ 'code'=>200], 200);

    
    }


    public function destroy($template_name, $connection_id)
    {
        DB::connection()->enableQueryLog();
        $object = TemplateConfig::where('template_name', $template_name)->where('connection_id', $connection_id)->delete();
        $queries = DB::getQueryLog();
        QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'template_config');
        Session::flash('message', 'Record was Successfully Deleted!');
        return Redirect::to('template_config');

    }

}