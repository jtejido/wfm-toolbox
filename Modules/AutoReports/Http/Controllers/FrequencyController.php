<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Models\Reports\Frequency;
use Carbon\Carbon;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;

class FrequencyController extends Controller
{

        public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function store($rid)
    {

                        DB::connection()->enableQueryLog();
                        if(!empty(Input::get('newday'))){
                                        foreach (Input::get('newday') AS $key => $value){
                                            $new_exp = !empty(Input::get('newexp')) ? Input::get('newexp') : null;
                                            $frequency = new Frequency;
                                            $frequency->rid = Input::get('newrid');
                                            $frequency->day = $value;
                                            $frequency->scheduled_time = Input::get('newsched');
                                            $frequency->sla_time = Input::get('newsla');
                                            $frequency->time_zone = Input::get('newtz');
                                            $frequency->expected_complete_time = Input::get('newect');
                                            $frequency->expire_time = $new_exp;
                                            $frequency->save();
                                        }
                                    }

                            $queries = DB::getQueryLog();
                            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'edit_frequency');
                            return Redirect::to('reports/' . $rid . '/edit')->withErrors('Frequency Added!.');
    }

 
        public function edit($rid,$day,$scheduled_time)
    {

        $frequency = Frequency::where([
                        ['rid', $rid],
                        ['day', $day],
                        ['scheduled_time', $scheduled_time]
                    ])->get();

        return view('autoreports::frequency.edit')
            ->with('frequency', $frequency);
    }

    public function update($rid,$day,$scheduled_time)
    {

        $expire_time = !empty(Input::get('expire_time')) ? Input::get('expire_time') : null;

            DB::connection()->enableQueryLog();
            $test = Frequency::select()
                ->where('rid', '=', $rid)
                ->where('day', '=', $day)
                ->where('scheduled_time', '=', $scheduled_time)
                ->update(['scheduled_time' => Input::get('scheduled_time'), 'expected_complete_time' => Input::get('expected_complete_time'), 'sla_time' => Input::get('sla_time'), 'time_zone' => Input::get('time_zone'), 'expected_complete_time' => Input::get('expected_complete_time'), 'expire_time' => $expire_time]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'edit_frequency');


            // redirect
            return Redirect::to('reports/'. $rid . '/edit')->withErrors('Frequency was Successfully Updated!');
        
    }

    public function destroy($rid,$day,$scheduled_time)
    {

        DB::connection()->enableQueryLog();
        $frequency = Frequency::where('rid', $rid)->where('day', $day)->where('scheduled_time', $scheduled_time)->delete();
        $queries = DB::getQueryLog();
        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'edit_frequency');

        // redirect
        return Redirect::to('reports/'. $rid . '/edit')->withErrors('Frequency was Successfully Deleted!');
    }

}