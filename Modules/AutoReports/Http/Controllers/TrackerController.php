<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\DataTables\Reports\TrackerControllerDataTable;

class TrackerController extends Controller
{
    protected $htmlBuilder; 

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    public function index(Request $request, TrackerControllerDataTable $dataTable)
    {

        if ($request->ajax()) {
            return $dataTable->ajax();
        }

        $html = $dataTable->html();

        return view('autoreports::current_day_tracker')->with('html', $html);
    }
    
}
