<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use \App\Library\QueryTrack;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\Reports\Reports;
use \App\Models\Reports\ReportsDash;
use DB;
use Response;
use \App\Library\CreateReport;
use \App\Models\Reports\ReportRecipients;
use \App\Models\Reports\Frequency;
use \App\Models\Reports\TemplateConfig;
use \App\Models\Reports\Recipients;
use \App\Models\Reports\RunReportLog;
use \App\Models\Reports\ReportRecipientDash;
use Carbon\Carbon;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\Reports\ReportTemplate;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\DataTables\Reports\ReportsDataTable;


class ReportsController extends Controller
{
    
    protected $htmlBuilder; 

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }


    public function index(Request $request, ReportsDataTable $dataTable)
    {
        
        
        $model = new Reports;
        $query = "nextval('app_reports_mgmt.reports_rid_seq') as nxt";
        $next_rid = $model::selectRaw($query)->value('nxt');
        Session::put('next_rid', $next_rid);

        if ($request->ajax()) {
            return $dataTable->ajax();
        }

        $html = $dataTable->html();

        return view('autoreports::reports')
        ->with(compact('html', 'clientdd', 'internal_externaldd', 'isdateappendeddd', 'first_day_of_weekdd', 'savewithmacrodd', 'deleteconnectionsdd', 'run_regiondd', 'report_statusdd', 'time_zones', 'template_dd'));

    }

    public function searchRecipient(Request $request)
    {

          $term = Input::get('searchText');
  
          $results = array();
          
          $queries = Recipients::where('name', 'iLIKE', '%'.$term.'%')
            ->take(10)->get();
          
          foreach ($queries as $query)
          {
              $results[] = [ 'id' => $query->recipient_id, 'name' => $query->name.' - '. $query->company];
          }
        return response()->json($results);

    }

    public function recipientstore()
    {

        $rules = array(
            'temp_name'       => 'required',
            'email_address'      => 'email|required|unique:pgsql.app_reports_mgmt.recipients,email_address',
            'internal_external'      => 'required',
            'company'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
               return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 400);
        } else {
            DB::connection()->enableQueryLog();
            $recipient = new Recipients;
            $recipient->name       = Input::get('temp_name');
            $recipient->email_address = Input::get('email_address'); 
            $recipient->internal_external = Input::get('internal_external');
            $recipient->company = Input::get('company');
            $recipient->s3_id = Input::get('s3_id');
            $recipient->save();
            $queries = DB::getQueryLog();
            QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'reports');
        }
    }


    public function store()
    {

        if(Input::has('client')){
        $client_temp = Input::get('client');
        }
        else{
        $client_temp = Input::get('client_not_listed');
        }

        if($client_temp != "Template Report"){
        $rules = array(
            'name'       => 'required',
        );
        }
        
        elseif($client_temp == "Template Report"){
        $rules = array(
            'updatetemplate'       => 'required',
        );
        }
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('reports')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {


        if($client_temp != "Template Report"){
            $report = Reports::where('name', 'ilike', Input::get('name'))->where('client', '!=', 'Template Report')->first();
            if(empty($report)){

                        CreateReport::createreport();
                        
                        // redirect
                        Session::flash('message', 'Report was Successfully Saved!');
                        return Redirect::to('reports');
                }
                elseif(!empty($report)){
                Session::flash('message', 'The Report name is already existing, please use a different name.');
                return Redirect::to('reports');
                }
            }
            elseif($client_temp == "Template Report"){
                        CreateReport::createreport();
                        // redirect
                        Session::flash('message', 'Report was Successfully Saved!');
                        return Redirect::to('reports');
                    }
        }
    }

 
    public function edit($rid)
    {
        // get the report
        $report = Reports::where('rid', $rid)->get();

        $is_template = Reports::select('client')->where('rid', $rid)->first();
        $frequency = Frequency::where('rid', $rid)->get();
        $report_templates = ReportTemplate::where('rid', $rid)->get();
        $report_recipient = ReportRecipientDash::where('rid', $rid)->get();

        $report_rid = $rid;

        // show the edit form and pass the report
        return view('autoreports::report.edit')
        ->with(compact('report', 'frequency', 'report_recipient', 'report_templates', 'is_template', 'clientdd', 'internal_externaldd', 'isdateappendeddd', 'first_day_of_weekdd', 'savewithmacrodd', 'deleteconnectionsdd', 'run_regiondd', 'report_statusdd', 'time_zones', 'template_dd', 'report_rid'));

    }


    public function update($rid)
    {

        if(Input::has('client')){
        $client_temp = Input::get('client');
        }
        else{
        $client_temp = Input::get('client_not_listed');
        }

        if($client_temp != "Template Report"){
        $rules = array(
            'name'       => 'required',
        );
        }
        
        elseif($client_temp == "Template Report"){
        $rules = array(
            'updatetemplate'       => 'required',
        );
        }
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('reports/' . $rid . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
                        
                        $report = Reports::find($rid);
                        DB::connection()->enableQueryLog();

                        if($client_temp != "Template Report"){
                            $report->name       = !empty(Input::get('name')) ? Input::get('name'): '';
                        }
                        elseif($client_temp == "Template Report"){
                            $report->name       = !empty(Input::get('updatetemplate')) ? Input::get('updatetemplate'): '';
                        }

                        if (Input::has('client')){
                        $report->client = Input::get('client');
                        }
                        else{
                        $report->client = Input::get('client_not_listed');
                        }

                        $report->encryption = !empty(Input::get('encryption')) ? Input::get('encryption'): '';
                        $report->internal_external = !empty(Input::get('internal_external')) ? Input::get('internal_external'): '';
                        $report->isdateappended = !empty(Input::get('isdateappended')) ? Input::get('isdateappended') : '';
                        $report->savewithmacro = !empty(Input::get('savewithmacro')) ? Input::get('savewithmacro') : '';
                        $report->deleteconnections = !empty(Input::get('deleteconnections')) ? Input::get('deleteconnections') : '';
                        $report->nameofemailbodysheet = !empty(Input::get('nameofemailbodysheet')) ? Input::get('nameofemailbodysheet') : '';
                        $report->validationchecks = !empty(Input::get('validationtables')) ? Input::get('validationtables') : '';
                        $report->validationtables = '';
                        $report->customemailtext = !empty(Input::get('customemailtext')) ? Input::get('customemailtext') : '';
                        $report->report_status = !empty(Input::get('report_status')) ? Input::get('report_status') : '';
                        $report->custom_email_subject = !empty(Input::get('custom_email_subject')) ? Input::get('custom_email_subject') : '';
                        $report->run_region = !empty(Input::get('run_region')) ? Input::get('run_region') : '';
                        $report->custom_attachment_name = !empty(Input::get('custom_attachment_name')) ? Input::get('custom_attachment_name') : '';
                        $report->zip_file_format = !empty(Input::get('zip_file_format')) ? Input::get('zip_file_format') : '';
                        $report->first_day_of_week  = !empty(Input::get('first_day_of_week')) ? Input::get('first_day_of_week') : '';
                        $report->save();

 
                    if(($client_temp == "Template Report") && (!empty(Input::get('where_condition')))){
                        

                            $report_template = new ReportTemplate;
                            $report_template->rid = Input::get('rid');
                            $report_template->name = Input::get('final_name');
                            $report_template->where_condition = Input::get('where_condition');
                            $report_template->customer_id = null;
                            $report_template->template = Input::get('updatetemplate');
                            $report_template->save();
                        }

                    if(($client_temp == "Template Report") && (empty(Input::get('where_condition')))){
                        
                            $report_template = ReportTemplate::where('rid', $rid);

                            if (!empty($report_template)){
                            $report_template = ReportTemplate::select()
                            ->where('rid', '=', $rid)
                            ->update(['template' => Input::get('updatetemplate')]);
                            }
                        }
                        
                        $queries = DB::getQueryLog();
                        QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'reports');
                       Session::flash('message', 'Report was Successfully Updated!');
                        return Redirect::to('reports/' . $rid . '/edit');




        }
    }

    public function run($rid)
    {
        // get the report
            DB::connection()->enableQueryLog();
            $RunReportLog = new RunReportLog;
            $RunReportLog->rid       = $rid;
            $RunReportLog->time_stamp       = \Carbon\Carbon::now();
            $RunReportLog->save();
            $queries = DB::getQueryLog();
            QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'reports');
            // redirect
            Session::flash('message', 'Report '.$rid.' will run on the next 15 minute job');
            return Redirect::to('reports');
    }

    public function copyreport(Request $copy)
    {
        // get which to copy
           $report = $copy->copy1;
           $recipient = $copy->copy2;
           $frequency = $copy->copy3;
           $rid_original = $copy->rid;
           $rid_new = $copy->rid_new;

           if (empty($recipient)){
            $recipient = null;
           }
           if (empty($frequency)){
            $frequency = null;
           }

           

           if(!empty($report)){
            $original_report_details = DB::select(DB::raw("select * FROM app_reports_mgmt.reports WHERE rid = :rid"),  array('rid' => $rid_original));

        // show the edit form and pass the report
           return view('autoreports::report.add')
            ->with('original_report_details', $original_report_details)
            ->with('rid_new', $rid_new)
            ->with('recipient', $recipient)
            ->with('frequency', $frequency);
           }

           else{
           Session::flash('message', 'You have to include at least the Report Details when copying a Report.');
            return Redirect::to('reports');
        }
           
    }

}