<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\Reports\Recipients;
use \App\Models\Reports\AssignedReports;
use View;
use DB;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\DataTables\Reports\RecipientsDataTable;
use App\DataTables\Reports\AssignedReportsDataTable;

class RecipientsController extends Controller
{

    protected $htmlBuilder; 

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }


    public function index(Request $request, RecipientsDataTable $dataTable)
    {
        $int_ext = Recipients::distinct()->select('internal_external')->get();
        if ($request->ajax()) {
            return $dataTable->ajax();
        }
         $html = $dataTable->html();

        return view('autoreports::recipients')->with(compact('html', 'int_ext'));
    }


    public function store()
    {

        $rules = array(
            'name'       => 'required',
            'email_address'      => 'email|required|unique:pgsql.app_reports_mgmt.recipients,email_address',
            'internal_external'      => 'required',
            'company'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('recipients')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            DB::connection()->enableQueryLog();
            $recipient = new Recipients;
            $recipient->name       = Input::get('name');
            $recipient->email_address = Input::get('email_address'); 
            $recipient->internal_external = Input::get('internal_external');
            $recipient->company = Input::get('company');
            $recipient->s3_id = Input::get('s3_id');
            $recipient->save();
            $queries = DB::getQueryLog();
            QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'recipients');

            Session::flash('message', 'Recipient was Successfully Saved!');
            return Redirect::to('recipients');
        }
    }


    public function edit(Request $request, AssignedReportsDataTable $dataTable)
    {
        $request->session()->forget('recipient_id');
        $recipient = Recipients::find($request->recipient_id);
        $request->session()->put('recipient_id', $request->recipient_id);
        if ($request->ajax()) {
            return $dataTable->ajax();
        }
         $html = $dataTable->html();

        return view('autoreports::recipient.reports')
            ->with('recipient', $recipient)->with('html', $html);
    }


    public function update(Request $request, $recipient_id)
    {
        

            $column_name = Input::get('name');
            $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = Recipients::select()
                ->where('recipient_id', '=', $recipient_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'recipients');
            return response()->json([ 'code'=>200], 200);

    }
}