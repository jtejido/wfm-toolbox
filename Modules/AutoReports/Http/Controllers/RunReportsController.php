<?php
namespace Modules\AutoReports\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\Reports\RRun;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\DataTables\Reports\RunReportsDataTable;

class RunReportsController extends Controller
{

    protected $htmlBuilder; 

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

        public function index(Request $request, RunReportsDataTable $dataTable)
    {

        if ($request->ajax()) {
            return $dataTable->ajax();
        }
         $html = $dataTable->html();

        return view('autoreports::reports_to_run')->with(compact('html'));
    }
}