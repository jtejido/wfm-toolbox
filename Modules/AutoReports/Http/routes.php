<?php

Route::group(['middleware' => ['web', 'auth', 'module:auto_reports'], 'namespace' => 'Modules\AutoReports\Http\Controllers'], function()
{
   			Route::group(['middleware' => ['permission']], function()
			{
				// All actions have to be checked against middleware
				Route::post('approval_queue/approve', 'ApprovalQueueController@approveadd');
				Route::get('approval_queue/{request_id}/{rid}/{recipient_id}/{request_type}/approve', 'ApprovalQueueController@approvedelete');
				Route::post('approval_queue/reject', 'ApprovalQueueController@reject');

			    Route::delete('template_config/{template_name}/{connection_id}/delete', 'TemplateConfigController@destroy');
			    Route::post('template_config/update/{template_name}/{connection_id}', ['as' => 'template_config/update', 'uses' => 'TemplateConfigController@update']);    

			    Route::post('recipients/update/{recipient_id}', ['as' => 'recipients/update', 'uses' => 'RecipientsController@update']);
			    Route::post('recipients', 'RecipientsController@store');      

			    Route::post('report_recipient/update/{rid}/{recipient_id}/{email_type}', ['as' => 'report_recipient/update', 'uses' => 'ReportRecipientController@update']); 
			    Route::post('report_template/update/{rid}', ['as' => 'report_template/update', 'uses' => 'ReportTemplateController@update']); 
			    Route::post('reports/{rid}/edit/frequency', 'FrequencyController@store');
			    Route::post('reports/{rid}/edit/report_recipient', 'ReportRecipientController@store');  
			    Route::get('reports/copy/{copy?}', 'ReportsController@copyreport');
			    Route::put('reports/{rid}/put', 'ReportsController@update');
			    Route::post('reports/add_recipient_record', 'ReportsController@recipientstore');
			    Route::post('reports', 'ReportsController@store');

			    Route::delete('frequency/{rid}/{day}/{scheduled_time}/delete', 'FrequencyController@destroy');
			    Route::put('frequency/{rid}/{day}/{scheduled_time}/put', 'FrequencyController@update');
			    Route::get('frequency/{rid}/{day}/{scheduled_time}/edit', 'FrequencyController@edit');
			    
			    Route::post('template_config', 'TemplateConfigController@store');
			});

   			// All views are good for everyone
			Route::get('approval_queue', 'ApprovalQueueController@index');
		    Route::get('template_config', 'TemplateConfigController@index');
			Route::get('recipients', 'RecipientsController@index');
		    Route::get('reports_to_run', 'RunReportsController@index');
		    Route::get('current_day_tracker', 'TrackerController@index');
		    Route::get('manual_waiting', 'WaitingController@index');
		    Route::get('reports/{rid}/edit', 'ReportsController@edit');
		    Route::get('recipients/{recipient_id}/edit', 'RecipientsController@edit');
		    Route::get('reports/{rid}/run', 'ReportsController@run');
		    Route::get('reports/searchrecipient', ['uses' => 'ReportsController@searchRecipient']);
			Route::get('reports', 'ReportsController@index');
	    	
});
