@extends('autoreports::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-list-ul" aria-hidden="true"></i> Template Configuration</h3>
    <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#create-template">Create Record</button>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
    <div class="widget-box">
        <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
          <h5>All Items</h5>
        </div>
        <div class="widget-content" >
            <div class="row-fluid">
                {!! $html->table(['class' => 'table-bordered table-condensed table-hover']) !!}
            </div>
        </div>
    </div>
</div>

<!-- Create Modal -->
<div id="create-template" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" action="{{ URL::to('template_config') }}">
                            {!! csrf_field() !!}
                                <div class="large-12 columns">
                                    <strong><label for="template_name">Template Name:</label></strong>
                                   <input class="form-control" name="template_name" type="text" id="template_name">
                                </div>
                                <div class="large-12 columns">
                                    <strong> <label for="connection_id">Connection ID:</label></strong>
                                    <input class="form-control" name="connection_id" type="text" id="connection_id">
                                </div>
                                <div class="large-12 columns">
                                    <strong> <label for="db_object">DB Object:</label></strong>
                                    <input class="form-control" name="db_object" type="text" id="db_object">
                                </div>
                                <div class="large-12 columns">
                                    <strong> <label for="use_where">Use Where?</label></strong>
                                    <select class="form-control" id="use_where" name="use_where"><option value="TRUE">True</option><option value="FALSE">False</option></select>
                                </div>
                                <div class="clearfix"></div>
                                </br></br>
                                <div class="col-sm-12 text-center">
                                <input class="btn btn-primary btn-lg" type="submit" value="Create Record">
                                </div>
                                <div class="clearfix"></div>
                                </br></br>

                        </form>
                    </div>
                 </div>
</div>
</div>
</div>
@endsection
@section('js')
<script>
$(document).ajaxComplete(function () {
        $.fn.editable.defaults.mode = 'inline';
        $(document).ready(function() {
            $('.db_object').editable({
                params: function(params) {
                    // add additional params from data-attributes of trigger element
                    params.name = $(this).editable().data('name');
                    return params;
                },
                error: function(response, newValue) {
                    if(response.status === 500) {
                        return 'Server error. Check entered data.';
                    } else {
                        return response.responseText;
                        // return "Error.";
                    }
                }
            });

            $('.use_where').editable({
                type: 'select',
                source: [
                    {value: 'TRUE', text: 'True'},
                    {value: 'FALSE', text: 'False'}
                ],
                params: function(params) {
                    // add additional params from data-attributes of trigger element
                    params.name = $(this).editable().data('name');
                    return params;
                },
                error: function(response, newValue) {
                    if(response.status === 500) {
                        return 'Server error. Check entered data.';
                    } else {
                        return response.responseText;
                        // return "Error.";
                    }
                }
             });
        });
});
</script>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('vendor/datatables/buttons.server-side.js') }}"></script>
{!! $html->scripts() !!}
@endsection