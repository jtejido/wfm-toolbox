@extends('autoreports::layouts.app')
@section('content')
 @foreach($original_report_details as $key => $value_rep)
<div class="hero-unit">
    <h3><i class="fa fa-list-ul" aria-hidden="true"></i> Reports Management</h3>
<p>Copy Report({!! $value_rep->rid !!}) - {!! $value_rep->name !!}</p>
</div>
@if (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif

<div class="container-fluid">
 <form method="POST" action="{{ URL::to('reports') }}">
 {!! csrf_field() !!}
                                            <div class="col-sm-6">
                                                <strong>{!! Form::label('rid', 'Report ID') !!}</strong>
                                                {!! Form::text('rid', $rid_new, array('class' => 'form-control', 'readonly' => 'readonly')) !!}

                                                <strong> {!! Form::label('client', 'Name of Client for Report:') !!}</strong>
                                                
                                                <select id="client" name="client" class="form-control">
                                                <option value="">Not Listed</option>
                                                @foreach(session('clientdd') as $key => $value)
                                                <option value="{!! $value !!}" @if(($value_rep->client) == $value) selected @endif>{!! $value !!}</option>
                                                @endforeach
                                                </select>
                                                </br>
                                                <input class="form-control" name="client_not_listed" type="text" style="display: none" placeholder="Please type the Client here">

                                                
                                                        
        @if ($value_rep->client == "Template Report") 

        {!! Form::label('name', 'Name of Report:') !!}

        <input class="form-control" id="name" name="name" type="text" value="" style="display: none">

        <select class="form-control" name="updatetemplate">
            <option value=""></option>
            @foreach(session('template_dd') as $key => $value)
            <option value="{!! $value->template_name !!}"@if((($value_rep->client) == "Template Report") and (($value_rep->name) == $value->template_name)) selected @endif>{!! $value->template_name !!}</option>
            @endforeach
        </select>

        <input class="form-control" name="where_condition" type="text" placeholder="Please type the Where Statement">
        <input class="form-control" name="final_name" type="text" placeholder="Please type the Template Final Name">

        @else
        {!! Form::label('name', 'Name of Report:') !!}

        {!! Form::text('name', $value_rep->name, array('class' => 'form-control', 'id' => 'name')) !!}

        <select class="form-control" name="updatetemplate" style="display: none">
            <option value=""></option>
            @foreach(session('template_dd') as $key => $value)
            <option value="{!! $value->template_name !!}"@if((($value_rep->client) == "Template Report") and (($value_rep->name) == $value->template_name)) selected @endif>{!! $value->template_name !!}</option>
            @endforeach
        </select>

        <input class="form-control" name="where_condition" type="text" style="display: none" placeholder="Please type the Where Statement">
        <input class="form-control" name="final_name" type="text" style="display: none" placeholder="Please type the Template Final Name">


        @endif

                                                <br />
                                                <strong> {!! Form::label('internal_external', 'Target Audience for Report:') !!}</strong>
                                                <select id="internal_external" name="internal_external" class="form-control">
                                                @foreach(session('internal_externaldd') as $key => $value)
                                                <option value="{!! $value['internal_external'] !!}" @if(($value_rep->internal_external) == $value['internal_external']) selected @endif>{!! $value['internal_external'] !!}</option>
                                                @endforeach
                                                </select>
    
                                                <strong> {!! Form::label('encryption', 'Password for Encryption:') !!}</strong>
                                                {!! Form::text('encryption', $value_rep->encryption, array('class' => 'form-control')) !!}

                                                <strong> {!! Form::label('isdateappended', 'Date Format for Attachment:') !!}</strong>
                                                <select id="isdateappended" name="isdateappended" class="form-control">
                                                @foreach(session('isdateappendeddd') as $key => $value)
                                                <option value="{!! $value['isdateappended'] !!}" @if(($value_rep->isdateappended) == $value['isdateappended']) selected @endif>{!! $value['isdateappended'] !!}</option>
                                                @endforeach
                                                </select>

                                                <strong> {!! Form::label('first_day_of_week', 'First Day of Week:') !!}</strong>
                                                <select id="first_day_of_week" name="first_day_of_week" class="form-control">
                                                @foreach(session('first_day_of_weekdd') as $key => $value)
                                                <option value="{!! $value['first_day_of_week'] !!}" @if(($value_rep->first_day_of_week) == $value['first_day_of_week']) selected @endif>{!! $value['first_day_of_week'] !!}</option>
                                                @endforeach
                                                </select>

                                                <strong> {!! Form::label('savewithmacro', 'Final File Type for Report:') !!}</strong>
                                                <select id="savewithmacro" name="savewithmacro" class="form-control">
                                                @foreach(session('savewithmacrodd') as $key => $value)
                                                <option value="{!! $value['savewithmacro'] !!}" @if(($value_rep->savewithmacro) == $value['savewithmacro']) selected @endif>{!! $value['savewithmacro'] !!}</option>
                                                @endforeach
                                                </select>

                                               <strong> {!! Form::label('zip_file_format', 'ZIP File Format:') !!}</strong>
                                                {!! Form::text('zip_file_format', $value_rep->zip_file_format, array('class' => 'form-control')) !!}

                                                <strong> {!! Form::label('deleteconnections', 'Remove Pivot Cache?:') !!}</strong>
                                                <select id="deleteconnections" name="deleteconnections" class="form-control">
                                                @foreach(session('deleteconnectionsdd') as $key => $value)
                                                <option value="{!! $value['deleteconnections'] !!}" @if(($value_rep->deleteconnections) == $value['deleteconnections']) selected @endif>{!! $value['deleteconnections'] !!}</option>
                                                @endforeach
                                                </select>

                                                <strong> {!! Form::label('nameofemailbodysheet', 'Sheet Name for Body Message:') !!}</strong>
                                                {!! Form::text('nameofemailbodysheet', $value_rep->nameofemailbodysheet, array('class' => 'form-control')) !!}

                                                <strong>  {!! Form::label('custom_email_subject', 'Custom Email Subject:') !!}</strong>
                                                {!! Form::text('custom_email_subject', $value_rep->custom_email_subject, array('class' => 'form-control')) !!}

                                                <strong>  {!! Form::label('customemailtext', 'Custom Email Text:') !!}</strong>
                                                {!! Form::text('customemailtext', $value_rep->customemailtext, array('class' => 'form-control')) !!}

                                                <strong> {!! Form::label('custom_attachment_name', 'Custom Attachment Name:') !!}</strong>
                                                {!! Form::text('custom_attachment_name', $value_rep->custom_attachment_name, array('class' => 'form-control')) !!}

                                                <strong>{!! Form::label('run_region', 'Run Region:') !!}</strong>
                                                <select id="run_region" name="run_region" class="form-control">
                                                @foreach(session('run_regiondd') as $key => $value)
                                                <option value="{!! $value['run_region'] !!}" @if(($value_rep->run_region) == $value['run_region']) selected @endif>{!! $value['run_region'] !!}</option>
                                                @endforeach
                                                </select>

                                                 <strong>{!! Form::label('report_status', 'Report Status:') !!}</strong>
                                                 <select id="report_status" name="report_status" class="form-control">
                                                @foreach(session('report_statusdd') as $key => $value)
                                                <option value="{!! $value['report_status'] !!}" @if(($value_rep->report_status) == $value['report_status']) selected @endif>{!! $value['report_status'] !!}</option>
                                                @endforeach
                                                </select>

                                                 
                                                 <strong>{!! Form::label('validationtables', 'Validation IDs:') !!}</strong>
                                                 {!! Form::text('validationtables', $value_rep->validationchecks, array('class' => 'form-control')) !!}
                                            
<input type="hidden" name="copyrecipients" value="{{$recipient}}" />
<input type="hidden" name="copyfrequency" value="{{$frequency}}" />
<input type="hidden" name="original_rid" value="{{$value_rep->rid}}" />
<br /><br />
{!! Form::submit('Create Report', array('class' => 'btn btn-primary btn-md')) !!}
<a href="{{ URL::to('reports') }}" class="btn btn-success btn-md"><i class="fa fa-close"></i> &nbsp; CANCEL</a>
{!! Form::close() !!}
</div>
@endforeach
</div>
@endsection
@section('js')
<script type="text/javascript">



 $('select[name=client]').on('change', function(){
    var input = document.getElementById("name");
    if($(this).val() == "Template Report"){
        $('input[name=final_name]').show();
        $('input[name=where_condition]').show();
        $('input[name=name]').hide().val(input.value);
    }else {
        $('input[name=final_name]').hide().val('');
        $('input[name=where_condition]').hide().val('');
        $('input[name=name]').show().val(input.value);
    }
});   

</script>
@endsection