@extends('autoreports::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Current Day Tracker</h3>
</div>
<div class="container-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
<div class="row-fluid">
    {!! $html->table(['class' => 'table-bordered table-condensed table-hover']) !!}
</div>
</div>
</div>
</div>
@endsection
@section('js')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ URL::to('vendor/datatables/buttons.server-side.js') }}"></script>
{!! $html->scripts() !!}
@endsection