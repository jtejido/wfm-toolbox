@extends('dbmgmt::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Notify</h3>
</div>
<div class="container-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-striped table-condensed" id="sortable">
    <thead>
        <tr>
            <th>Object ID</th>
            <th>Recipient ID</th>
            <th>First Fail</th>
            <th>Notify</th>
            <th>Resolved</th>
        </tr>
    </thead>
    <tbody>
    @foreach($notify as $key => $value)
        <tr>
            <td>{{ $value->obj_id }}</td>
            <td>{{ $value->recipient_id }}</td>
            <td>{{ $value->first_fail_ts }}</td>
            <td>{{ $value->notify_ts }}</td>
            <td>{{ $value->resolved_ts }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
</div>
</div>
</div>
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        }
} );
});
</script>
@endsection