@extends('dbmgmt::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Notify Recipients</h3>
    <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#create-notify-recipient">Create Record</button>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<small style="float: right;"><i>Columns marked with * are editable</i></small>
<br />
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-striped table-condensed" id="sortable">
    <thead>
        <tr>
            <th>Object ID</th>
            <th>Recipient ID</th>
            <th>Start Date*</th>
            <th>End Date*</th>
        </tr>
    </thead>
    <tbody>
    @foreach($notify_recipients as $key => $value)
        <tr>
            <td>{{ $value->obj_id }}</td>
            <td>{{ $value->recipient_id }}</td>
            <td><a href="#" class="testEdit" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="YYYY/MM/DD" data-template="YYYY / MMM / D" data-column="start_dte" data-url="/tools/notify_recipient/update/{{ $value->obj_id }}/{{ $value->recipient_id }}" data-pk="{{$value->client_id}}" data-title="change" data-name="start_dte" data-value="{{ $value->start_dte }}">{{ $value->start_dte }}</a></td>
            <td><a href="#" class="testEdit2" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="YYYY/MM/DD" data-template="YYYY / MMM / D" data-column="start_dte" data-url="/tools/notify_recipient/update/{{ $value->obj_id }}/{{ $value->recipient_id }}" data-pk="{{$value->client_id}}" data-title="change" data-name="start_dte" data-value="{{ $value->end_dte }}">{{ $value->end_dte }}</a></td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
</div>
</div>
</div>
<!-- Create Modal -->
<div id="create-notify-recipient" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                    <form method="POST" action="{{ URL::to('notify_recipient') }}" accept-charset="UTF-8">
                    {!! csrf_field() !!}
                            <div class="col-sm-12">
                                <strong>{!! Form::label('obj_id', 'Validation ID:') !!}</strong>
                               {!! Form::text('obj_id', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('recipient_id', 'Recipient ID:') !!}</strong>
                                {!! Form::text('recipient_id', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('start_dte', 'Start Date:') !!}</strong>
                                {!! Form::text('start_dte', null, array('class' => 'form-control', 'id' => 'datepickerstart')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('end_dte', 'End Date:') !!}</strong>
                                {!! Form::text('end_dte', null, array('class' => 'form-control', 'id' => 'datepickerend')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                            <div class="col-sm-12 text-center">
                            {!! Form::submit('Create Record', array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                    {!! Form::close() !!}
                    </div>        
            </div>
        
</div>
</div>
</div>
@endsection
@section('js')
<script>


$( function() {
    $( "#datepickerstart" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

$( function() {
    $( "#datepickerend" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );

$.fn.editable.defaults.mode = 'inline';
$(document).ready(function() {
    $('.testEdit').editable({
        combodate: { maxYear: 2018 },
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
    });

    $('.testEdit2').editable({
        combodate: { maxYear: 2018 },
        params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Server error. Check entered data.';
            } else {
                return response.responseText;
                // return "Error.";
            }
        }
     });
});

$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        }
} );
});

$(document).ready(function(){
$('#sortable tr').each(function() {
    $(this).children('th').eq(0).css('background-color', '#f6f6f6');
    $(this).children('th').eq(1).css('background-color', '#f6f6f6');
});
});
</script>
@endsection