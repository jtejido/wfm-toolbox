@extends('app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Objects</h3>
</div>
@if (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
<a href="#" class="close">&times;</a>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid">
        <table class="table-striped table-condensed" id="sortable">
            <thead>
                <tr>
                    <th>Object ID</th>
                    <th>Hour</th>
                    <th>Minutes</th>
                    <th>DOW</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($schedules as $key => $value)
                <tr>
                    <td>{{ $value->obj_id }}</td>
                    <td>{{ $value->time_hour }}</td>
                    <td>{{ $value->time_min }}</td>
                    <td>
                    <?php
                     $dow_arr = array("0", "1", "2", "3", "4", "5", "6");
                     $day_arr = array("sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday");
                     $dow = str_replace($dow_arr, $day_arr, $value->dow);
                     $dow = substr($dow, 1, -1);
                    ?>
                        {{ $dow  }}
                    </td>
                    <td>
                        <span style="text-align: center;">
                        <form method="POST" action="{{ URL::to('schedule/'.$value->obj_id.'/delete') }}" accept-charset="UTF-8">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-primary btn-md"> Delete</button>
                        <a class="btn btn-primary btn-md" href="{{ URL::to('schedule/'.$value->obj_id.'/edit') }}"> Edit</a>
                        {!! Form::hidden('_method', 'DELETE') !!}
                        {!! Form::close() !!}
                        </span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>  
</div>

<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "pageLength": 5,
        "language": {
            "emptyTable": "No Schedules Available",
            "search": "Filter:"
        }
} );
});
</script>
@stop