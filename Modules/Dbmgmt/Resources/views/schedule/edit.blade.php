@extends('app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Schedules</h3>
</div>
@if (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
<a href="#" class="close">&times;</a>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<form method="POST" action="{{ URL::to('schedule/'.$schedule->obj_id.'/put') }}" accept-charset="UTF-8">
<input name="_method" type="hidden" value="PUT">
        {!! csrf_field() !!}
        <div class="col-sm-7 col-sm-offset-2">
        {!! Form::label('obj_id', 'Validation ID:') !!}

        {!! Form::text('obj_id', $schedule->obj_id, array('class' => 'form-control', 'readonly' => 'readonly')) !!}
        </br>
        {!! Form::label('time_hour', 'Time(Hour):') !!}

        {!! Form::text('time_hour', substr($schedule->time_hour, 1, -1), array('class' => 'form-control')) !!}
        </br>
        {!! Form::label('time_min', 'Time(Minutes):') !!}

        {!! Form::text('time_min', substr($schedule->time_min, 1, -1), array('class' => 'form-control')) !!}
        </br>
         {!! Form::label('new_dow[]', 'Day of Week:') !!}
            
                <div class="input-group">
                    {!! Form::checkbox('new_dow[]', '1', in_array("1", $dow_arr)) !!}&nbsp;<label>Monday</label>
                    </br>
                    {!! Form::checkbox('new_dow[]', '2', in_array("2", $dow_arr)) !!}&nbsp;<label>Tuesday</label>
                    </br>
                    {!! Form::checkbox('new_dow[]', '3', in_array("3", $dow_arr)) !!}&nbsp;<label>Wednesday</label>
                    </br>
                    {!! Form::checkbox('new_dow[]', '4', in_array("4", $dow_arr)) !!}&nbsp;<label>Thursday</label>
                    </br>
                    {!! Form::checkbox('new_dow[]', '5', in_array("5", $dow_arr)) !!}&nbsp;<label>Friday</label>
                    </br>
                    {!! Form::checkbox('new_dow[]', '6', in_array("6", $dow_arr)) !!}&nbsp;<label>Saturday</label>
                    </br>
                    {!! Form::checkbox('new_dow[]', '0', in_array("0", $dow_arr)) !!}&nbsp;<label>Sunday</label>
                </div>
                </br>
                </br>
     {!! Form::submit('Save this Record', array('class' => 'btn btn-primary btn-lg')) !!}
    <a href="{!! URL::to('schedule') !!}" class="btn btn-success btn-lg"><i class="fi-close large"></i> &nbsp; CANCEL</a>
    </div></br></br></br>
{!! Form::close() !!}
</div>
@stop