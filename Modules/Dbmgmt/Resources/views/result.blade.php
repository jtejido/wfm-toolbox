@extends('dbmgmt::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-wrench" aria-hidden="true"></i> Result</h3>
    <button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#create-result">Create Record</button>
</div>
@if (session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>All Items</h5>
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
<table class="table-striped table-condensed" id="sortable">
    <thead>
        <tr>
            <th>Object ID</th>
            <th>Timestamp</th>
            <th>Result</th>
            <th>Result Extra</th>
            <th>Pass</th>
        </tr>
    </thead>
    <tbody>
    @foreach($results as $key => $value)
        <tr>
            <td>{{ $value->obj_id }}</td>
            <td>{{ $value->val_timestamp }}</td>
            <td>{{ $value->val_result }}</td>
            <td>{{ $value->val_result_extra }}</td>
            <td>
                @if ($value->val_pass == "1") 
                    True
                @elseif ($value->val_pass == "0") 
                    False
                @endif 
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
</div>
</div>
</div>
<!-- Create Modal -->
<div id="create-result" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Record</h4>
      </div>
            <div class="modal-body">
                    <div class="container-fluid">
                        <form method="POST" action="{{ URL::to('result') }}" accept-charset="UTF-8">
                        {!! csrf_field() !!}
                            <div class="col-sm-12">
                                <strong>{!! Form::label('obj_id', 'Validation ID:') !!}</strong>
                               {!! Form::text('obj_id', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('val_result', 'Result:') !!}</strong>
                                {!! Form::text('val_result', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('val_result_extra', 'Result Extra:') !!}</strong>
                                {!! Form::text('val_result_extra', null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="col-sm-12">
                                <strong> {!! Form::label('val_pass', 'Result Pass?') !!}</strong>
                                {!! Form::select('val_pass', array('t' => 'True', 'f' => 'False'), null, array('class' => 'form-control')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                            <div class="col-sm-12 text-center">
                            {!! Form::submit('Create Record', array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                            <div class="clearfix"></div>
                            </br></br>
                        {!! Form::close() !!}
                    </div>
                 </div>
</div>
</div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        }
} );
});
</script>
@endsection