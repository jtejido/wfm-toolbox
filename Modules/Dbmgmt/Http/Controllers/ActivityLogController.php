<?php
namespace Modules\Dbmgmt\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\DB\ActivityLog;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class ActivityLogController extends Controller
{


    public function index()
    {

        // get all the results
        $activity_logs = ActivityLog::all();

        // load the view and pass the reports
        return view('dbmgmt::activity_log')
            ->with('activity_logs', $activity_logs);

    }

    

}