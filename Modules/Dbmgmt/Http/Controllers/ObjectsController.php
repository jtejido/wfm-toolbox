<?php
namespace Modules\Dbmgmt\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\DB\Objects;
use \App\Models\DB\Schedules;
use View;
use DB;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;

class ObjectsController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        //reserve next obj_id for 'create' modal window of new report
        $query = "nextval('meta_dbmgmt.val_objects_obj_id_seq') as nxt";
        $next_obj_id = Objects::selectRaw($query)->value('nxt');
        Session::put('next_obj_id', $next_obj_id);
        // get all the reports
        $objects = Objects::all();
        // load the view and pass the reports
        return view('dbmgmt::objects')
            ->with('objects', $objects);

    }


    public function store ()
    {

        $rules = array(
            'obj_id'       => 'required',
            'obj_descr'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('objects')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            DB::connection()->enableQueryLog();
            $object = new Objects;
            $object->obj_id       = Input::get('obj_id');
            $object->obj_name = Input::get('obj_name'); 
            $object->obj_schema       = Input::get('obj_schema');
            $object->obj_type = Input::get('obj_type'); 
            $object->obj_where       = Input::get('obj_where');
            $object->obj_column = Input::get('obj_column'); 
            $object->obj_operator       = Input::get('obj_operator');
            $object->obj_exp_result = Input::get('obj_exp_result'); 
            $object->obj_descr       = Input::get('obj_descr');
            $object->obj_active = Input::get('obj_active'); 
            $object->obj_friendly_name = Input::get('obj_friendly_name'); 
            $object->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'validation');

            Session::flash('message', 'Record was Successfully Saved!');
            return Redirect::to('objects');
        }
    }


    public function update(Request $request, $obj_id, $obj_descr)
    {
       $column_name = Input::get('name');
       if(Input::get('name') == 'obj_active'){
               $column_value = Input::get('value');
        }
        else {
               $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        }
        
            DB::connection()->enableQueryLog();
            $test = Objects::select()
                ->where('obj_id', '=', $obj_id)
                ->where('obj_descr', '=', $obj_descr)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'validation');
            return response()->json([ 'code'=>200], 200);
    }

    public function edit($obj_id, $obj_descr)
    {
        $schedules = Schedules::where('obj_id', $obj_id)->get();

        return view('dbmgmt::object.edit')
            ->with('schedules', $schedules);

    }

}