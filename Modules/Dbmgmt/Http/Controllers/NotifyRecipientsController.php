<?php
namespace Modules\Dbmgmt\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\DB\NotifyRecipients;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;
use DB;


class NotifyRecipientsController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        // get all the reports
        $notify_recipients = NotifyRecipients::all();
        // load the view and pass the reports
        return view('dbmgmt::notify_recipient')
            ->with('notify_recipients', $notify_recipients);
    }


    public function store ()
    {

        $rules = array(
            'obj_id'       => 'required',
            'recipient_id'      => 'required',
            'start_dte'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('notify_recipient')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            DB::connection()->enableQueryLog();
            $notify_recipient = new NotifyRecipients;
            $notify_recipient->obj_id       = Input::get('obj_id');
            $notify_recipient->recipient_id = Input::get('recipient_id'); 
            $notify_recipient->start_dte = !empty(Input::get('start_dte')) ? Input::get('start_dte') : null;
            $notify_recipient->end_dte = !empty(Input::get('end_dte')) ? Input::get('end_dte') : null;
            $notify_recipient->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'notify_recipients');
            // redirect
            Session::flash('message', 'Record was Successfully Saved!');
            return Redirect::to('notify_recipient');
        }
    }


    public function update(Request $request, $obj_id, $recipient_id)
    {
       $column_name = Input::get('name');
       $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        
            DB::connection()->enableQueryLog();
            $test = NotifyRecipients::select()
                ->where('obj_id', '=', $obj_id)
                ->where('recipient_id', '=', $recipient_id)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'notify_recipients');
            return response()->json([ 'code'=>200], 200);
    }


}