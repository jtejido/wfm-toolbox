<?php
namespace Modules\Dbmgmt\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\DB\Results;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;
use DB;

class ResultsController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        // get all the results
        $results = Results::whereRaw('val_timestamp >= now()::DATE-5')->get();

        // load the view and pass the reports
        return view('dbmgmt::result')
            ->with('results', $results);

    }


    public function store ()
    {

        $rules = array(
            'obj_id'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('result')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            DB::connection()->enableQueryLog();
            $result = new Results;
            $result->obj_id       = Input::get('obj_id');
            $result->val_result       = Input::get('val_result');
            $result->val_result_extra = Input::get('val_result_extra'); 
            $result->val_pass       = Input::get('val_pass');
            $result->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'results');
            // redirect
            Session::flash('message', 'Record was Successfully Saved!');
            return Redirect::to('result');
        }
    }

}