<?php
namespace Modules\Dbmgmt\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\DB\Notify;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class NotifyController extends Controller
{

    public function index()
    {

        // get all the reports
        $notify = Notify::all();
        // load the view and pass the reports
        return view('dbmgmt::notify')
            ->with('notify', $notify);
    }

}