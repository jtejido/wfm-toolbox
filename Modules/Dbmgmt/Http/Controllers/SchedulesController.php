<?php
namespace Modules\Dbmgmt\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\Models\DB\Schedules;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;
use DB;


class SchedulesController extends Controller
{


    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $schedules = Schedules::all();


        return view('dbmgmt::schedule')
            ->with('schedules', $schedules);
    }


    public function store ()
    {

        $rules = array(
            'obj_id'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('schedule')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            DB::connection()->enableQueryLog();
            $schedule = new Schedules;
            $schedule->obj_id       = Input::get('obj_id');
            $schedule->time_hour = '{' . (!empty(Input::get('time_hour')) ? Input::get('time_hour') : '0') . '}';
            $schedule->time_min = '{' . (!empty(Input::get('time_min')) ? Input::get('time_min') : '0') . '}';
                $dow_temp = array();
                    foreach (Input::get('new_dow') AS $key => $value){
                        $dow_temp[] = "$value";
                    }
                $dow = implode(',', $dow_temp);
            $schedule->dow       = '{'. $dow . '}';
            $schedule->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'schedule');
            Session::flash('message', 'Record was Successfully Saved!');
            return Redirect::to('schedule');
        }
    }


    public function edit($obj_id)
    {
        $schedule = Schedules::find($obj_id);

             $dows = substr($schedule->dow, 1, -1);
             $dow_temp = explode(',', $dows);
             $dow_arr = array();
                foreach ($dow_temp AS $key => $value){
                    $dow_arr[] = "$value";
                }



        return view('dbmgmt::schedule.edit')
            ->with('schedule', $schedule)->with('dow_arr', $dow_arr);

    }


    public function update($obj_id)
    {

        $rules = array(
            'obj_id'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('schedule/' . $obj_id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            DB::connection()->enableQueryLog();
            $schedule = Schedules::find($obj_id);
            $schedule->obj_id       = Input::get('obj_id');
            $schedule->time_hour = '{' . (!empty(Input::get('time_hour')) ? Input::get('time_hour') : '0') . '}';
            $schedule->time_min = '{' . (!empty(Input::get('time_min')) ? Input::get('time_min') : '0') . '}';
                $dow_temp = array();
                    foreach (Input::get('new_dow') AS $key => $value){
                        $dow_temp[] = "$value";
                    }
                $dow = implode(',', $dow_temp);
            $schedule->dow       = '{'. $dow . '}';
            $schedule->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'schedule');
            Session::flash('message', 'Record was Successfully Updated!');
            return Redirect::to('schedule');

        }
    }

    public function destroy($obj_id)
    {
        DB::connection()->enableQueryLog();
        $schedule = Schedules::where('obj_id', $obj_id)->delete();
        $queries = DB::getQueryLog();
        $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'schedule');

        Session::flash('message', 'Record was Successfully Deleted!');
        return Redirect::to('schedule');

    }

}