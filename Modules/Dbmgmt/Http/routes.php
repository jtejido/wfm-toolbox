<?php

Route::group(['middleware' => ['web', 'auth', 'module:dbmgmt'], 'namespace' => 'Modules\Dbmgmt\Http\Controllers'], function()
{

		    Route::get('notify', 'NotifyController@index');
		    Route::post('notify_recipient/update/{obj_id}/{recipient_id}', ['as' => 'notify_recipient/update', 'uses' => 'NotifyRecipientsController@update']);
		    Route::get('notify_recipient', 'NotifyRecipientsController@index');
		    Route::get('objects/{obj_id}/{obj_descr}/schedules', 'ObjectsController@edit')->where('obj_descr', '(.*)');
		    Route::post('objects/update/{obj_id}/{obj_descr}', ['as' => 'objects/update', 'uses' => 'ObjectsController@update']);
		    Route::get('objects', 'ObjectsController@index');
		    Route::get('result', 'ResultsController@index');
		    Route::delete('schedule/{obj_id}/delete', 'SchedulesController@destroy');
		    Route::get('schedule', 'SchedulesController@index');
		    Route::get('activity_log', 'ActivityLogController@index');



});