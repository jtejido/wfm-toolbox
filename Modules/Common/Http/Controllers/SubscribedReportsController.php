<?php
namespace Modules\Common\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
use DB;
use PDO;
use \App\Models\Common\SubscribedReports;
use \App\Models\Reports\ReportRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use \App\Library\QueryTrack;

class SubscribedReportsController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        if (empty(Session::get('recipient_data_toolbox'))){
            return Redirect::to('register');
        }
       
        else {
             $assigned_reports = SubscribedReports::where('recipient_id', '=', Session::get('recipient_data_toolbox'))->get();
            return view('common::subscribed_reports')
            ->with('assigned_reports', $assigned_reports);
        }
        
    }

        public function destroy($rid)
    {

        // Do internal check of the table first
        $check = ReportRequest::where('rid' ,'=', $rid)->where('recipient_id' ,'=', Session::get('recipient_data_toolbox'))
                ->where(function($query){
                 $query->where('request_type', '!=', 'approve');
                 $query->where('request_type', '!=', 'rejected');
                    })
                ->first();

         if(empty($check->rid)){
            DB::connection()->enableQueryLog();
            DB::table('app_reports_mgmt.report_request')->insert(
                    array('rid' => $rid, 'recipient_id' => Session::get('recipient_data_toolbox'), 'request_type' => 'delete', 'request_datetime' => 'now()'));
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'subscribed_reports');
                    Session::flash('message', 'Deletion will approved by a Manager.');
                    return Redirect::to('subscribed_reports');
            }
        else {
                    Session::flash('message', 'This is Already Pending your Manager approval.');
                    return Redirect::to('subscribed_reports');
            }
        // redirect


    }

         public function add($rid)
    {

        $check = ReportRequest::where('rid' ,'=', $rid)->where('recipient_id' ,'=', Session::get('recipient_data_toolbox'))
                ->where(function($query){
                 $query->where('request_type', '!=', 'approve');
                 $query->where('request_type', '!=', 'rejected');
                    })
                ->first();
        if(empty($check->rid)){
                $query = "nextval('app_reports_mgmt.report_request_request_id_seq') as nxt";
                $next_request_id = ReportRequest::selectRaw($query)->value('nxt');
                DB::connection()->enableQueryLog();
                $subscribe = new ReportRequest;
                $subscribe->request_id       = $next_request_id;
                $subscribe->recipient_id       = Session::get('recipient_data_toolbox');
                $subscribe->rid       = $rid;
                $subscribe->request_type = 'add';
                $subscribe->request_datetime = Carbon::now();
                 $subscribe->save();
                $queries = DB::getQueryLog();
                $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'subscribed_reports');

           Session::flash('message', 'This will be Approved by the Manager.');
            return Redirect::to('subscribed_reports');
            }
        else
        {
           Session::flash('message', 'This is Already Pending a Manager Approval.');
            return Redirect::to('subscribed_reports');
        }

    }

    public function bulk_delete(Request $request)
    {
        if (Input::has('rids_to_delete')) {
            $rids_to_delete = Input::get('rids_to_delete');
            
            foreach ($rids_to_delete as $rid_to_delete) {
                DB::connection()->enableQueryLog();
                DB::table('app_reports_mgmt.report_request')->insert(
                    array('rid' => $rid_to_delete, 'recipient_id' => Session::get('recipient_data_toolbox'), 'request_type' => 'delete', 'request_datetime' => 'now()'));
                    $queries = DB::getQueryLog();
                    $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'customer');
            }
            
            Session::flash('message', 'These will be Approved by your Manager or an Admin.');
            return Redirect::to('subscribed_reports');
        } else {
            Session::flash('message', 'No Reports Selected.');
            return Redirect::to('subscribed_reports');
        }

    }

    public function bulk_add(Request $request)
    {
        if (Input::has('rids_to_add')) {
            $rids_to_add = Input::get('rids_to_add');
            
            foreach ($rids_to_add as $rid_to_add) {
                DB::connection()->enableQueryLog();
                $query = "nextval('app_reports_mgmt.report_request_request_id_seq') as nxt";
                $next_request_id = ReportRequest::selectRaw($query)->value('nxt');
                DB::connection()->enableQueryLog();
                $subscribe = new ReportRequest;
                $subscribe->request_id       = $next_request_id;
                $subscribe->recipient_id       = Session::get('recipient_data_toolbox');
                $subscribe->rid       = $rid_to_add;
                $subscribe->request_type = 'add';
                $subscribe->request_datetime = Carbon::now();
                $subscribe->save();
                $queries = DB::getQueryLog();
                $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'customer');
            }
            
            Session::flash('message', 'These will be Approved by your Manager or an Admin.');
            return Redirect::to('subscribed_reports');
        } else {
            Session::flash('message', 'No Reports Selected.');
            return Redirect::to('subscribed_reports');
        }

    }

}