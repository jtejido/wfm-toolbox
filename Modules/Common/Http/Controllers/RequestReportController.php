<?php
namespace Modules\Common\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
use DB;
use PDO;
use \App\Models\Common\RequestReports;
use \App\Models\Reports\ReportRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use \App\Library\QueryTrack;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use App\DataTables\Common\RequestReportDataTable;

class RequestReportController extends Controller
{

    protected $htmlBuilder; 

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
    }

    public function index(Request $request, RequestReportDataTable $dataTable)
    {
        if (empty(Session::get('recipient_data_toolbox'))){
            return Redirect::to('register');
        }
        else {

            if ($request->ajax()) {
                return $dataTable->ajax();
            }

            $html = $dataTable->html();
            return view('common::request_report')->with('html', $html);
        }

        

    }

    public function search(Request $request)
    {       
            $request->session()->forget('report_name');
            $name = $request->keywords; 
            //Remove Special Characters from the report name
            $name = preg_replace('/[^ A-Za-z0-9\-]/', '', $name);
            $request->session()->put('report_name', $name);
            return redirect('request_report');
    }


        public function add($rid, $recipient_id)
    {

        if (empty(Session::get('recipient_data_toolbox'))){
            return Redirect::to('register');
        }
        else {
            $check = ReportRequest::where('rid' ,'=', $rid)->where('recipient_id' ,'=', $recipient_id)
                ->where(function($query){
                 $query->where('request_type', '!=', 'approve');
                 $query->where('request_type', '!=', 'rejected');
                    })
                ->first();
                if(empty($check->rid)){
                        $query = "nextval('app_reports_mgmt.report_request_request_id_seq') as nxt";
                        $next_request_id = ReportRequest::selectRaw($query)->value('nxt');
                        DB::connection()->enableQueryLog();
                        $subscribe = new ReportRequest;
                        $subscribe->request_id       = $next_request_id;
                        $subscribe->recipient_id = $recipient_id; 
                        $subscribe->rid = $rid;
                        $subscribe->request_type = 'add';
                        $subscribe->request_datetime = Carbon::now();
                        $subscribe->save();
                        $queries = DB::getQueryLog();
                        QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'request_report');
                    Session::flash('message', 'This will be Approved by the Manager.');
                    return Redirect::to('request_report');
                    }
                else
                {
                    Session::flash('message', 'This is Already Pending a Manager Approval.');
                    return Redirect::to('request_report');
                }
        }
        

    }


}
