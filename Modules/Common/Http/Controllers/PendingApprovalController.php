<?php
namespace Modules\Common\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Models\Reports\ApprovalQueue;
use App\Models\Common\RejectedRequests;
use App\Models\Common\ApprovedRequests;


class PendingApprovalController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return View
     */

    public function index()
    {
        if (empty(Session::get('recipient_data_toolbox'))){
            return Redirect::to('register');
        }

        else {
        $pending_reports = ApprovalQueue::where('recipient_id', '=', Session::get('recipient_data_toolbox'))->get();
        $approved_reports = ApprovedRequests::where('recipient_id', '=', Session::get('recipient_data_toolbox'))->orderBy('datetime', 'desc')->get();
        $rejected_reports = RejectedRequests::where('recipient_id', '=', Session::get('recipient_data_toolbox'))->orderBy('datetime', 'desc')->get();

        return view('common::pending_approval')
            ->with('pending_reports', $pending_reports)->with('approved_reports', $approved_reports)->with('rejected_reports', $rejected_reports);
        }
    }

   

}