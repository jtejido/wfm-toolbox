<?php
namespace Modules\Common\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
use Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;



class DownloadReportsController extends Controller
{

    

    public function index()
    {

        
        return view('common::download_reports');

    }


    public function subfolders($folder)
    {

    $folder = preg_replace('/[^ A-Za-z]/', '', $folder);
    $report_group = $folder;
    $directory = '/test/'.$report_group;
    
    if(isset($report_group) && !empty($report_group)) {
            $valid = array_search($report_group,Session::get('download_reports_available'),true);
    }

    if($valid !== FALSE) {
        if(file_exists(storage_path('app').$directory)) {
                $scanned_directory = array_diff(Storage::directories($directory), array('..', '.'));
                    foreach($scanned_directory as $key=>$value){
                      $scanned_directory[$key]=str_replace("test/".$report_group."/","",$value);
                    }
        } else {
            return View::make('templates.common.download_reports')->withErrors('No Files Found.');
        }

        $scanned_directory = array_reverse($scanned_directory);
        return view('common::download.subfolders')
        ->with('report_group', $report_group)->with('scanned_directory', $scanned_directory);
    }
    else {
        return view('common::download_reports')->withErrors('Security Error. The folder specified ('.$report_group.') is not part of your assigned Accounts.');
        }
    }

    public function files($folder, $subfolder)
    {
    $folder = preg_replace('/[^ A-Za-z]/', '', $folder);
    $subfolder = preg_replace('/[^ A-Za-z0-9\-]/', '', $subfolder);
    $report_group = $folder;
    $report_sub_group = $subfolder;
    $directory = '/test/'.$report_group.'/'.$report_sub_group;
    if(isset($report_group) && !empty($report_group)) {
            $valid = array_search($report_group,Session::get('download_reports_available'),true);
    }

    if($valid !== FALSE) {
        if(file_exists(storage_path('app').$directory)) {
            $scanned_directory = array_diff(Storage::files($directory), array('..', '.'));
                foreach($scanned_directory as $key=>$value){
                  $scanned_directory[$key]=str_replace("test/".$report_group."/".$report_sub_group."/","",$value);
                }
        } else {
            return view('common::download_reports')->withErrors('No Files Found.');
        }
            $scanned_directory = array_reverse($scanned_directory);
           return view('common::download.files')
                ->with('report_group', $report_group)->with('report_sub_group', $report_sub_group)->with('scanned_directory', $scanned_directory);  
        }
    else {
            return view('common::download_reports')->withErrors('Security Error. The folder specified ('.$report_group.') is not part of your assigned Accounts.');
    }

    }
    
    public function download($folder, $subfolder, $file)
    {
    $folder = preg_replace('/[^ A-Za-z]/', '', $folder);
    $subfolder = preg_replace('/[^ A-Za-z0-9\-]/', '', $subfolder);
    $download = '/test/'.$folder.'/'.$subfolder.'/'.$file;

    if(isset($folder) && !empty($folder)) {
            $valid = array_search($folder,Session::get('download_reports_available'),true);
    }

    if($valid !== FALSE) {
        if(file_exists((storage_path('app').$download))) {
        $file = storage_path('app').$download; // or wherever you have stored your PDF files
        return response()->download($file);
        }
        else {
        return view('common::download_reports')->withErrors('File not Found.');
        }
    }
    else {
            return view('common::download_reports')->withErrors('Security Error. The folder specified ('.$folder.') is not part of your assigned Accounts.');
    }
    }


}