<?php
namespace Modules\Common\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Models\CDP\CDPClients;
use \App\Models\CDP\CDPSites;
use \App\Models\CDP\CDPForm;
use \App\Library\QueryTrack;


class CDPController extends Controller
{
    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }


    public function index()
    {
        $sites = CDPSites::select()->orderBy('site')->get();
        $clients = CDPClients::select()->orderBy('client')->get();


        return view('common::clean_desk_policy')
            ->with('sites', $sites)->with('clients', $clients);
    }


    public function store()
    {

        $rules = array(
            'site'       => 'required',
            'client'       => 'required',
            'date_time'       => 'required',
            'cdv'       => 'required',
            'cpv'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('clean_desk_policy')
                ->withErrors($validator);
        } else {
            $site_name = CDPSites::select()->where('site_id', '=', Input::get('site'))->first();
            $client_name = CDPClients::select()->where('client_id', '=', Input::get('client'))->first();
            DB::connection()->enableQueryLog();
            $form = new CDPForm;
            $form->client_id = Input::get('client'); 
            $form->client       = $client_name->client;
            $form->site_id       = Input::get('site');
            $form->site = $site_name->site;
            $form->date_time = Input::get('date_time');
            $form->clean_desktop_violation = Input::get('cdv');
            $form->clean_desktop_violation_info = Input::get('cdvi');
            $form->cell_phone_violation = Input::get('cpv');
            $form->cell_phone_violation_info = Input::get('cpvi');
            $form->addtl_comments = Input::get('addtl_comments');
            $form->winid = Session::get('toolbox_s3_id');
            $form->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'clean_desk_policy');

            Session::flash('message', 'Form was Successfully Saved!');
            return Redirect::to('clean_desk_policy');
        }
    }

   

}