<?php
Route::group(['middleware' => ['web', 'auth', 'module:common'], 'namespace' => 'Modules\Common\Http\Controllers'], function()
{

                    
                    Route::get('subscribed_reports/{rid}/add', 'SubscribedReportsController@add');
                    Route::get('subscribed_reports/{rid}/delete', 'SubscribedReportsController@destroy');
                    Route::get('request_report/{rid}/{recipient_id}/add', 'RequestReportController@add');
                    Route::post('subscribed_reports/bulk_add', 'SubscribedReportsController@bulk_add');
                    Route::post('subscribed_reports/bulk_delete', 'SubscribedReportsController@bulk_delete');
                    Route::post('clean_desk_policy', 'CDPController@store');

            Route::get('/', 'SubscribedReportsController@index');
            Route::get('subscribed_reports', 'SubscribedReportsController@index');
            Route::post('request_report/executeSearch', 'RequestReportController@search');
            Route::get('request_report', 'RequestReportController@index');
            Route::get('download_reports/{folder}/subfolder/{subfolder}/files/{file}', 'DownloadReportsController@download');
            Route::get('download_reports/{folder}/subfolder/{subfolder}/files', 'DownloadReportsController@files');
            Route::get('download_reports/{folder}/subfolder', 'DownloadReportsController@subfolders');
            Route::get('download_reports', 'DownloadReportsController@index');
            Route::get('pending_approval', 'PendingApprovalController@index');
            Route::get('clean_desk_policy', 'CDPController@index');
});
