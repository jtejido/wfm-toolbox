@extends('common::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-area-chart" aria-hidden="true"></i> Subscribed Reports</h3>
</div>
@if(session('message'))
    <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="alert alert-info">
What's shown here are the reports you're currently receiving, and previously received via email at a set frequency. If you wish to opt out of receiving a specific report, you should click the <code>Unsubscribe</code> button found next to the report, if you want to re-add a previous report, click the <code>Subscribe</code> button. Both actions would require a Manager otr Admin's Approval.
</div>
<div class="container-fluid">
<form method="post" action="{{ URL::to('subscribed_reports/bulk_delete') }}">
{!! csrf_field() !!}
<div class="col-sm-6">
  <div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-check"></i></span>
      <h5>Subscribed Reports</h5>
      <button type="submit" class="buttons btn btn-default btn-sm">Unsubcribe Selected</button>
    </div>
    <div class="widget-content" >
      <div class="paging-content" id="content">
      @foreach($assigned_reports as $key=>$value)
        <?php
        $text = date_parse(". $value->start_dte .");
        $monthNum  = $text['month'];
        $monthName = date('F', mktime(0, 0, 0, $monthNum, 10));
        ?>
        @if (empty($value->end_dte))
        <div>
            <br />
            <article class="assigned">
                <div class="assigned-date"><label style="text-align: center">Date Subscribed:</label>
                  <p class="assigned-month">{!! $text['year']!!} {!!$monthName!!} </p>
                  <p class="assigned-day">{!! $text['day'] !!}</p>
                </div>
                <div class="assigned-desc">
                  <h5 class="assigned-desc-header">{!! $value->name !!}</h5>
                  <p class="assigned-desc-detail">
                    @if (empty($value->end_dte))
                        
                            <span class="assigned-desc-time"></span><i>You're Currently Subscribed to this Report.</i>
                            <input type="checkbox" class="pull-right" name="rids_to_delete[]" value="{{$value->rid}}" />

                    @endif
                  </p>
                    @if (empty($value->end_dte))


                            <a class="rsvp btn btn-danger btn-md fa fa-trash" href="{{ URL::to('subscribed_reports/'.$value->rid.'/delete') }}"> &nbsp;&nbsp;Unsubscribe&nbsp;&nbsp;&nbsp; </a>

                    @endif
                </div>
              </article>
          </div>
        @endif
        @endforeach
      </div>
      <div class="paginationy">
        <ul>
            <li><a href="#" id="prev" class="prevnext">Previous</a></li>
            <li><a href="#" id="next" class="prevnext">Next</a></li>
        </ul>
        <br />
      </div>
    </div>
   </div> 
</div>
</form>
<form method="post" action="{{ URL::to('subscribed_reports/bulk_add') }}">
{!! csrf_field() !!}
<div class="col-sm-6">
  <div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-trash"></i></span>
      <h5>Unsubscribed Reports</h5>
      <button type="submit" class="buttons btn btn-default btn-sm">Subcribe Selected</button>    
    </div>
    <div class="widget-content" >
    <div class="paging-contentx" id="content2">
        @foreach($assigned_reports as $key=>$value)
        <?php
        $text = date_parse(". $value->start_dte .");
        $monthNum  = $text['month'];
        $monthName = date('F', mktime(0, 0, 0, $monthNum, 10));
        ?>
        @if (!empty($value->end_dte))
          <div>
            <br />
            <article class="assigned">
                <div class="assigned-date"><label style="text-align: center">Date Subscribed:</label>
                  <p class="assigned-month">{!! $text['year']!!} {!!$monthName!!} </p>
                  <p class="assigned-day">{!! $text['day'] !!}</p>
                </div>
                <div class="assigned-desc">
                  <h5 class="assigned-desc-header">{!! $value->name !!}</h5>
                  <p class="assigned-desc-detail">
                    @if (!empty($value->end_dte))
                  <span class="assigned-desc-time"></span><i>Report was Unsubscribed {!! $value->end_dte !!}</i>
                  <input type="checkbox" class="pull-right" name="rids_to_add[]" value="{{$value->rid}}" />
                    @endif
                  </p>
                    @if (!empty($value->end_dte))
                        <a class="rsvp btn btn-success btn-md fa fa-plus" href="{{ URL::to('subscribed_reports/'.$value->rid.'/add') }}"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subscribe&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    @endif
                </div>
            </article>
            </div>
      @endif
      @endforeach
      </div>
      <div class="paginationx">
        <ul>
            <li><a href="#" id="prevx" class="prevnext">Previous</a></li>
            <li><a href="#" id="nextx" class="prevnext">Next</a></li>
        </ul>
        <br />
      </div>
    </div>
  </div>
</div>
</form>
</div>
@endsection
@section('js')
<script>

jQuery(window).load(function() {
    $('#content').MyPagination({height: 400, fadeSpeed: 0});
});
jQuery(window).load(function() {
    $('#content2').MyPaginationx({heightx: 400, fadeSpeedx: 0});
});
</script>
@endsection