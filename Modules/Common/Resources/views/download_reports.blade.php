@extends('common::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-download" aria-hidden="true"></i> Download Reports</h3>
	<div class="dropdown">
	  <button class="btn btn-default dropdown-toggle test" type="button" data-toggle="dropdown">Select Account
	  <span class="caret"></span></button>
	  <ul class="dropdown-menu scrollable-menu collapse" id="test">
		 @foreach(session('download_reports_available') as $folder)
				<li><a href="{{ URL::to('download_reports/'.$folder.'}/subfolder') }}">{!!$folder!!}</a></li>
			@endforeach
	  </ul>
	</div>
</div>

@if (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="alert alert-info">
<h5>Select your Account from above to get Started.</h5>
</div>
</div>
@endsection
