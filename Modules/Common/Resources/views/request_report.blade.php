@extends('common::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-search-plus" aria-hidden="true"></i> Request Report</h3>
    <div class="col-sm-4">
        <div class="row">

            <form class="form-inline" method="POST" action="{{ URL::to('request_report/executeSearch') }}">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="text" class="form-control" name="keywords" id="search-input" placeholder="Search Report">
            </div>
            <div class="form-group">
                 <button id="searchbutton" type="submit" class="btn btn-default"><i class="fa fa-search" ></i></button>
            </div>
            </form>

         </div>
     </div>
     <br /><br />
</div>

<div class="container-fluid">
@if (session('message'))
    <div class="alert alert-info">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@endif
<div class="alert alert-info">
Using the Search field above, you can look for a report you wish to opt into, then click <code>Subscribe</code> button next to the report. Take note that this will require an Admin's approval and will take effect the next day after approval.
</div>
<div class="row-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-search"></i></span>
      <h5>Search Results</h5>
    </div>
    <div class="widget-content" >
        <div class="row-fluid">
               {!! $html->table(['class' => 'table-bordered table-condensed table-hover']) !!}
        </div>
    </div>
</div>
</div>
</div>
@endsection
@section('js')
{!! $html->scripts() !!}
@endsection