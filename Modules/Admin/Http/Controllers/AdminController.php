<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Controller;
use \App\Models\Admin\ModuleMap;
use \App\Models\Admin\ModuleMapDash;
use \App\Models\Admin\QueryUsersDash;
use \App\Models\Admin\WebReport;
use \App\Models\Admin\WebReportUsers;
use \App\Models\Admin\AccountMapDash;
use \App\Models\Admin\AccountMap;
use \App\Library\SSP;
use \App\QueryTracker;
use View;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use \App\Library\QueryTrack;
use DB;


class AdminController extends Controller
{

    public function __construct(QueryTrack $track) {
        $this->track = $track;
        }

    public function index()
    {

        $admins = ModuleMapDash::all();
        $query_users = QueryUsersDash::all();
        $webreports = WebReport::distinct()->select('report_guid', 'title')->orderBy('title', 'desc')->get();
        $accounts = AccountMapDash::distinct()->select('client')->orderBy('client', 'asc')->get();
        
        $modules = ['elements', 'auto_reports', 'dbmgmt', 'mss_search', 'data_delivery', 'web_query', 'flow'];

       return view('admin::admin')
            ->with(compact('admins', 'modules', 'query_users', 'webreports', 'accounts'));

    }


   
     public function update(Request $request, $winid)
    {
       $column_name = Input::get('name');
       if(Input::get('name') == 'is_admin'){
               $column_value = Input::get('value');
        }

        elseif(Input::get('name') == 'is_readonly'){
               $column_value = Input::get('value');
        }

       elseif(Input::get('name') == 'modules'){
               $mod_temp = array();
                    foreach (Input::get('value') AS $key => $value){
                        $mod_temp[] = "$value";
                    }
                $mod = implode(',', $mod_temp);
                $column_value = '{'. $mod . '}';
        }

       else {
                $column_value = !empty(Input::get('value')) ? Input::get('value'): '';
        }
        
            DB::connection()->enableQueryLog();
            $test = ModuleMap::select()
                ->where('winid', '=', $winid)
                ->update([$column_name => $column_value]);
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'admin');
            return response()->json([ 'code'=>200], 200);
    }

     public function storemodule ()
    {
            $rules = array(
            'winid'       => 'required|unique:pgsql.app_toolbox.users,winid',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('admin')
                ->withErrors($validator)
                ->withInput(Input::except('admin'));
        } else {

            DB::connection()->enableQueryLog();
            $moduleuser = new ModuleMap;
            $moduleuser->winid       = Input::get('winid');
                $mod_temp = array();
                    foreach (Input::get('modules') AS $key => $value){
                        $mod_temp[] = "$value";
                    }
                $mod = implode(',', $mod_temp);
            $moduleuser->modules       = '{'. $mod . '}';
            $moduleuser->is_admin       = Input::get('is_admin');
            $moduleuser->is_readonly       = Input::get('is_readonly');
            $moduleuser->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'admin');
            Session::flash('message', 'Record was Successfully Saved!');
            return Redirect::to('admin');
        }
    }

     public function storequery ()
    {
            $rules = array(
            'user_name'       => 'required',
            'report_guid'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('admin')
                ->withErrors($validator)
                ->withInput(Input::except('admin'));
        } else {
            DB::connection()->enableQueryLog();
            $queryuser = new WebReportUsers;
            $queryuser->user_name       = Input::get('user_name');
            $queryuser->report_guid       = Input::get('report_guid');
            $queryuser->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'admin');
            Session::flash('message', 'Record was Successfully Saved!');
            return Redirect::to('admin');
        }
    }

    public function storeuseraccount ()
    {
            $rules = array(
            'user_name'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('admin')
                ->withErrors($validator)
                ->withInput(Input::except('admin'));
        } else {

            DB::connection()->enableQueryLog();
            $mapuser = new AccountMap;
            $mapuser->user_name       = Input::get('user_name');
            $mapuser->client       = Input::get('client');
            $mapuser->user_created_mode       = 1;
            $mapuser->save();
            $queries = DB::getQueryLog();
            $this->track->insert(Session::get('toolbox_s3_id'), $queries, 'admin');
            Session::flash('message', 'Record was Successfully Saved!');
            return Redirect::to('admin');
        }
    }

       public function querysearch()
    {
        
        $model = new QueryTracker;
        $table = $model->getTable();
        $primaryKey = 'winid';

        $columns = array(
            array( 'db' => 'winid', 'dt' => 0 ),
            array( 'db' => 'query',  'dt' => 1 ),
            array( 'db' => 'bindings',  'dt' => 2 ),
            array( 'db' => 'which_page',   'dt' => 3 ),
            array( 'db' => 'time_stamp',     'dt' => 4 )
        );

        $sql_details = array(
            'user' => 'svc.webtools',
            'pass' => 'str0ng3rb3113rf@5t3r',
            'db'   => 'cc_reporting',
            'host' => '10.235.230.63'
        );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }

       public function accountsearch()
    {
        
        $model = new AccountMapDash;
        $table = $model->getTable();
        $primaryKey = 'user_name';

        $columns = array(
            array( 'db' => 'user_name', 'dt' => 0 ),
            array( 'db' => 'client',  'dt' => 1 ),
            array( 'db' => 'name',  'dt' => 2 )
        );

        $sql_details = array(
            'user' => 'svc.webtools',
            'pass' => 'str0ng3rb3113rf@5t3r',
            'db'   => 'cc_reporting',
            'host' => '10.235.230.63'
        );

        echo json_encode(
            SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
        );
    }

}