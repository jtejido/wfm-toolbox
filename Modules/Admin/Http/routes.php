<?php

Route::group(['middleware' => ['web', 'auth', 'module:admin'], 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{

			    Route::post('admin/update/{winid}', ['as' => 'admin/update', 'uses' => 'AdminController@update',]);
			    Route::post('admin/storemodule', 'AdminController@storemodule');
			    Route::post('admin/storequery', 'AdminController@storequery');
			    Route::post('admin/storeuseraccount', 'AdminController@storeuseraccount');

		Route::get('admin/querysearch', ['uses' => 'AdminController@querysearch']);
	    Route::get('admin/accountsearch', ['uses' => 'AdminController@accountsearch']);
	    Route::get('admin', 'AdminController@index');

});
