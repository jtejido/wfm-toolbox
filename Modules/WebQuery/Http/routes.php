<?php

Route::group(['middleware' => ['web', 'auth', 'module:web_query'], 'namespace' => 'Modules\WebQuery\Http\Controllers'], function()
{

                Route::get('web_query/{report_guid}/run', 'WebQueryController@run');
                Route::resource('web_query', 'WebQueryController');


});
