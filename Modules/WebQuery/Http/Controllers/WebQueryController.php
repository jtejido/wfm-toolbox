<?php
namespace Modules\WebQuery\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use \App\ActivityLog;
use \App\Models\Admin\WebReportUsers;
use \App\Models\Admin\WebReport;
use View;
use Redirect;
use Session;
use DB;
use Illuminate\Support\Facades\Input;
use PDO;


class WebQueryController extends Controller
{

    public function index()
    {

        if((Session::get('is_admin')) < 1) {
               $web_query = WebReport::select('title', 'descr', 'report_guid')->whereIn('report_guid', function($query){
                    $query->select('report_guid')
                    ->from(with(new WebReportUsers)->getTable())
                    ->where('user_name', '=', Session::get('toolbox_s3_id'));
                })->get();
        }
        else {
                $web_query = WebReport::select('title', 'descr', 'report_guid')->get();
        }


        // load the view and pass the reports
       return view('webquery::web_query')
            ->with('web_query', $web_query);

            

    }

    public function run($report_guid)
    {

        if((Session::get('is_admin')) < 1) {
           $web_query = WebReportUsers::where('report_guid', $report_guid)->where('user_name', '=', Session::get('toolbox_s3_id'))->first();

            if(empty($web_query)) {

                return Redirect::to('403');
            }


        }

                $stmt = DB::select(DB::raw("select query, title, descr from app_reports.web_report where report_guid = :report_guid"), array('report_guid' => $report_guid));
                foreach ($stmt as $key => $value){
                $query = $value->query;
                $title = $value->title;
                $descr = $value->descr;
                 }
                if (empty($query)){
                Session::flash('message', 'Not a Valid Report.');
                return Redirect::to('web_query');
                }

                $stmt = DB::select(DB::raw($query));
                if (empty($stmt)){
                Session::flash('message', 'No Data available.');
                return Redirect::to('web_query');
                }


                $table = '<table class="table-bordered table-condensed" id="sortable"><thead>';
                foreach($stmt as $key => $value) {
                    foreach($value as $key => $value) {
                        $table .= "<th>".$key."</th>";
                    }
                    break;
                }
                $table .= "</thead><tbody>";
                foreach($stmt as $row) {
                    $table .= "<tr>";
                    foreach ($row as $column) {
                        $table .= "<td>".$column."</td>";
                    }
                    $table .= "</tr>";
                }
                $table .= "</tbody></table>";

                return view('webquery::web_query.query')
                ->with('table', $table)->with('title', $title)->with('descr', $descr);

        }
    

}