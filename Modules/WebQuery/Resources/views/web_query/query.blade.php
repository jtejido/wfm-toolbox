@extends('webquery::layouts.bare')
@section('content')
<div class="hero-unit">
<h4><i class="fa fa-clipboard" aria-hidden="true"></i> {!!$descr!!}</h4><br>
<button type="button" class="btn btn-default btn-md" data-toggle="modal" data-target="#timer">Set Refresh Rate</button>
</div>
<div class="container-fluid">


<div class="modal fade" id="timer">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3>Set Page Refresh Frequency</h3>
  </div>
  <div class="modal-body">
  <div class="container-fluid">
    <select class="form-control" id="selRe" onchange="setRe(this.options[selectedIndex].value)">
    <option value="0" selected>No Refresh</option>
    <option value="60">1 Minute</option>
    <option value="300">5 Minutes</option>
    <option value="600">10 Minutes</option>
    </select>
    <br>
    <button type="button" class="btn btn-primary btn-md" data-dismiss="modal">Save</button>
  </div>
  </div>
</div>
</div>
</div>

<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>{!!$title!!}</h5><button type="button" class="buttons btn btn-default btn-circle btn-sm fa fa-refresh" onclick="window.location.reload();">
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
      {!!$table!!}
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
      dom: "<'row'<'col-sm-3'lB><'col-sm-6'><'col-sm-3'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        buttons: [
            'copy', 'excel', 'print'
        ],
        "stateSave": true,
        "scrollX": true,
        "language": {
            "search": "Filter:",
             "emptyTable": "No Reports Available"
          },
         select: true
} );
});

function createCookie(name,value,days)
{
if (days)
{
var date = new Date();
date.setTime(date.getTime()+(days*24*60*60*1000));
var expires = "; expires="+date.toGMTString();
}
else var expires = "";
document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name)
{
var nameEQ = name + "=";
var ca = document.cookie.split(';');
for(var i=0;i < ca.length;i++)
{
var c = ca[i];
while (c.charAt(0)==' ') c = c.substring(1,c.length);
if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
}
return null;
}

function eraseCookie(name)
{
createCookie(name,"",-1);
}


var reIt

function doit(){
if (window.location.reload)
window.location.reload( true );
else if (window.location.replace)
window.location.replace(unescape(location.href))
else
window.location.href=unescape(location.href)
}

function startUp(){
if (readCookie('resetInt')!=null){
var opts=document.getElementById('selRe').options
for (var i_tem = 0; i_tem < opts.length; i_tem++)
if (opts[i_tem].value==readCookie('resetInt')/1000)
opts.selectedIndex=i_tem
reIt=setTimeout("doit()", readCookie('resetInt'))
}
else
return;
}

function setRe(val){
clearTimeout(reIt)
if (val==0){
eraseCookie('resetInt')
return;
}
else
createCookie('resetInt', val*1000, 7)
startUp();
}

onload=startUp;

</script>
@endsection