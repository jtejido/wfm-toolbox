@extends('webquery::layouts.app')
@section('content')
<div class="hero-unit">
    <h3><i class="fa fa-clipboard" aria-hidden="true"></i> Web Query</h3>
</div>
@if (session('message'))
    <div class="alert alert-warning">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! session('message') !!}
    </div>
@elseif (count($errors) > 0)
<div class="alert alert-warning">
<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
<ul>
@foreach ($errors->all() as $error)
<li>{!! $error !!}</li>
@endforeach
</ul>
</div>
@endif
<div class="container-fluid">
<div class="widget-box">
    <div class="widget-title bg_lg"><span class="icon"><i class="fa2 fa fa-table"></i></span>
      <h5>Web Reports</h5><button type="button" class="buttons btn btn-default btn-circle btn-sm fa fa-refresh" onclick="window.location.reload();">
    </div>
    <div class="widget-content" >
    <div class="row-fluid" >
      <table class="table-bordered table-condensed" id="sortable">
        <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($web_query as $key => $value)
            <tr>
                <td>{{ $value->title }}</td>
                <td>{{ $value->descr }}</td>
                <td>
                <a class="btn btn-primary btn-md" href="{{ URL::to('web_query/'.$value->report_guid.'/run') }}" target="_blank"> Run</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
    </div>
  </div>

</div>
@endsection
@section('js')
<script>
$(document).ready(function(){
    $('#sortable').DataTable( {
        "stateSave": true,
        "language": {
            "emptyTable": "No Data Available",
            "search": "Filter:"
        }
} );
});

</script>
@endsection