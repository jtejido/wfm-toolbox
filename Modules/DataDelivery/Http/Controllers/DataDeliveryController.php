<?php
namespace Modules\DataDelivery\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use View;
use Session;
USE DB;
use Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class DataDeliveryController extends Controller
{


    public function index()
    {

        
        return view('datadelivery::data_delivery');


    }


    public function upload()
    {
        $csv_array = [
        1 => 'ChatProductivity.csv',
        2 => 'CasesProductivity.csv',
        3 => 'CSatData.csv',
        4 => 'htc_agt_ptype_sla.csv',
        5 => 'htc_chat_daily.csv',
        6 => 'htc_qms_ivr.csv',
        7 => 'htc_device_map.csv',
        8 => 'htc_dsat_daily.csv',
        9 => 'htc_email_tickets_lang_daily.csv',
        10 => 'htc_email_tickets_lang_weekly.csv',
        11 => 'htc_email_replies_daily.csv',
        12 => 'htc_email_sla_monthly.csv',
        13 => 'htc_email_sla_weekly.csv',
        14 => 'htc_email_sla_daily.csv',
        15 => 'htc_csat_daily_qms_2014v.csv',
        16 => 'google_yt_commerce_dashboard.csv',
        17 => 'google_yt_cpo.csv',
        18 => 'google_yt_productivity.csv',
        19 => 'windstream_csat.csv',
        20 => 'groupon_ftr.csv',
        21 => 'google_tier_2_quality_form.csv',
        22 => 'google_qms.csv',
    ];
    $load_array = [
    1 => "select meta_dbmgmt.ld_csv('ChatProductivity.csv', 'EMEA/source_web_upload/ChatProductivity.csv','date'); ",
    2 => "select meta_dbmgmt.ld_csv('CasesProductivity.csv', 'EMEA/source_web_upload/CasesProductivity.csv');",
    3 => "select meta_dbmgmt.ld_csv('CSatData.csv', 'EMEA/source_web_upload/CSatData.csv','case_ids');",
    4 => "select meta_dbmgmt.ld_csv('htc_agt_ptype_sla.csv','EMEA/source_web_upload/htc_agt_ptype_sla.csv', 'ticketnumber');",
    5 => "select meta_dbmgmt.ld_csv('htc_chat_daily.csv','EMEA/source_web_upload/htc_chat_daily.csv', 'customer_id || entered_queue');",
    6 => "select meta_dbmgmt.ld_csv_noheader('source_htc_csv.htc_qms_ivr','EMEA/source_web_upload/htc_qms_ivr.csv', 'survey_no');",
    7 => "select meta_dbmgmt.ld_csv('htc_device_map.csv','EMEA/source_web_upload/htc_device_map.csv', 'e2e_device_or_htc_model');",
    8 => "select meta_dbmgmt.ld_csv_noheader('source_htc_csv.htc_dsat_daily','EMEA/source_web_upload/htc_dsat_daily.csv', 'case_no'); ",
    9 => "select meta_dbmgmt.ld_csv('htc_email_tickets_lang_daily.csv','EMEA/source_web_upload/htc_email_tickets_lang_daily.csv', 'site || language || dte');",
    10 => "select meta_dbmgmt.ld_csv('htc_email_tickets_lang_weekly.csv','EMEA/source_web_upload/htc_email_tickets_lang_weekly.csv', 'site || language || callcenter || dte');",
    11 => "select meta_dbmgmt.ld_csv('htc_email_replies_daily.csv','EMEA/source_web_upload/htc_email_replies_daily.csv', 'site || callcenter || agent || date');",
    12 => "select meta_dbmgmt.ld_csv('htc_email_sla_monthly.csv','EMEA/source_web_upload/htc_email_sla_monthly.csv', 'site || callcenter || month || dte');",
    13 => "select meta_dbmgmt.ld_csv('htc_email_sla_weekly.csv','EMEA/source_web_upload/htc_email_sla_weekly.csv', 'site || callcenter || week || dte');",
    14 => "select meta_dbmgmt.ld_csv('htc_email_sla_daily.csv','EMEA/source_web_upload/htc_email_sla_daily.csv', 'site || callcenter || dte');",
    15 => "select meta_dbmgmt.ld_csv_noheader('source_htc_csv.htc_csat_daily_qms_2014v','EMEA/source_web_upload/htc_csat_daily_qms_2014v.csv', 'survey_no'); ",
    16 => "select meta_dbmgmt.ld_csv_trunc('google_yt_commerce_dashboard.csv','EMEA/source_web_upload/google_yt_commerce_dashboard.csv'); ",
    17 => "select meta_dbmgmt.ld_csv_trunc('google_yt_cpo.csv','EMEA/source_web_upload/google_yt_cpo.csv'); ",
    18 => "select meta_dbmgmt.ld_csv_trunc('google_yt_productivity.csv','EMEA/source_web_upload/google_yt_productivity.csv'); ",
    19 => "select source_windstream_csv.ld_csat();",
    20 => "select meta_dbmgmt.ld_csv('groupon_ftr.csv', 'EMEA/source_web_upload/groupon_ftr.csv'); ",
    21 => "select meta_dbmgmt.ld_csv('google_tier_2_quality_form.csv', 'EMEA/source_google_csv/google_tier_2_quality_form.csv'); ",
    22 => "select meta_dbmgmt.ld_csv('google_qms.csv','EMEA/source_web_upload/google_qms.csv', 'timestamp||auditor_ldap');",
    ];

    $directory = '/dd/';
    $db = DB::connection('sqlite');
    $db->statement('CREATE TABLE IF NOT EXISTS upload_log (user_id STRING, file_name STRING, upload_ts DATETIME DEFAULT CURRENT_TIMESTAMP)');
    $statement = $db->insert('INSERT INTO upload_log (user_id, file_name) VALUES (:user, :file);', array('user' => Session::get('toolbox_s3_id'), 'file' => $csv_array[Input::get('csvtype')]));

    $target_dir = storage_path('app').$directory;
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir. $csv_array[$_POST['csvtype']])) {
         $message = "Sorry, there was an error uploading your file.";
    } else {
        $message = "Sorry, there was an error uploading your file.";
    }

    if ($_POST['csvtype'] == 19){
        $db = DB::connection('pgsql_external');
    }
    else {
        $db = DB::connection('pgsql');
    }
        $stmt = $db->select($load_array[$_POST['csvtype']]);
        $message = "The file ". basename($_FILES["fileToUpload"]["name"]). " was imported Successfully.";
         Session::flash('message', $message);
         return Redirect::to('data_delivery');

    }

}