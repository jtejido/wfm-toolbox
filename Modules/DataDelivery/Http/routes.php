<?php

Route::group(['middleware' => ['web', 'auth', 'module:data_delivery'], 'namespace' => 'Modules\DataDelivery\Http\Controllers'], function()
{

		    Route::post('data_delivery/upload', ['uses' => 'DataDeliveryController@upload']);
		    Route::get('data_delivery', 'DataDeliveryController@index');


});
