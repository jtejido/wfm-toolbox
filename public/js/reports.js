$(document).on('click', '[type="submit"]', function() {
    var date_recip = document.getElementById("date_recip").value;
    if (date_recip.match(/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/) || date_recip == null || date_recip == "") {
        return true;
    } else {
        alertify.alert("Invalid Date format for Recipient's Start/End Date. (mm/dd/yyyy)");
        return false;
    }
});

$(document).on('click', '[type="submit"]', function() {
    var date = document.getElementById("expire_time").value;
    if (date.match(/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/) || date == null || date == "") {
        return true;
    } else {
        alertify.alert("Invalid Expire Time format: Expire Time should be in HH:MM:SS format.");
        return false;
    }
});


$(document).ready(function(){
    if($('select[name=client]').val() == ""){
        $('input[name=client_not_listed]').show();
    }
});

$('select[name=client]').on('change', function(){
    if($(this).val() == ""){
        $('input[name=client_not_listed]').show();
    }else {
        $('input[name=client_not_listed]').hide().val('');
    }
});

$('select[name=client]').on('change', function(){
    var input = document.getElementById("name");
    if($(this).val() == "Template Report"){
        $('select[name=updatetemplate]').show();
        $('input[name=name]').hide().val(input.value);
    }else {
        $('select[name=updatetemplate]').hide().val(' ');
        $('input[name=name]').show().val(input.value);
    }
});

$('select[name=client]').on('change', function(){
    if($(this).val() == "Template Report"){
        $("#template_yes").show();
        $("#template_no").hide();
    }else {
        $("#template_yes").hide();
        $("#template_no").show();
    }
});


