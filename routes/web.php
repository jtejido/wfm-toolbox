<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'LDAPAuthController@index');
Route::get('help', 'ErrorController@maintenance');
Route::post('login', 'LDAPAuthController@login');
Route::get('logout', 'LDAPAuthController@logout');
Route::get('register', 'RegisterEmailController@index');
Route::post('register/submit', 'RegisterEmailController@submit');
Route::get('503', 'ErrorController@maintenance');
Route::get('403', 'ErrorController@unauthorized');

