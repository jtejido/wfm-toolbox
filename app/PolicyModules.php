<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyModules extends Model
{

  protected $table = 'app_toolbox.policy_modules';
  protected $primaryKey = 'policy_id';

/*
  protected static function boot() {
    parent::boot();

    static::deleted(function ($processes) {
        $processes->processes()->delete();
    });
  }
*/
  
  public function usermodules()
  {
      return $this->hasMany('App\Modules', 'policy_id');
  }

    public function userpolicy()
  {
      return $this->hasMany('App\Policy', 'policy_id');
  }

  public function user()
  {
      return $this->belongsTo('App\User', 'winid');
  }

}
