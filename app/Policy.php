<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{

  protected $table = 'app_toolbox.policy';
  protected $primaryKey = 'policy_id';


  public function policymodules()
  {
      return $this->belongsTo('App\PolicyModules', 'policy_id');
  }

}
