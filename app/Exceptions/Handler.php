<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use App\Library\osTicket;
use Session;
use Request;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
      \Illuminate\Auth\AuthenticationException::class,
      \Illuminate\Auth\Access\AuthorizationException::class,
      \Symfony\Component\HttpKernel\Exception\HttpException::class,
      \Illuminate\Database\Eloquent\ModelNotFoundException::class,
      \Illuminate\Validation\ValidationException::class,
      \Illuminate\Session\TokenMismatchException::class,
      \Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        NotFoundHttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
      if($this->shouldReport($e)) {
        if(config('app.debug') == false) {
          $reportValues['user_id'] = Session::get('toolbox_s3_id');
          $reportValues['path'] = Request::url();
          $reportValues['env'] = env('APP_ENV');

          $sessionDetails = json_encode($reportValues);

          $ticket = new osTicket;
          $ticket->createTicket($e->getMessage().' in '.$e->getFile().' on line '.$e->getLine(), $sessionDetails.PHP_EOL.'===== START STACK TRACE ====='.PHP_EOL.$e->getTraceAsString());
          }
        }
        return parent::report($e);

    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }
        if ($e instanceof NotFoundHttpException) {
            return response()->view("errors.404", [], 404);
        }
        if ($e instanceof ValidationException) {
          return parent::render($request, $e);
        }

        // Added a custom handler to handle Production errors not showing Laravel's default 500 message
        if(config('app.debug')) {
          return parent::render($request, $e);
        }
        return response()->view("errors.500", [], 500);
    }
}
