<?php

namespace App\DataTables\Common;

use \App\Models\Common\RequestReports;
use Yajra\Datatables\Services\DataTable;
use Session;

class RequestReportDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('action', function ($report) {
                return '<a class="btn btn-success btn-md fa fa-plus" href="/tools/request_report/'.$report->rid.'/'.Session::get('recipient_data_toolbox').'/add"> Subscribe&nbsp;</a>';
            })->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {


        $collection = RequestReports::select(['rid', 'name', 'client'])->where('name', 'ilike', '%'.Session::get('report_name').'%')
        ->where(function($query)
            {
                foreach (Session::get('toolbox_accounts') as $key=>$value){
                $query->orWhere('client', 'ilike', "%$value%");
                }
            });
        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'rid', 'name' => 'rid', 'title' => 'RID'])
              ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
              ->addColumn(['data' => 'client', 'name' => 'client', 'title' => 'Client'])
              ->addColumn(['data' => 'action', 'name' => 'Action', 'title' => 'Action', 'orderable' => false, 'searchable' => false])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            return [
                 'stateSave' => true,
                 'language' => [
                                    'search' => 'Filter:'
                                ],

            ];
        }
}
