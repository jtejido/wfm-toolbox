<?php

namespace App\DataTables\Reports;

use App\Models\Reports\TemplateConfigDash;
use Yajra\Datatables\Services\DataTable;

class TemplateConfigDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('db_object', function ($template_config) {
                return '<a href="#" class="db_object" data-type="text" data-column="db_object" data-url="/tools/template_config/update/'.$template_config->template_name.'/'.$template_config->connection_id.'" data-pk="'.$template_config->template_name.'" data-title="change" data-name="db_object">'.$template_config->db_object.'</a>';
                })
            ->addColumn('use_where', function ($template_config) {
                return '<a href="#" class="use_where" data-column="use_where" data-url="/tools/template_config/update/'.$template_config->template_name.'/'.$template_config->connection_id.'" data-pk="'.$template_config->template_name.'" data-title="change" data-name="use_where" data-value="'.$template_config->use_where.'">'.$template_config->use_where.'</a>';
                })
            ->addColumn('action', function ($template_config) {
                    return 
                    '
                        <form method="POST" action="/tools/template_config/'.$template_config->template_name.'/'.$template_config->connection_id.'/delete">
                        '.csrf_field().'               
                        <button class="btn-primary btn-lg fa fa-trash" style="border: none;"> Delete</button>
                        <input name="_method" type="hidden" value="delete">
                        </form>
                    ';
            })
            ->rawColumns(['db_object', 'use_where', 'action'])
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {


        $collection = TemplateConfigDash::select(['template_name', 'connection_id', 'db_object', 'use_where']);

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'template_name', 'name' => 'template_name', 'title' => 'Template Name'])
              ->addColumn(['data' => 'connection_id', 'name' => 'connection_id', 'title' => 'Connection ID'])
              ->addColumn(['data' => 'db_object', 'name' => 'db_object', 'title' => 'DB Object'])
              ->addColumn(['data' => 'use_where', 'name' => 'use_where', 'title' => 'Use Where'])
              ->addColumn(['data' => 'action', 'name' => 'Action', 'title' => 'Action', 'orderable' => false, 'searchable' => false])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            return [
                 'stateSave' => true,
                 'dom'          => "<'row'<'col-sm-2'l><'col-sm-2'B><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                 'buttons'      => ['reload'],
                 'language' => [
                                    'search' => 'Filter:'
                                ],

            ];
        }
}
