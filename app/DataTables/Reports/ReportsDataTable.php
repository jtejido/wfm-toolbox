<?php

namespace App\DataTables\Reports;

use App\Models\Reports\ReportsDash;
use Yajra\Datatables\Services\DataTable;
use DB;

class ReportsDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('action', function ($report) {
                return '<ul class="list-inline"><li><a class="btn btn-primary btn-md fa fa-pencil-square-o" title="Edit" href="/tools/reports/'.$report->rid.'/edit"></a></li><li> <a class="btn btn-primary btn-md fa fa-play" title="Run" href="/tools/reports/'.$report->rid.'/run"></a></li><li><button type="button" class="copyreport btn btn-primary btn-md fa fa-clipboard" title="Copy" data-id="'.$report->rid.'" data-toggle="modal" data-target="#copyreport"></button></li></ul>';
            })->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {


        $collection = ReportsDash::select(['rid', 'name', 'internal_external','client', 'report_status', 'status']);

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'rid', 'name' => 'rid', 'title' => 'RID'])
              ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
              ->addColumn(['data' => 'internal_external', 'data-class' => 'select-filter-int-ext', 'name' => 'internal_external', 'title' => 'Internal/External'])
              ->addColumn(['data' => 'client', 'data-class' => 'select-filter-client', 'name' => 'client', 'title' => 'Client'])
              ->addColumn(['data' => 'report_status', 'data-class' => 'select-filter-status', 'name' => 'report_status', 'title' => 'Report Status'])
              ->addColumn(['data' => 'status', 'name' => 'status', 'title' => 'Status'])
              ->addColumn(['data' => 'action', 'name' => 'Action', 'title' => 'Action', 'orderable' => false, 'searchable' => false])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            $report_clients = ReportsDash::select('client')->where('client', '!=', '')->distinct('client')->orderBy('client')->get();
            foreach($report_clients as $report_client){
            $clients_temp[] = $report_client->client;
            }
            $clients = json_encode($clients_temp);

            $report_status = ReportsDash::select('report_status')->distinct('report_status')->orderBy('report_status')->get();
            foreach($report_status as $status){
            $report_status_temp[] = $status->report_status;
            }
            $status_array = json_encode($report_status_temp);
            
            return [
                 'responsive' => true,
                 'statesave' => true,
                 'dom'          => "<'row'<'col-sm-2'l><'col-sm-3'B><'col-sm-7'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                 'buttons'      => ['reset','reload'],
                 'language' => [
                                    'search' => 'Filter:'
                                ],
                 'initComplete' => "function () {
                            this.api().columns('.select-filter-client').every(function () {
                                var column = this;
                                var client_array = ". $clients . ";
                                var select = $('<select class=\"form-control\"><option value=\"\"></option></select>')
                                    .appendTo( $(column.footer()).empty() )
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );
                 
                                        column
                                            .search( val ? '^'+val+'$' : '', true, false )
                                            .draw();
                                    } );
                                    $.each(client_array, function(key, value) { 
                                    select.append( '<option value=\"'+value+'\">'+value+'</option>' );
                                });

                            } );
                            this.api().columns('.select-filter-int-ext').every(function () {
                                var column = this;
                                var select = $('<select class=\"form-control\"><option value=\"\"></option></select>')
                                    .appendTo( $(column.footer()).empty() )
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );
                 
                                        column
                                            .search( val ? '^'+val+'$' : '', true, false )
                                            .draw();
                                    } );

                                    select.append( '<option value=\"Internal\">Internal</option>' );
                                    select.append( '<option value=\"External\">External</option>' );

                            });
                            this.api().columns('.select-filter-status').every(function () {
                                var column = this;
                                var status_array = ". $status_array . ";
                                var select = $('<select class=\"form-control\"><option value=\"\"></option></select>')
                                    .appendTo( $(column.footer()).empty() )
                                    .on( 'change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );
                 
                                        column
                                            .search( val ? '^'+val+'$' : '', true, false )
                                            .draw();
                                    } );
                                    $.each(status_array, function(key, value) { 
                                    select.append( '<option value=\"'+value+'\">'+value+'</option>' );
                                });

                            });
                        }",
            ];
        }
}
