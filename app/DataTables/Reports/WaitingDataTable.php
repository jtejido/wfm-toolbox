<?php

namespace App\DataTables\Reports;

use App\Models\Reports\MWaiting;
use Yajra\Datatables\Services\DataTable;
use Yajra\Datatables\Datatables;

class WaitingDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {


        $collection = MWaiting::select(['rid', 'scheduled_time', 'sla_time']);

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'rid', 'name' => 'rid', 'title' => 'RID'])
              ->addColumn(['data' => 'scheduled_time', 'name' => 'scheduled_time', 'title' => 'Scheduled Time'])
              ->addColumn(['data' => 'sla_time', 'name' => 'sla_time', 'title' => 'SLA Time'])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            return [
                 'stateSave' => true,
                 'dom'          => "<'row'<'col-sm-2'l><'col-sm-2'B><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                 'buttons'      => ['reload'],
                 'language' => [
                                    'search' => 'Filter:'
                                ],
            ];
        }
}
