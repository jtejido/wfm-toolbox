<?php

namespace App\DataTables\Reports;

use \App\Models\Reports\RRun;
use Yajra\Datatables\Services\DataTable;

class RunReportsDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {


        $collection = RRun::select(['rid', 'report_pc', 'pc_user','client', 'report_name']);

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'rid', 'name' => 'rid', 'title' => 'RID'])
              ->addColumn(['data' => 'report_pc', 'name' => 'report_pc', 'title' => 'Report PC'])
              ->addColumn(['data' => 'pc_user', 'name' => 'pc_user', 'title' => 'PC User'])
              ->addColumn(['data' => 'client', 'name' => 'client', 'title' => 'Client'])
              ->addColumn(['data' => 'report_name', 'name' => 'report_name', 'title' => 'Report Name'])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            return [
                 'stateSave' => true,
                 'language' => [
                                    'search' => 'Filter:',
                                    'emptyTable' => 'No Data Available',
                                ],
            ];
        }
}
