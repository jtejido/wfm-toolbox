<?php

namespace App\DataTables\Reports;

use \App\Models\Reports\AssignedReports;
use Yajra\Datatables\Services\DataTable;
use Session;

class AssignedReportsDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('action', function ($report) {
                return '<a class="btn btn-primary btn-md fa fa-edit" title="Edit" href="/tools/reports/'.$report->rid.'/edit"> Edit</a>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {

        $collection = AssignedReports::select(['rid', 'name', 'client'])->where('recipient_id', '=', Session::get('recipient_id'));

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'rid', 'name' => 'rid', 'title' => 'RID'])
              ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
              ->addColumn(['data' => 'client', 'data-class' => 'select-filter', 'name' => 'client', 'title' => 'Client'])
              ->addColumn(['data' => 'action', 'name' => 'Action', 'title' => 'Action', 'orderable' => false, 'searchable' => false])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            return [
                 'stateSave' => true,
                 'dom'          => "<'row'<'col-sm-2'l><'col-sm-3'B><'col-sm-7'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                 'buttons'      => ['reload'],
                 'language' => [
                                    'search' => 'Filter:',
                                    'emptyTable' => 'No Reports Subscribed',
                                ],
            ];
        }
}
