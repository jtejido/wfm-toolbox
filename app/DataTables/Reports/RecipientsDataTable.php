<?php

namespace App\DataTables\Reports;

use \App\Models\Reports\Recipients;
use Yajra\Datatables\Services\DataTable;

class RecipientsDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('email_address', function ($recipients) {
                return '<a href="#" class="email_address" data-type="text" data-column="email_address" data-url="/tools/recipients/update/'.$recipients->recipient_id.'" data-pk="'.$recipients->recipient_id.'" data-title="change" data-name="email_address">'.$recipients->email_address.'</a>';
                })
            ->addColumn('name', function ($recipients) {
                return '<a href="#" class="name" data-type="text" data-column="name" data-url="/tools/recipients/update/'.$recipients->recipient_id.'" data-pk="'.$recipients->recipient_id.'" data-title="change" data-name="name">'.$recipients->name.'</a>';
                })
            ->addColumn('internal_external', function ($recipients) {
                return '<a href="#" class="internal_external" data-column="internal_external" data-url="/tools/recipients/update/'.$recipients->recipient_id.'" data-pk="'.$recipients->recipient_id.'" data-title="change" data-name="internal_external" data-value="'.$recipients->internal_external.'">'.$recipients->internal_external.'</a>';
                })
            ->addColumn('company', function ($recipients) {
                return '<a href="#" class="company" data-type="text" data-column="company" data-url="/tools/recipients/update/'.$recipients->recipient_id.'" data-pk="'.$recipients->recipient_id.'" data-title="change" data-name="company">'.$recipients->company.'</a>';
                })
            ->addColumn('s3_id', function ($recipients) {
                return '<a href="#" class="s3_id" data-type="text" data-column="s3_id" data-url="/tools/update/'.$recipients->recipient_id.'" data-pk="'.$recipients->recipient_id.'" data-title="change" data-name="s3_id">'.$recipients->s3_id.'</a>';
                })
            ->addColumn('action', function ($recipients) {
                return '<a class="btn btn-primary btn-md" href="/tools/recipients/'.$recipients->recipient_id.'/edit"> See Reports</a>';
                })
            ->rawColumns(['email_address','name','internal_external','company','s3_id', 'action'])
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {


        $collection = Recipients::select(['recipient_id', 'email_address', 'name','internal_external', 'company', 's3_id']);

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'recipient_id', 'name' => 'recipient_id', 'title' => 'Recipient ID'])
              ->addColumn(['data' => 'email_address', 'name' => 'email_address', 'title' => 'Email Address'])
              ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Name'])
              ->addColumn(['data' => 'internal_external', 'name' => 'internal_external', 'title' => 'Internal/External'])
              ->addColumn(['data' => 'company', 'name' => 'company', 'title' => 'Company'])
              ->addColumn(['data' => 's3_id', 'name' => 's3_id', 'title' => 'S3 ID'])
              ->addColumn(['data' => 'action', 'name' => 'Action', 'title' => 'Action', 'orderable' => false, 'searchable' => false])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            return [
                 'stateSave' => true,
                 'language' => [
                                    'search' => 'Filter:',
                                    'emptyTable' => 'No Data Available',
                                ],
            ];
        }
}

 