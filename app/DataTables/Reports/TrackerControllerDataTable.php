<?php

namespace App\DataTables\Reports;

use App\Models\Reports\Tracker;
use Yajra\Datatables\Services\DataTable;

class TrackerControllerDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {


        $collection = Tracker::select(['rid', 'report_name', 'scheduled_time','sla_time', 'next_scheduled_time', 'result', 'report_open_time']);

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'rid', 'name' => 'rid', 'title' => 'RID'])
              ->addColumn(['data' => 'report_name', 'name' => 'report_name', 'title' => 'Report Name'])
              ->addColumn(['data' => 'scheduled_time', 'name' => 'scheduled_time', 'title' => 'Scheduled Time'])
              ->addColumn(['data' => 'sla_time', 'name' => 'sla_time', 'title' => 'SLA Time'])
              ->addColumn(['data' => 'next_scheduled_time', 'name' => 'next_scheduled_time', 'title' => 'Next Scheduled Time'])
              ->addColumn(['data' => 'result', 'name' => 'result', 'title' => 'Result'])
              ->addColumn(['data' => 'report_open_time', 'name' => 'report_open_time', 'title' => 'Report Open Time'])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            return [
                 'stateSave' => true,
                 'dom'          => "<'row'<'col-sm-2'l><'col-sm-2'B><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                 'buttons'      => ['reload'],
                 'language' => [
                                    'search' => 'Filter:'
                                ],
            ];
        }
}
