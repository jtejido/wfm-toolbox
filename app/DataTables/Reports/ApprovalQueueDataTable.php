<?php

namespace App\DataTables\Reports;
use DB;
use Session;
use App\Models\Reports\ApprovalQueue;
use Yajra\Datatables\Services\DataTable;

class ApprovalQueueDataTable extends DataTable
{


    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->addColumn('action', function ($report) {
                if ($report->request_type == "add"){
                    return 
                    '
                    <ul class="list-inline">
                    <li>
                        <a data-toggle="modal" data-request-id="'.$report->request_id.'" data-rid="'.$report->rid.'" data-recipient-id="'.$report->recipient_id.'" data-request-type="'.$report->request_type.'" data-target="#approve" class="btn-primary btn-lg fa fa-check approve" title="Approve" href="#"> Approve</a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-request-id="'.$report->request_id.'" data-rid="'.$report->rid.'" data-recipient-id="'.$report->recipient_id.'" data-request-type="'.$report->request_type.'" data-target="#reject" class="btn-primary btn-lg fa fa-trash reject" title="Reject" href="#">
                         Reject</a>
                     </li>
                    </ul>
                    ';
                }
                elseif ($report->request_type == "delete"){
                    return 
                    '
                    <ul class="list-inline">
                    <li>
                        <form method="POST" action="/tools/approval_queue/'.$report->request_id.'/'.$report->rid.'/'.$report->recipient_id.'/'.$report->request_type.'/approve">
                        '.csrf_field().'               
                        <button class="btn-primary btn-lg fa fa-check" style="border: none;"> Approve</button>
                        <input name="_method" type="hidden" value="get">
                        </form>
                    </li>
                    <li>
                        <a data-toggle="modal" data-request-id="'.$report->request_id.'" data-rid="'.$report->rid.'" data-recipient-id="'.$report->recipient_id.'" data-request-type="'.$report->request_type.'" data-target="#reject" class="btn-primary btn-lg fa fa-trash reject" title="Reject" href="#">
                         Reject</a>
                    </li>
                    </ul>
                    ';
                }
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {

        $employees = DB::select("select recipient_id from app_toolbox.get_reporting_recipient_employees(:win_id) where depth != 0", ['win_id' => Session::get('toolbox_s3_id')]);
        $collection = ApprovalQueue::select(['request_type', 'rid', 'name', 'recipient_id', 'recip_name', 'email_address', 'request_datetime', 'request_id']);
        if ((Session::get('is_approver') == '1') && (Session::get('is_admin') == '0')){
            foreach ($employees as $employee){
            $collection = $collection->orWhere('recipient_id', '=', $employee->recipient_id);
            }
        }

        return $collection;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
              ->addColumn(['data' => 'request_type', 'name' => 'request_type', 'title' => 'Request Type'])
              ->addColumn(['data' => 'rid', 'name' => 'rid', 'title' => 'RID'])
              ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Report Name'])
              ->addColumn(['data' => 'recipient_id', 'name' => 'recipient_id', 'title' => 'Recipient_ID'])
              ->addColumn(['data' => 'recip_name', 'name' => 'recip_name', 'title' => 'Recipient Name'])
              ->addColumn(['data' => 'email_address', 'name' => 'email_address', 'title' => 'Email Address'])
              ->addColumn(['data' => 'request_datetime', 'name' => 'request_datetime', 'title' => 'Request Submitted'])
              ->addColumn(['data' => 'request_id', 'name' => 'request_id', 'title' => 'Request ID'])
              ->addColumn(['data' => 'action', 'name' => 'Action', 'title' => 'Action', 'orderable' => false, 'searchable' => false])
              ->parameters($this->getBuilderParameters());

    }

    protected function getBuilderParameters()
        {
            return [
                 'stateSave' => true,
                 'dom'          => "<'row'<'col-sm-2'l><'col-sm-2'B><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                 'buttons'      => ['reload'],
                 'language' => [
                                    'search' => 'Filter:'
                                ],

            ];
        }
}
