<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{

  protected $table = 'app_toolbox.modules';
  protected $primaryKey = 'module_id';

  
  public function policymodules()
  {
      return $this->belongsTo('App\PolicyModules', 'policy_id');
  }

}
