<?php
namespace App\Http\Controllers;
use \App\Http\Controllers\Controller;
use View;
use Redirect;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\Models\Reports\Recipients;
use \App\User;
use \App\MSS;
use Illuminate\Support\Facades\Input;
use Session;

class RegisterEmailController extends Controller {



  public function index() {
     return View::make('register');
  }

  public function submit() {


  	$rules = array(
            'email_address'      => 'required',
            'internal_external'      => 'required',
            'company'      => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);


        if ($validator->fails()) {
            return Redirect::to('register')
                ->withErrors($validator);
        } else {
             $email = Input::get('email_address');
		     $check = Recipients::where('email_address', 'ilike', "%$email%")->first();
		     if (!empty($check)){
		     		$recipient = Recipients::find($check->recipient_id);
		            $recipient->email_address = $email;
		            $recipient->save();
		            Session::put('email_address_toolbox', $email);
		            Session::put('recipient_data_toolbox', $check->recipient_id);
		            return Redirect::to('subscribed_reports');
		     }
		     else {
		     		$name = MSS::select('full_name')->where('win_id', Session::get('toolbox_s3_id'))->first();
		     		$recipient = new Recipients;
		            $recipient->name       = $name->full_name;
		            $recipient->email_address = $email; 
		            $recipient->internal_external = Input::get('internal_external');
		            $recipient->company = Input::get('company');
		            $recipient->s3_id = Session::get('toolbox_s3_id');
		            $recipient->save();
		            return Redirect::to('subscribed_reports');
		     }
        }


  	 
  }





}
