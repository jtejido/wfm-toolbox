<?php
namespace App\Http\Controllers;
use \App\Http\Controllers\Controller;
use View;


class ErrorController extends Controller {



  public function maintenance() {
     return View::make('errors.503');
  }

  public function unauthorized() {
     return View::make('errors.403');
  }





}
