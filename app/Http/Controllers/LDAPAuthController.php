<?php
namespace App\Http\Controllers;
use \App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Redirect;
use Log;
use Auth;
use \App\User;
use \App\UserMap;
use \App\EmployeeMap;
use \App\ApproverMap;
use \App\Models\Reports\Reports;
use Session;
use Storage;
use View;
use Exception;


class LDAPAuthController extends Controller {



public function login() {




              $user = User::where('win_id', Input::get('username'))->first();

              //Store Email_address
              if (!empty($user)){
                Session::put('email_address_toolbox', $user->email_address);

                Session::put('recipient_data_toolbox', $user->recipient_id);
              }
              else {
                return Redirect::refresh()->with('error', 'No Account Found. Please contact Support.');
              }

              $is_approver = User::where('current_sup_win', '=', $user->win_id)->first();

              if(!empty($user->client)){
                $download_reports_dd = json_decode($user->client);
              }
              else {
                $download_reports_dd[] = '';
              }

              $modules_arr = json_decode($user->modules);


                Session::put('toolbox_s3_id', $user->win_id);
                Session::put('approver_id', $user->current_sup_win);

                if(!empty($modules_arr)){
                    if(($key = array_search('mss_search', $modules_arr)) !== false) {
                        unset($modules_arr[$key]);
                        array_unshift($modules_arr , 'mss_search');
                      }
                    if(($key = array_search('dbmgmt', $modules_arr)) !== false) {
                      unset($modules_arr[$key]);
                      array_unshift($modules_arr , 'dbmgmt');
                    }
                    if(($key = array_search('auto_reports', $modules_arr)) !== false) {
                      unset($modules_arr[$key]);
                      array_unshift($modules_arr , 'auto_reports');
                      
                      if ($user->is_readonly == "TRUE"){
                      Session::put('auto_reports_approver', 0);
                      }
                      else {
                      Session::put('auto_reports_approver', 1);
                      }
                      
                      Session::put('auto_reports_admin', 1);

                      $clientquery = Reports::distinct()->select('client')->orderBy('client', 'asc')->get();
                      foreach ($clientquery as $client){
                      $clientdd[] = $client->client;
                      }
                      $clientdd = array_filter($clientdd);
                      Session::put('clientdd', $clientdd);
                      $internal_externaldd = Reports::distinct()->select('internal_external')->orderBy('internal_external', 'asc')->get();
                      Session::put('internal_externaldd', $internal_externaldd);
                      $isdateappendeddd = Reports::distinct()->select('isdateappended')->orderBy('isdateappended', 'asc')->get();
                      Session::put('isdateappendeddd', $isdateappendeddd);
                      $first_day_of_weekdd = Reports::distinct()->select('first_day_of_week')->orderBy('first_day_of_week', 'asc')->get();
                      Session::put('first_day_of_weekdd', $first_day_of_weekdd);
                      $savewithmacrodd = Reports::distinct()->select('savewithmacro')->orderBy('savewithmacro', 'desc')->get();
                      Session::put('savewithmacrodd', $savewithmacrodd);
                      $deleteconnectionsdd = Reports::distinct()->select('deleteconnections')->orderBy('deleteconnections', 'asc')->get();
                      Session::put('deleteconnectionsdd', $deleteconnectionsdd);
                      $run_regiondd = Reports::distinct()->select('run_region')->orderBy('run_region', 'asc')->get();
                      Session::put('run_regiondd', $run_regiondd);
                      $report_statusdd = Reports::distinct()->select('report_status')->orderBy('report_status', 'asc')->get();
                      Session::put('report_statusdd', $report_statusdd);
                      $template_dd = DB::select(DB::raw("select distinct app_reports_mgmt.template_config.template_name from app_reports_mgmt.template_config order by app_reports_mgmt.template_config.template_name asc"));
                      Session::put('template_dd', $template_dd);

                    }

                    elseif(((array_search('auto_reports', $modules_arr)) !== true) && (!empty($is_approver))) {
                        array_unshift($modules_arr , 'auto_reports');
                        Session::put('auto_reports_approver', 1);
                        Session::put('auto_reports_admin', 0);
                    }


                    elseif(((array_search('auto_reports', $modules_arr)) !== true) && (empty($is_approver))) {
                        Session::put('auto_reports_approver', 0);
                        Session::put('auto_reports_admin', 0);
                    }


                    if(($key = array_search('elements', $modules_arr)) !== false) {
                      unset($modules_arr[$key]);
                      array_unshift($modules_arr , 'elements');
                    }

                  }



                Session::put('toolbox_modules', $modules_arr);

                if(!empty($download_reports_dd)){

                  Session::put('toolbox_accounts', array_filter($download_reports_dd));

                  $directory = '/test';

                  $scanned_directory = array_diff(Storage::directories($directory), array('..', '.'));
                        foreach($scanned_directory as $key=>$value){
                          $scanned_directory[$key]=str_replace("test/","",$value);
                        }
                  $accounts = array_diff(array_unique($scanned_directory),array('', '11', 'Daily'));
                  $accounts = array_intersect($accounts, Session::get('toolbox_accounts'));

                  Session::put('download_reports_available', $accounts);
                }
                


                if ($user->is_admin == "TRUE"){
                Session::put('is_admin', 1);
                }
                else {
                Session::put('is_admin', 0);
                }

                if ($user->is_readonly == "TRUE"){
                Session::put('is_readonly', 1);
                }
                else {
                Session::put('is_readonly', 0);
                }

                if (!empty($is_approver)){
                Session::put('is_approver', 1);
                }
                else {
                Session::put('is_approver', 0);
                }

                Session::put('imposter', 0);

              Auth::login($user);

              if (empty(Session::get('recipient_data_toolbox'))){
                return Redirect::to('register');
              }
              else {
                return redirect()->intended('/');
              }

     
  }

 


 public function logout() {
    Auth::logout();
    Session::flush();
    return Redirect::to('login')->with('message', 'You just logged out.');
 }

  public function index() {
    return View::make('auth.login');
  }

}
