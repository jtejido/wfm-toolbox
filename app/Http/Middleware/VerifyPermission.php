<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use App\Http\Requests;
use Closure;
use Session;
use View;

class VerifyPermission
{
    public function handle($request, Closure $next)
    {

      if (Session::get('is_readonly') == 1) {
          return redirect('403');
      }
        
        return $next($request);
    }

}