<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use App\Http\Requests;
use Closure;
use Session;
use View;
use App\Library\UserBadge;
use App\Library\Badge;

class VerifyModule
{
    public function handle($request, Closure $next, $module)
    {

    	if ($module == '503') {
			return redirect('503');
		}

		else {
		    if ($module != 'admin') {
		      
					if ($module != 'auto_reports') {
		      
			      		if (!in_array($module, Session::get('toolbox_modules'))) {
			         		return redirect('403');
			      				}

			   		 }

			   		 elseif ($module == 'auto_reports') {
		      			
		      			$path = $request->path();
			      		if ((Session::get('auto_reports_admin') != 1) && ($path != 'approval_queue')) {
						         return redirect('403');
						      	}
				      	elseif ((Session::get('is_approver') != 1) && (Session::get('auto_reports_approver') != 1) && ($path == 'approval_queue')) {
				         		return redirect('403');
				      			}

			   		 }
			        
			     }

			elseif ($module == 'admin') {
		      
			      if (Session::get('is_admin') != 1) {
			         return redirect('/');
			      	}

			    }
		  	 
		}
			// Get Badge counts
		    $badge = new UserBadge();
            $badge->pendingbadge();
        if (Session::get('auto_reports_approver') == '1'){
            $admin_badge = new Badge();
            $admin_badge->badge();
        }
		
		return $next($request);

	}
}