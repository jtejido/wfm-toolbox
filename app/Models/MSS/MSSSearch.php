<?php

namespace App\Models\MSS;

use Illuminate\Database\Eloquent\Model;

class MSSSearch extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_reports.mss_all_search';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'win_id';
  public $timestamps = false;

}
