<?php

namespace App\Models\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class ProcessStep extends Model
{
  use SoftDeletes;
  protected $table = 'app_toolbox.flow_step';
  protected $touches = ['Process'];
  protected $primaryKey = 'step_id';

  protected static function boot()
  {
      parent::boot();

      static::addGlobalScope('priority', function (Builder $builder) {
          $builder->orderBy('priority', 'asc');
      });
  }


  public function process()
  {
      return $this->belongsTo('App\Models\Flow\Process', 'process_id');
  }
  public function user()
  {
      return $this->belongsTo('App\User', 'winid');
  }

}
