<?php

namespace App\Models\Flow;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{

  protected $table = 'app_toolbox.flow_folder';
  protected $primaryKey = 'folder_id';

/*
  protected static function boot() {
    parent::boot();

    static::deleted(function ($processes) {
        $processes->processes()->delete();
    });
  }
*/
  
  public function processes()
  {
      return $this->hasMany('App\Models\Flow\Process', 'folder_id')->orderBy('name');
  }

  public function user()
  {
      return $this->belongsTo('App\User', 'winid');
  }

}
