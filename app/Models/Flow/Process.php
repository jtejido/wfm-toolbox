<?php

namespace App\Models\Flow;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Process extends Model
{
  use SoftDeletes;
  protected $table = 'app_toolbox.flow_process';
  protected $primaryKey = 'process_id';

  protected static function boot() {
    parent::boot();

    static::deleted(function ($steps) {
      $steps->steps()->delete();
    });
  }

  public function folder()
  {
      return $this->belongsTo('App\Models\Flow\Folder', 'folder_id');
  }
  public function user()
  {
      return $this->belongsTo('App\User', 'winid');
  }
  public function steps()
  {
      return $this->hasMany('App\Models\Flow\ProcessStep', 'process_id');
  }

}
