<?php

namespace App\Models\CDP;

use Illuminate\Database\Eloquent\Model;

class CDPSites extends Model
{


    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_toolbox.cdp_sites';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'site_id';
  public $timestamps = false;

}
