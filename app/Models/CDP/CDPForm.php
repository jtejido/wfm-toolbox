<?php

namespace App\Models\CDP;

use Illuminate\Database\Eloquent\Model;

class CDPForm extends Model
{


    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_toolbox.cdp_form';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'form_id';
  public $timestamps = false;

}
