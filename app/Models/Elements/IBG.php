<?php

namespace App\Models\Elements;

use Illuminate\Database\Eloquent\Model;

class IBG extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_elements.ibg';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'ibg_id';
  public $timestamps = false;

}
