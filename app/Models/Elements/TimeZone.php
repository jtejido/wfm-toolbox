<?php

namespace App\Models\Elements;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_elements.time_zones';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = NULL;
  public $timestamps = false;

}
