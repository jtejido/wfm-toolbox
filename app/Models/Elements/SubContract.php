<?php

namespace App\Models\Elements;

use Illuminate\Database\Eloquent\Model;

class SubContract extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_elements.subcontract';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'subcontract_id';
  public $timestamps = false;

}
