<?php

namespace App\Models\Elements;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{


    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_elements.region';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'region_id';
  public $timestamps = false;

}
