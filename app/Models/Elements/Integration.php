<?php

namespace App\Models\Elements;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_elements.integration';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'type',
      'sub_type',
      'integration_comment',
      'element_type',
      'system_name',
      'ticket_req'
  ];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'src_type_id';
  public $timestamps = false;

}
