<?php

namespace App\Models\Elements;

use Illuminate\Database\Eloquent\Model;

class Elements extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_elements.elements';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'element_name',
      'subcontract_id',
      'language_id',
      'site_id',
      'costcentre_id',
      'element_source_timezone',
      'report_timezone'
      ];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'ibg_id';
  public $timestamps = false;

}
