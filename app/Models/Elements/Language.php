<?php

namespace App\Models\Elements;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{


    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_elements.language';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'language_id';
  public $timestamps = false;

}
