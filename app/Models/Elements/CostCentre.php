<?php

namespace App\Models\Elements;

use Illuminate\Database\Eloquent\Model;

class CostCentre extends Model
{


    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_elements.costcentre';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'description',
      'site_id',
      'contract_id',
      'src_type_id',
      'client_id'
  ];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'costcentre_id';
  public $timestamps = false;

}
