<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_dbmgmt.activity_log_dash';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'recipient_id';
  public $timestamps = false;

}
