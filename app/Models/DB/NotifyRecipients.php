<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Model;

class NotifyRecipients extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'meta_dbmgmt.val_notify_recipient';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'obj_id';
  public $timestamps = false;

}
