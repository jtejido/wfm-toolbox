<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ModuleMapDash extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_toolbox.vw_toolbox_users_dash';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'winid';
  public $timestamps = false;

}
