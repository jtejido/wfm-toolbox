<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class WebReport extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_reports.web_report';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = null;
  public $timestamps = false;

}
