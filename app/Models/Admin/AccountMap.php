<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AccountMap extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_reports.user_map';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'user_name';
  public $timestamps = false;

}
