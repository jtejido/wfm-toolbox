<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ApprovalQueue extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_reports_mgmt.vw_approval_queue_dash';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'request_id';
  public $timestamps = false;

}
