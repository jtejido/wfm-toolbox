<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class RunReportLog extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_reports_mgmt.run_report_log';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'rid';
  public $timestamps = false;

}
