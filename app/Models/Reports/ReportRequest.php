<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportRequest extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_reports_mgmt.report_request';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'request_id';
  public $timestamps = false;

}
