<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ActionRequest extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_reports_mgmt.action_request';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'action_id';
  public $timestamps = false;

}
