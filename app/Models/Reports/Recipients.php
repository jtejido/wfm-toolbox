<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class Recipients extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_reports_mgmt.recipients';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'recipient_id';
  public $timestamps = false;

}
