<?php

namespace App\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class ReportRecipientDash extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_toolbox.vw_report_recipients_assigned_dash';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = null;
  public $timestamps = false;

}
