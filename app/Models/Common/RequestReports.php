<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class RequestReports extends Model
{

    /**
   * The database table used by the model.
   *
   * @var string
   */
   protected $table = 'app_toolbox.vw_request_reports_query_dash';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [];
  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
  protected $primaryKey = 'rid';
  public $timestamps = false;

}
