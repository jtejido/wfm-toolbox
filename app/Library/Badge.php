<?php
namespace App\Library;
use Session;
use DB;
use App\Models\Reports\ApprovalQueue;
use App\ApproverMap;


class Badge {
 public static function badge() {
  $employees = DB::select("select recipient_id from app_toolbox.get_reporting_recipient_employees(:win_id) where depth != 0", ['win_id' => Session::get('toolbox_s3_id')]);
	$approval_count = ApprovalQueue::select();
	if ((Session::get('is_approver') == '1') && (Session::get('is_admin') == '0')){
            foreach ($employees as $employee){
            $approval_count = $approval_count->orWhere('recipient_id', '=', $employee->recipient_id);
            }
        }
    $approval_count = $approval_count->count();
    $total = Session::get('total');

    if ($approval_count){
	  	$total++;
	  }

	  Session::put('total', $total);
      Session::put('approval_count', $approval_count);
      return true;
 }
}