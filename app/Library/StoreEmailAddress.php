<?php
namespace App\Library;
use Session;
use App\Models\Reports\Recipients;
use App\MSS;
use App\User;
use Illuminate\Support\Facades\Input;

class StoreEmailAddress {
 public static function insert($email, $winid, $name) {

         $check = Recipients::where('email_address', 'ilike', "%$email[0]%")->first();
         if (!empty($check)){
            $recipient = Recipients::find($check->recipient_id);
                $recipient->email_address = $email[0];
                $recipient->save();
                $user = User::where('win_id', $winid)->first();
                Session::put('email_address_toolbox', $user->email_address);
                Session::put('recipient_data_toolbox', $user->recipient_id);
         }
         else {
            $name = MSS::select('full_name')->where('win_id', $winid)->first();
            $recipient = new Recipients;
                $recipient->name       = $name[0];
                $recipient->email_address = $email[0]; 
                $recipient->internal_external = 'Internal';
                $recipient->company = 'Conduent';
                $recipient->s3_id = $winid;
                $recipient->save();
                $user = User::where('win_id', $winid)->first();
                Session::put('email_address_toolbox', $user->email_address);
                Session::put('recipient_data_toolbox', $user->recipient_id);
         }
        return true;
 }
}
