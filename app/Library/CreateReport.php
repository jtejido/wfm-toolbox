<?php
namespace App\Library;
use DB;
use Illuminate\Support\Facades\Input;
use \App\Models\Reports\ReportRecipients;
use \App\Models\Reports\Reports;
use \App\Models\Reports\ReportRequest;
use \App\Models\Reports\ReportTemplate;
use \App\Models\Reports\Frequency;
use \App\Library\QueryTrack;
use Session;

class CreateReport {


    public static function createreport(){
        DB::connection()->enableQueryLog();
                        if(Input::has('client')){
                            $client_temp = Input::get('client');
                        }
                        else{
                            $client_temp = Input::get('client_not_listed');
                        }
                                               

                        $report = new Reports;
                        $report->rid       = Input::get('rid');
                        if($client_temp != "Template Report"){
                            $report->name       = !empty(Input::get('name')) ? Input::get('name'): '';
                        }
                        elseif($client_temp == "Template Report"){
                            $report->name       = !empty(Input::get('updatetemplate')) ? Input::get('updatetemplate'): '';
                        }
                        $report->encryption = !empty(Input::get('encryption')) ? Input::get('encryption'): '';
                        $report->internal_external = !empty(Input::get('internal_external')) ? Input::get('internal_external'): '';
                        if (Input::has('client')){
                        $report->client = !empty(Input::get('client')) ? Input::get('client'): '';
                        }
                        else{
                        $report->client = !empty(Input::get('client_not_listed')) ? Input::get('client_not_listed'): '';
                        }
                        $report->isdateappended = !empty(Input::get('isdateappended')) ? Input::get('isdateappended') : '';
                        $report->savewithmacro = !empty(Input::get('savewithmacro')) ? Input::get('savewithmacro') : '';
                        $report->deleteconnections = !empty(Input::get('deleteconnections')) ? Input::get('deleteconnections') : '';
                        $report->nameofemailbodysheet = !empty(Input::get('nameofemailbodysheet')) ? Input::get('nameofemailbodysheet') : '';
                        $report->validationchecks = !empty(Input::get('validationtables')) ? Input::get('validationtables') : '';
                        $report->validationtables = '';
                        $report->customemailtext = !empty(Input::get('customemailtext')) ? Input::get('customemailtext') : '';
                        $report->report_status = !empty(Input::get('report_status')) ? Input::get('report_status') : '';
                        $report->custom_email_subject = !empty(Input::get('custom_email_subject')) ? Input::get('custom_email_subject') : '';
                        $report->run_region = !empty(Input::get('run_region')) ? Input::get('run_region') : '';
                        $report->custom_attachment_name = !empty(Input::get('custom_attachment_name')) ? Input::get('custom_attachment_name') : '';
                        $report->zip_file_format = !empty(Input::get('zip_file_format')) ? Input::get('zip_file_format') : '';
                        $report->first_day_of_week  = !empty(Input::get('first_day_of_week')) ? Input::get('first_day_of_week') : '';
                        $report->save();


                        //store report template
                        if($client_temp == "Template Report"){
                        $report_template = new ReportTemplate;
                        $report_template->rid = Input::get('rid');
                        $report_template->name = Input::get('final_name');
                        $report_template->where_condition = Input::get('where_condition');
                        $report_template->customer_id = null;
                        $report_template->template = Input::get('updatetemplate');
                        $report_template->save();

                        }

                        $copyrecipient = Input::get('copyrecipients');
                        $copyfrequency = Input::get('copyfrequency');

                            //store recipient    
                            $int = 1;
                            $limit = 10; 
                               while($int <= $limit) {
                                    $recipient_counter = 'ID'.$int.'_recipient_id';
                                    $rid_counter = 'ID'.$int.'_rid';
                                    $email_counter = 'ID'.$int.'_email_type';
                                    $tcb_counter = 'ID'.$int.'_to_cc_bcc';
                                    $sd_counter = 'ID'.$int.'_start_dte';
                                    $ed_counter = 'ID'.$int.'_end_dte';
                                    if(!empty(Input::get($recipient_counter))){

                                            $report_recipient = new ReportRecipients;
                                            $report_recipient->recipient_id       = Input::get($recipient_counter);
                                            $report_recipient->rid       = Input::get($rid_counter);
                                            $report_recipient->email_type = Input::get($email_counter);
                                            $report_recipient->to_cc_bcc = Input::get($tcb_counter);
                                            $report_recipient->start_dte = !empty(Input::get($sd_counter)) ? Input::get($sd_counter) : null;
                                            $report_recipient->end_dte = !empty(Input::get($ed_counter)) ? Input::get($ed_counter) : null;
                                            $report_recipient->save();
                                        }
                                    $int++;
                            }

                            //store frequency
                                if(!empty(Input::get('newday'))){
                                        foreach (Input::get('newday') AS $key => $value){
                                            $new_exp = !empty(Input::get('newexp')) ? Input::get('newexp') : null;
                                            $frequency = new Frequency;
                                            $frequency->rid = Input::get('newrid');
                                            $frequency->day = $value;
                                            $frequency->scheduled_time = Input::get('newsched');
                                            $frequency->sla_time = Input::get('newsla');
                                            $frequency->time_zone = Input::get('newtz');
                                            $frequency->expected_complete_time = Input::get('newect');
                                            $frequency->expire_time = $new_exp;
                                            $frequency->save();
                                        }
                                }
            $queries = DB::getQueryLog();
            QueryTrack::insert(Session::get('toolbox_s3_id'), $queries, 'reports');

                        if($copyrecipient == "Recipients"){

                            DB::insert(DB::raw("insert into app_reports_mgmt.report_recipient(rid, recipient_id, email_type,to_cc_bcc,start_dte) select :new_rid,recipient_id,email_type,to_cc_bcc,current_date+1 from app_reports_mgmt.report_recipient where rid=:orig_rid"),  array('new_rid' => Input::get('rid'), 'orig_rid' => Input::get('original_rid')));


                        }

                        if($copyfrequency == "Frequency"){

                            DB::insert(DB::raw("insert into app_reports_mgmt.frequency(rid, day, scheduled_time,sla_time,time_zone,expected_complete_time,expire_time) select :new_rid,day,scheduled_time,sla_time,time_zone,expected_complete_time,expire_time from app_reports_mgmt.frequency where rid=:orig_rid"),  array('new_rid' => Input::get('rid'), 'orig_rid' => Input::get('original_rid')));


                        }

        }
}