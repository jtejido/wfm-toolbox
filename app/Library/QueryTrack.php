<?php
namespace App\Library;
use \App\QueryTracker;
use Carbon\Carbon;

class QueryTrack {
 public static function insert($winid, $queries, $page) {

    $query = array();
        foreach ($queries as $key['query'] => $value){
            $query[] = $value;
        }
        $lastQuery = $query;

        foreach ($lastQuery as $key => $value){
          $track = new QueryTracker;
          $track->winid = $winid; 
          $track->query = $value['query'];
          $track->bindings = json_encode($value['bindings']);
          $track->which_page = $page;
          $track->time_stamp = Carbon::now();
          $track->save();
        }
        return true;
 }
}
