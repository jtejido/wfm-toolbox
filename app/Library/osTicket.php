<?php
 
namespace App\Library;
 
class osTicket {
 
  protected $url;
 
  protected $apikey;
 
  protected $topicid;

  public function __construct($apikey = null, $url = null, $topic_id = null) {
    $this->url = env('TICKET_URL', $url);
    $this->apikey = env('TICKET_APIKEY', $apikey);
    $this->topicid = env('TICKET_TOPIC', $topic_id);
  }
 
  public function createTicket($subject, $message) {
    $data = array(
            'name' => 'CC Datamart',
            'email' => 'noreply@xerox.com',
            'phone' => '000000000',
            'subject' => $subject,
            'message' => $message,
            'ip' => '10.235.230.66',
            'topicId' => $this->topicid,
            'account' => array(32),
            'user_location' => 'CC Datamart',
            'product' => array(100),
    );

    function_exists('curl_version') or die('CURL support required');
    function_exists('json_encode') or die('JSON support required');
    set_time_limit(30);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_USERAGENT, 'osTicket API Client v1.8');
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Expect:', 'X-API-Key: '.$this->apikey));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $ticket_id = (int) $result;
 
    return $ticket_id;
  }
 
}