<?php
namespace App\Library;
use Session;
use App\RejectedCount;
use App\ApprovedCount;
use App\Models\Reports\ApprovalQueue;
use Carbon\Carbon;

class UserBadge {
 public static function pendingbadge() {
	  $pending_rejected = RejectedCount::where('recipient_id', Session::get('recipient_data_toolbox'))->count();
	  $pending_approved = ApprovedCount::where('recipient_id', Session::get('recipient_data_toolbox'))->count();
	  $pending_count = ApprovalQueue::where('recipient_id', Session::get('recipient_data_toolbox'))->count();
	  $total = 0;

	  if ($pending_rejected){
	  	$total++;
	  }

	  if ($pending_approved){
	  	$total++;
	  }

      Session::put('pending_rejected', $pending_rejected);
      Session::put('pending_approved', $pending_approved);
      Session::put('total', $total);
      Session::put('pending_count', $pending_count);

      return true;
 }
}